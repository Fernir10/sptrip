['.less', '.svg', '.css', '.styl', '.scss', '.ttf', '.woff', '.woff2'].forEach((ext) => require.extensions[ext] = () => {});
require('babel-core/register');
require('babel-polyfill');

const spdy = require('spdy');
const fs = require('fs');
const {api} = require('./server/tourvisor/api');
const globalapp = require('./server/main');

const http = require('http').Server(globalapp);

const https2 = spdy.createServer({
  key: fs.readFileSync('./keys/01_02_2018/sptrip.key'),
  cert: fs.readFileSync('./keys/01_02_2018/sptrip.crt'),
  ca: fs.readFileSync('./keys/01_02_2018/sptrip.ca')
}, globalapp);

process.on('unhandledRejection', (r) => console.log(r));

const io = require('socket.io')(https2);

io.on('connection', (socket) => {
  socket.on('getdata', (requestId) => api.getSearchStreamResults(requestId, socket));

  socket.on('search', (requestId) => {
    socket.stop = false;
    socket.emit('search', {
      status: {
        state: 'searching',
        progress: 1
      }
    });

    return api.createSearchStream(requestId, socket);
  });
});


https2.listen(443);
http.listen(80);
console.log('server started');
