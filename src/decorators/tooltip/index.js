import React, {Component as ReactComponent} from 'react';
import cn from 'classnames';

import './tooltip.less';

export const tooltip = (Component) => {
  class DecoratedComponent extends ReactComponent {
    static defaultProps = {
      foldingTooltip: true,
      className: '',
      isShowTooltip: false,
      onFocusIn: null,
      onFocusOut: null,
      onMouseEnter: null,
      onMouseLeave: null,
      isContentBoxTooltip: false
    };

    state = {
      isShow: false,
      focused: false
    };

    show = () => this.setState({isShow: true});

    hide = () => this.setState({isShow: false});

    onFocusIn = () => {
      this.initEventCallback('onFocusIn');
      this.setState({focused: true});
      this.show();
    };

    onFocusOut = () => {
      this.initEventCallback('onFocusOut');
      this.setState({focused: false});
      this.hide();
    };

    onMouseEnter = () => {
      this.initEventCallback('onMouseEnter');
      this.show();
    };

    onMouseLeave = () => {
      this.initEventCallback('onMouseLeave');
      this.hide();
    };

    initEventCallback(name) {
      this.props[name] ? this.props[name]() : null;
    }

    getClasses() {
      return cn({
        tooltip: true,
        'tooltip--content-box': this.props.isContentBoxTooltip,
        'tooltip--folding': this.props.foldingTooltip,
        'tooltip--error': this.props.errorMessage,
        'tooltip--expanded': (this.state.isShow && this.props.errorMessage) || (this.state.focused && this.props.errorMessage)
      });
    }

    render() {
      return (
        <Component
          {...this.props}
          className={this.props.className}
          style={{position: 'relative', ...this.props.style}}
          onFocusIn={this.onFocusIn}
          onFocusOut={this.onFocusOut}
        >
          {this.props.errorMessage && (
            <span className={this.getClasses()}>
              <div className="tooltip__i">
                <span className="tooltip__txt">
                  {this.props.errorMessage}
                </span>
              </div>
            </span>
          )}
        </Component>
      );
    }
  }

  return DecoratedComponent;
};
