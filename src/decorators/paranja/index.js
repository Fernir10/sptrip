import React, {Fragment, PureComponent as ReactComponent} from 'react';
import styled from 'styled-components';

export const paranja = (Component) => {
  const Styled = styled(Component)`
    z-index: ${(props) => (props.show ? 6 : 0)} !important;
  `;

  const Paranja = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: 3;
    background: rgba(0, 0, 0, ${(props) => props.show ? 0.3 : 0});
    pointer-events: ${(props) => props.show ? 'all' : 'none'};
    transition: background .3s ease;
  `;

  return class DecoratedComponent extends ReactComponent {
    state = {
      show: false
    };

    oldSetState = null;

    componentDidMount() {
      document.addEventListener('keydown', (e) => {
        if (e.keyCode === 27) {
          this.onClose();
        }
      }, {passive: true});

      this.oldSetState = this.component.setState.bind(this.component);
      this.component.setState = (args) => {
        this.oldSetState(args, () => {
          if (typeof args.isOpened !== 'undefined' && args.isOpened !== this.state.show) {
            this.setState({
              show: args.isOpened
            });
          }
          if (typeof args.focus !== 'undefined' && args.focus !== this.state.show) {
            this.setState({
              show: args.focus
            });
          }
        });
      };
    }

    onClose = () => {
      if (this.component) {
        this.component.setState({isOpened: false});
      }
    };

    render() {
      return (
        <Fragment>
          <Paranja
            onClick={this.onClose}
            show={this.state.show}
          />
          <Styled
            innerRef={(node) => { this.component = node; }}
            {...this.props}
            show={this.state.show}
          />
        </Fragment>
      );
    }
  };
};
