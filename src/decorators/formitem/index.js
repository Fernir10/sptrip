import React, {PureComponent as ReactComponent} from 'react';
import cn from 'classnames';
import styled from 'styled-components';

import './formitem.less';

export const formItem = (Component) => {
  const StyledComponent = styled(Component)`
    color: ${(props) => props.theme.color};
    background: ${(props) => props.theme.whiteBackgroundColor};
    box-shadow: 0 0 0 1px ${(props) => props.theme.borderColor};
  `;

  return class DecoratedComponent extends ReactComponent {
    constructor(props) {
      super();
      this.props = props;
    }

    render() {
      return (
        <StyledComponent {...this.props} className={cn('formitem', this.props.className)}/>
      );
    }
  };
};
