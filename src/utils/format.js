const pricePercent = 1.5;

export const formatPrice = (num = 0, currency = '') => {
  const oldPrice = pricePercent > 0 ? Math.round(num - ((num / 100) * pricePercent)) : num;
  return `${oldPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '\u00a0')}${currency === 'RUB' ? ' руб' : ` ${currency}`}`;
};

export const fullMonthsOne = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
export const fullMonths = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
export const shortMonths = ['янв', 'фев', 'мар', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'ноя', 'дек'];

const arrRu = ['Я', 'я', 'Ю', 'ю', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ж', 'ж', 'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р', 'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ы', 'ы', 'Ь', 'ь', 'Ъ', 'ъ', 'Э', 'э'];
const arrEn = ['Ya', 'ya', 'Yu', 'yu', 'Ch', 'ch', 'Sh', 'sh', 'Sh', 'sh', 'Zh', 'zh', 'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e', 'E', 'e', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c', 'Y', 'y', '`', '`', '\'', '\'', 'E', 'e'];

export const LatCyr = (text) => {
  let tmp = text;
  for (let i = 0; i < arrEn.length; i++) {
    const reg = new RegExp(arrEn[i], 'g');
    tmp = tmp.replace(reg, arrRu[i]);
  }
  return tmp;
};

export const calcDate = (date, addDays = 0) => {
  const [day, month, year] = date.split('.');
  const dateObj = new Date(Number(year), Number(month) - 1, Number(day));
  dateObj.setDate(dateObj.getDate() + addDays);
  return dateObj;
};

export const parseDate = (date) => {
  const [day, month, year] = date.split('.');
  const dateObj = new Date(Number(year), Number(month) - 1, Number(day));
  return `${dateObj.getDate()} ${fullMonths[dateObj.getMonth()]}`;
};

export const parseDateFull = (date) => {
  const [day, month, year] = date.split('.');
  const dateObj = new Date(Number(year), Number(month) - 1, Number(day));
  return `${dateObj.getDate()} ${shortMonths[dateObj.getMonth()]} ${dateObj.getFullYear()}`;
};

export const calculateDate = (addDays) => {
  const myDate = new Date();
  myDate.setDate(myDate.getDate() + addDays);
  return myDate;
};

export const formatDate = (date) => {
  const dateObj = new Date(Number(date));
  return `${`0${dateObj.getDate()}`.slice(-2)}.${`0${dateObj.getMonth() + 1}`.slice(-2)}.${dateObj.getFullYear()}`;
};

export const toCSS = (obj) => {
  if (typeof obj === 'object') {
    return Object.keys(obj).map((key) => {
      const upperIndex = key.replace(/([A-Z])/g, '-$1').toLowerCase();
      const value = typeof obj[key] === 'number' ? `${obj[key]}px` : obj[key];
      return `${upperIndex}: ${value};`;
    }).join('');
  }

  return obj;
};
