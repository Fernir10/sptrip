export const ModifyColor = (color = '#ffffff', percent = 0) => {
  const num = parseInt(color.slice(1), 16);
  const amt = Math.round(2.55 * percent);
  const R = (num >> 16) + amt;
  const G = (num >> 8 & 0x00FF) + amt;
  const B = (num & 0x0000FF) + amt;

  return `#${(0x1000000 + ((R < 255 ? R < 1 ? 0 : R : 255) * 0x10000) + ((G < 255 ? G < 1 ? 0 : G : 255) * 0x100) + (B < 255 ? B < 1 ? 0 : B : 255)).toString(16).slice(1)}`;
};

export const rgba = (hex, alpha) => {
  if (typeof hex !== 'string' || hex[0] !== '#') {
    return 'transparent';
  }

  const stringValues = (hex.length === 4)
    ? [hex.slice(1, 2), hex.slice(2, 3), hex.slice(3, 4)].map(n => `${n}${n}`)
    : [hex.slice(1, 3), hex.slice(3, 5), hex.slice(5, 7)];

  const intValues = stringValues.map(n => parseInt(n, 16));

  return (typeof alpha === 'number')
    ? `rgba(${intValues.join(', ')}, ${alpha})`
    : `rgb(${intValues.join(', ')})`;
};
