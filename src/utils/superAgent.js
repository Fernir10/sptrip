import request from 'superagent';

const superAgent = (() => {
  const pendingRequests = new Map();

  const makeRequest = (url, type = 'get', contentType = 'application/json') => (params = {}) => new Promise((resolve) => {
    const hash = new Date().getTime().toString();
    const paramsExploded = Object.keys(params).map((key) => `${key}=${params[key]}`).join('&');
    const serverHost = url.match(/http/gi) ? '' : '127.0.0.1';
    const serverLink = typeof document === 'undefined' ? serverHost : '';
    const unCompleteUrl = contentType === 'application/json' ? `${serverLink}${url}` : url;
    const completeUrl = `${unCompleteUrl}?${paramsExploded}`;

    if (pendingRequests.has(completeUrl)) {
      pendingRequests.get(completeUrl).req.abort();
    }

    const req = (type.toString() === 'get' ? request.get(completeUrl).query({}) : request.post(unCompleteUrl).send(params))
      // .set('Content-Type', contentType)
      .end((err, res) => {
        if (res && res.body && res.body.error) {
          resolve(res.body);
        } else if (err) {
          resolve(err);
        } else {
          if (res) {
            resolve(res.body && Object.keys(res.body).length ? res.body : res.text);
          } else {
            resolve({});
          }

          if (pendingRequests.get(completeUrl) && pendingRequests.get(completeUrl).hash === hash) {
            pendingRequests.delete(completeUrl);
          }
        }
      });

    pendingRequests.set(completeUrl, {req, hash});

    return req;
  });

  return {
    abort: () => pendingRequests.forEach(({req}) => req.abort()),
    record: makeRequest('/record/', 'post', 'multipart/form-data'),
    getrecord: makeRequest('/getrecord/', 'post'),
    access: makeRequest('/access/', 'post'),
    deleteUser: makeRequest('/delete/', 'post'),
    restore: makeRequest('/restore/', 'post'),
    register: makeRequest('/register/', 'post'),
    login: makeRequest('/login/', 'post'),
    changePassword: makeRequest('/edit/', 'post'),
    search: makeRequest('/api/tv/search/'),
    form: makeRequest('/api/tv/form/'),
    hot: makeRequest('/api/tv/hot/'),
    tour: makeRequest('/api/tv/tour/'),
    tourdetails: makeRequest('/api/tv/tourdetails/'),
    hotel: makeRequest('/api/tv/hotel/'),
    gettours: makeRequest('/api/tv/gettours/'),
    autocomplete: makeRequest('/api/tv/all/'),
    email: makeRequest('/ajax/email/', 'post'),
    calendar: makeRequest('/api/tv/calendar/'),
    settings: makeRequest('/settings_save', 'post'),
    getsettings: makeRequest('/settings'),
    server: {
      // country: makeRequest('http://tourvisor.ru/module/ajax_country.php'),
      API_LIST_URL: makeRequest('http://tourvisor.ru/xml/list.php'),
      API_SEARCH_URL: makeRequest('http://tourvisor.ru/xml/search.php'),
      API_RESULT_URL: makeRequest('http://tourvisor.ru/xml/result.php'),
      API_HOTEL_URL: makeRequest('http://tourvisor.ru/xml/hotel.php'),
      API_TOUR_URL: makeRequest('http://tourvisor.ru/xml/actualize.php'),
      API_ACTUALIZE_TOUR_URL: makeRequest('http://tourvisor.ru/xml/actdetail.php'),
      API_HOTTOURS_URL: makeRequest('http://tourvisor.ru/xml/hottours.php'),
      API_HOTEL_IMAGES_URL: makeRequest('https://tourvisor.ru/xml/modact.php'),
      API_MODMIN_URL: makeRequest('https://tourvisor.ru/xml/modcalendar.php')
    }
  };
})();

export default superAgent;
