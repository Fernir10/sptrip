import React, {Component, Fragment, PureComponent} from 'react';
import {Helmet} from 'react-helmet';
import {computed, extras, observable} from 'mobx';
import {observer} from 'mobx-react';
import {Route, Switch} from 'react-router';
import {withRouter} from 'react-router-dom';
import styled from 'styled-components';

import superAgent from './../utils/superAgent';

import {
  Avia,
  Contacts,
  CountryPage,
  HotelPage,
  Hotels,
  HotToursPage,
  IndexPage,
  Insurance,
  NewSearchPage,
  Personal,
  RegionPage,
  SearchPage,
  TourPage
} from './../pages';

import {ReviewsPage} from './../modules/reviews';


import {CallUs, Footer, Menu, PreselectOutput, Yametrika} from './../modules';

import {Flag, Img} from './../components';

import {ErrorIcon, PhoneIcon} from './../icons';

import './site.less';

// extras.isolateGlobalState();

class ScrollToTopRoute extends PureComponent {
  componentDidUpdate(newProps) {
    if (this.props.location.pathname !== newProps.location.pathname) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    return <Route {...this.props}/>;
  }
}

const Notfound = (props) => (
  <div className="container error--message">
    <Helmet>
      <title>Страница не найдена!</title>
      <meta name="description" content={`Страница ${props.location.pathname} не найдена!`}/>
    </Helmet>
    <h2>Страница {props.location.pathname} не найдена!</h2>
    <div className="flex">
      <ErrorIcon style={{marginRight: 10}}/>
      <span>Возможно она была удалена или перенесена</span>
      <Img src="/img/404-girl.png" style={{position: 'absolute', bottom: 0}}/>
    </div>
  </div>
);

const MainSite = styled(({theme: {backgroundColor = '#faf9f0', color = '#333'} = {}, children, className}) => (
  <div className={className}>{children}</div>
))`
  display: table;
  height: 100%;
  width: 100%;
  background-color: ${(props) => props.theme.backgroundColor};
  color: ${(props) => props.theme.color};
  font-size: 14px;
`;

@withRouter
@observer
export default class Site extends Component {
  @observable darkTheme = false;

  constructor(props) {
    super(props);
    const {store: {settings: {darkTheme = false} = {}} = {}} = props;
    this.darkTheme = darkTheme;

    if (typeof document !== 'undefined') {
      document.body.classList.add('document-loading');
      window.addEventListener("load", () => {
        document.body.classList.remove('document-loading');
      }, false);
    }
  }

  @computed get
  computedTheme() {
    return !this.darkTheme ? {
      name: 'light',
      backgroundColor: '#faf9f0',
      whiteTextColor: '#ffffff',
      whiteBackgroundColor: '#ffffff',
      borderColor: '#707070',
      blackColor: '#000000',
      orangeColor: '#d95107',
      orangeLightColor: '#ffaa00',
      blueColor: '#477ab9',
      darkBlueColor: '#394C7F',
      color: '#000000',
      darkColor: '#aaaaaa',
      darkEasyColor: '#3a3a3a',
      greenColor: '#007b0a'
    } : {
      name: 'dark',
      backgroundColor: '#3c3c3c',
      whiteTextColor: '#ffffff',
      whiteBackgroundColor: '#535353',
      borderColor: '#363636',
      blackColor: '#000000',
      orangeColor: '#c33a06',
      orangeLightColor: '#ae4c27',
      blueColor: '#2c2c2c',
      darkBlueColor: '#202020',
      color: '#d4d4d4',
      darkColor: '#282828',
      darkEasyColor: '#222222',
      greenColor: '#007b0a'
    };
  }

  changeTheme = () => {
    this.darkTheme = !this.darkTheme;
    superAgent.settings({
      settings: {
        darkTheme: this.darkTheme
      }
    });
  };

  componentDidMount() {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/js/sv.js');
    }
  }

  menuSkelet = [
    {
      title: 'Горящие туры',
      href: '/hottours/'
    },
    {
      title: 'Расчет тура',
      href: '/preselect/'
    },
    {
      title: 'Страны',
      children: this.props.store.countriesLinks && this.props.store.countriesLinks.map((c) => ({
        href: `/countries/${c.id}`,
        title: c.name,
        icon: <Flag country={c.id} size="small" style={{marginRight: '10px'}}/>
      }))
    },
    {
      title: 'Услуги',
      children: [
        {
          href: '/avia',
          title: 'Авиаперелеты'
        },
        {
          href: '/hotels',
          title: 'Отели'
        },
        {
          href: '/insurance',
          title: 'Страхование'
        }
      ]
    },
    {
      href: '/contacts/',
      title: 'Контакты'
    },
    {
      href: '/reviews/',
      title: 'Отзывы'
    },
    {
      title: 'Заказать звонок',
      type: 'phone-icon',
      href: '/callus/',
      icon: <PhoneIcon color="orange" style={{width: 20, height: 20, marginRight: 10, position: 'relative', top: 5}}/>
    }
  ];


  render() {
    const {pathname} = this.props.location;
    const isModal = this.props.location.state && this.props.location.state.modal;

    return (
      <Fragment>
        <Helmet
          titleTemplate="sptrip.ru | %s"
          defaultTitle="Интернет-агентство путешествий"
        >
          <title>Интернет-агентство путешествий</title>
          <meta charSet="utf-8"/>
          <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=0"/>
          <link rel="canonical" href={`https://sptrip.ru${pathname}`}/>
          <meta name="robots" content="index,follow"/>
          <meta name="keywords" content="отель забронировать 2018 дешево купить тур перелет страховка чартер 2017 раньше греция таиланд индия оаэ турция тунис путевки отдых помощь отпуск пляж море"/>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
          <link rel="icon" href="/img/cropped-sptrip_f-80x80.png" sizes="80x80"/>
          <link rel="apple-touch-icon-precomposed" href="/img/cropped-sptrip_f-180x180.png"/>
          <meta name="msapplication-TileImage" content="/img/cropped-sptrip_f-270x270.png"/>
          <link rel="manifest" href="/manifest.json"/>
          <meta name="theme-color" content="#477ab9"/>
          <meta name="application-name" content="Интернет-агентство путешествий sptrip.ru"/>
          <meta property="og:type" content="website"/>
          <meta property="og:url" content={`https://sptrip.ru${pathname}`}/>
          <meta property="og:image" content="https://sptrip.ru/img/cropped-sptrip_f-192x192.png"/>
          <meta property="og:site_name" content="sptrip.ru"/>
        </Helmet>
        <MainSite theme={this.computedTheme}>
          <Menu menu={this.menuSkelet} {...this.props} theme={this.computedTheme}/>
          <main className="main-content">
            <Switch>
              <ScrollToTopRoute exact path="/" render={(props) => (<IndexPage {...props} store={this.props.store} search={this.props.search} tours={this.props.tours} hot={this.props.hot} theme={this.computedTheme}/>)}/>
              <ScrollToTopRoute exact path="/avia/" component={Avia}/>
              <ScrollToTopRoute exact path="/hotels/" component={Hotels}/>
              <ScrollToTopRoute exact path="/insurance/" component={Insurance}/>
              <ScrollToTopRoute exact path="/contacts/" render={(props) => <Contacts {...props} {...this.props} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/search/:departure?/:country?/:region?/:hotel?/:queryhotel?/:datefrom?/:dateto?/:nightsfrom?/:nightsto?/:adults?/:child?/:childage1?/:childage2?/:childage3?/:beach?/:auto?/" render={(props) => <SearchPage {...props} store={this.props.store} search={this.props.search} tours={this.props.tours} hot={this.props.hot} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/hottours/" render={(props) => <HotToursPage {...props} store={this.props.store} departure={1} country={4} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/tour/:tourid?/" render={(props) => <TourPage {...props} store={this.props.store} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/hotel/:hotelcode?/:search?/" render={(props) => <HotelPage {...props} store={this.props.store} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/preselect/" render={(props) => <PreselectOutput {...props} store={this.props.store} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/callus/" render={(props) => <CallUs {...this.props} {...props} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/test/" render={(props) => <NewSearchPage {...this.props} {...props}/>} theme={this.computedTheme}/>
              <ScrollToTopRoute exact path="/countries/:country?/" render={(props) => <CountryPage {...this.props} {...props} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/regions/:region?/" render={(props) => <RegionPage {...this.props} {...props} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute exact path="/reviews/" render={(props) => <ReviewsPage {...this.props} {...props} theme={this.computedTheme}/>}/>
              <ScrollToTopRoute render={(props) => <Notfound {...props} {...this.props} theme={this.computedTheme}/>}/>
            </Switch>
          </main>
          {isModal && <Personal {...this.props} theme={this.computedTheme}/>}
          <Footer {...this.props} theme={this.computedTheme} changeTheme={this.changeTheme}/>
        </MainSite>
        <noscript>
          <div className="noscript">
            <div className="noscript-container">
              Для полноценной работы сайта необходимо включить JavaScript
            </div>
          </div>
        </noscript>
        <Yametrika counter={43459869}/>
      </Fragment>
    );
  }
}
