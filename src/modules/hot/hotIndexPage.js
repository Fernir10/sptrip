import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {observable} from 'mobx';

import {TourBlock} from '.';

import superAgent from '../../utils/superAgent';

import './hot.less';

@observer
export class HotToursIndexPage extends Component {
  @observable tourList = [];

  constructor(props) {
    super(props);
    if (this.props.store && this.props.store.hot) {
      this.tourList = this.props.store.hot || [];
    } else {
      this.updateTours();
    }
  }

  updateTours = () => {
    superAgent.hot({
      items: 8,
      picturetype: 1,
      visa: 1
    })
      .then((data) => {
        if (data) {
          this.tourList = data || [];
        }
      });
  };

  render() {
    return (
      <section className="hot--container">
        {this.props.children}
        <div className="hot" itemScope itemType="http://schema.org/ItemList">
          <link itemProp="itemListOrder" href="http://schema.org/ItemListOrderDescending"/>
          {this.tourList.map((tour, index) => <TourBlock theme={this.props.theme} key={`hottours-index-${index}`} {...tour}/>)}
        </div>
      </section>
    );
  }
}
