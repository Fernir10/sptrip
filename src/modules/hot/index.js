import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import cn from 'classnames';

import superAgent from '../../utils/superAgent';
import {formatPrice, parseDate} from './../../utils/format';
import {Card, Docker, Select, Stars, StarsSelector} from './../../components';
import {FireIcon} from '../../icons';

import './hot.less';


export const TourBlock = ({
  theme,
  hotelname,
  hotelpicture,
  hotelregionname,
  countryname,
  tourid,
  price,
  priceold,
  operators,
  operatorcode,
  hotelstars,
  flydate,
  nights,
  departurenamefrom,
  currency
}) => (
  <Card
    theme={theme}
    margin={10}
    picture={hotelpicture}
    alt={`${hotelregionname}, ${countryname} - отель ${hotelname} по самой низкой цене от SPTRIP.RU`}
    location={`/tour/${tourid}`}
    percent={`-${Math.ceil(100 - (price / (priceold / 100)))}%`}
  >
    {(operators && operators[operatorcode]) && (
      <div>{operators[operatorcode]}</div>
    )}
    <div><Stars fav={hotelstars} size={20}/></div>
    <div className="hot-card-title">{hotelname}</div>
    <div><div className="muted">{hotelregionname}, {countryname}</div></div>
    <div>{parseDate(flydate)}, {nights} ночей</div>
    <div>из {departurenamefrom}</div>
    <div className="flex-between flex">
      <div className="hot-oldprice flex-col muted">{formatPrice(priceold, currency)}</div>
      <div><span className="hot-newprice">{formatPrice(price, currency)}</span></div>
    </div>
  </Card>
);

@observer
export class HotTours extends Component {
  @observable.shallow tourList = null;
  @observable.shallow departureList = null;
  @observable.shallow countryList = null;
  @observable.shallow operators = {};

  @observable departure = 1;
  @observable country = 4;
  @observable star = 3;
  visa = 0;

  componentWillMount() {
    this.componentWillReceiveProps(this.props);
  }

  componentWillReceiveProps(newProps) {
    console.log(1);

    const {store: {tourList, operators, departures, countries} = {}, country: oldCountry, departure: oldDeparture} = this.props;
    const {departure, country} = newProps;

    this.departure = departure || 1;
    this.country = country || 4;

    if (tourList && operators && departures && countries && this.country === oldCountry && this.departure === oldDeparture) {
      this.departureList = departures.map((itm) => ({value: itm.id, title: itm.name}));
      this.countryList = countries.map((itm) => ({value: itm.id, title: itm.name}));
      operators.forEach((itm) => { this.operators[itm.id] = itm.name; });
      this.tourList = tourList;
    } else {
      this.updateForm();
      this.updateTours();
    }
  }

  updateForm = () => superAgent.form({
    type: 'departure,country,operator',
    departure: this.departure,
    country: this.country,
    cndep: this.departure
  })
    .then(({departures, countries, operators}) => {
      if (departures && countries && operators) {
        this.departureList = departures.map((itm) => ({value: itm.id, title: itm.name}));
        this.countryList = countries.map((itm) => ({value: itm.id, title: itm.name}));
        operators.forEach((itm) => { this.operators[itm.id] = itm.name; });
      }
    });

  updateTours = () => {
    superAgent.hot({
      items: this.props.count || 12,
      picturetype: 1,
      city: this.departure,
      countries: this.country,
      stars: this.star,
      visa: this.visa
    })
      .then((data) => {
        this.tourList = data && data.map ? data : null;
      });
  };

  onSelectDeparture = (val) => {
    this.departure = val;
    this.updateForm();
    this.updateTours();
  };

  onSelectCountry = (val) => {
    this.country = val;
    this.updateTours();
  };

  onSelectStars = (val) => {
    this.star = val;
    this.updateTours();
  };

  render() {
    const {theme, nomenu} = this.props;

    return (
      <Fragment>
        <div className={cn('container', {ptop20: !nomenu})}>
          {!nomenu && (
            <Fragment>
              <Helmet>
                <title>Горящие туры</title>
                <meta property="og:title" content="SPTRIP.RU | Горящие туры"/>
                <meta name="description" content="Страница горящих туров"/>
                <meta property="og:description" content="Страница горящих туров"/>
              </Helmet>
              <h1 className="flex"><FireIcon style={{width: 40, height: 40}}/>&nbsp;Горящие туры</h1>
              <Docker className="container-md bottom20 nomargin">
                <Select
                  theme={theme}
                  placeholder="Откуда"
                  onChange={this.onSelectDeparture}
                  options={this.departureList}
                  value={this.departure}
                />
                <Select
                  theme={theme}
                  placeholder="Куда"
                  onChange={this.onSelectCountry}
                  options={this.countryList}
                  value={this.country}
                />
                <StarsSelector
                  theme={theme}
                  shadowed
                  onChange={this.onSelectStars}
                  value={this.star}
                  size={20}
                  placeholder="Класс отеля"
                  style={{flex: 'inherit'}}
                />
                <div className="flex-col"/>
              </Docker>
            </Fragment>)
          }
          <div className="hot" style={{marginBottom: 20}}>
            { (this.tourList && this.tourList.length) && this.tourList.map((tour) => (
              <TourBlock
                theme={theme}
                key={tour.tourid}
                operators={this.operators}
                {...tour}
              />
            ))}
          </div>
        </div>
      </Fragment>
    );
  }
}
