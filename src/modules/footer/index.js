import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {rgba} from './../../utils/color';
import {Social} from './..';
import {Arrow, Columns, Docker, Flag} from '../../components';
import {ConfigIcon, EarthIcon, EmailIcon, PhoneIcon, ThemeIcon} from './../../icons';

import './footer.less';


const StyledBottomBlock = styled.div`
  position: fixed;
  z-index: 5;
  right: 0px;
  bottom: 40px;
  padding: 5px;
  border-radius: 4px;
  background: ${(props) => rgba(props.theme.backgroundColor, 0.5)};
  display: flex;
  flex-direction: column;
`;

const styleButtons = ({theme}) => `
  width: 40px !important;
  height: 40px !important;
  padding: 5px !important;
  border-radius: 4px;
  cursor: pointer;
  user-select: none;
  transition: all .2s ease;
  
  &:hover {
    background: ${theme.whiteBackgroundColor};
  }
`;

const StyledArrowTop = styled(Arrow)`${(props) => styleButtons(props)}`;
const StyledThemeIcon = styled(ThemeIcon)`${(props) => styleButtons(props)}`;

export const Footer = ({theme, changeTheme, store: {countriesLinks = []} = {}, location: {pathname} = {}}) => (
  <footer className="footer">
    <section className="container footer--content flex flex-center">
      <Columns>
        {countriesLinks ? countriesLinks.map((c) => c.id === pathname.split('/')[pathname.split('/').length - 1] ? (
          <span key={c.id} className="footer--content--small">
            <Flag country={c.id} size="small" style={{marginRight: '10px', position: 'relative', top: 3}}/>
            {c.name}
          </span>
        ) : (
          <Link key={c.id} to={`/countries/${c.id}`} className="footer--content--small">
            <Flag country={c.id} size="small" style={{marginRight: '10px', position: 'relative', top: 3}}/>
            {c.name}
          </Link>
        )) : null}
      </Columns>

    </section>
    <section className="footer-topline">
      <div className="flex-between container flex-row">
        <div className="footer-topline--item"><EmailIcon style={{marginRight: 10}}/><a href="mailto:info@sptrip.ru">info@sptrip.ru</a></div>
        <div className="footer-topline--item"><EarthIcon style={{marginRight: 10}}/> <a href="/contacts/">г. Москва, ул. Большая Якиманка, 38А</a></div>
        <div className="footer-topline--item"><PhoneIcon style={{marginRight: 10}}/> <a href="tel:84957443317">+7 (495) 744-33-17</a></div>
        <Social className="footer-topline--item"/>
      </div>
    </section>
    <section className="footer--content">
      <Docker className="footer--bottomline container">
        <div className="footer--bottomline--item">
          <Link
            to={{
              pathname,
              state: {modal: true, returnTo: pathname}
            }}
          >Положение об&nbsp;обработке персональных данных.</Link>
          <p>Данный интернет сайт носит исключительно информационный характер и&nbsp;вся информация на&nbsp;нем не&nbsp;является публичной офертой, определяемой положениями Статьи 437 (2) Гражданского кодекса Российской Федерации.</p>
          <p>Для получения подробной информации о&nbsp;наличии и&nbsp;стоимости, пожалуйста, обращайтесь к&nbsp;менеджерам по&nbsp;продажам.</p>
          <p>sptrip.ru использует файлы «cookie», с целью персонализации сервисов и повышения удобства пользования веб-сайтом. «Cookie» представляют собой небольшие файлы, содержащие информацию о предыдущих посещениях веб-сайта.</p>
          <p>Если вы не хотите использовать файлы «cookie», измените настройки браузера.</p>
        </div>
        <div className="nowrap footer--bottomline--item">
          <b className="bottom10">Наши Реквизиты:</b>
          <p>ИП Новиков Андрей Геннадьевич</p>
          <p>ИНН 710400364720</p>
          <p>ОГРНИП 316715400119637</p>
          <p>р/сч: 40802810600000062577</p>
          <p>АО «Тинькофф Банк» в г. Москва</p>
          <p>к/сч: 30101810145250000974</p>
          <p>БИК 044525974</p>
        </div>
        <div className="footer--bottomline--item last">
          <div className="bottom10 nowrap"><b>Время работы:</b></div>
          <span className="nowrap">c понедельника по субботу</span>,&nbsp;9:00&nbsp;—&nbsp;19:00</div>
      </Docker>
    </section>
    <section className="footer-topline">
      <div className="flex-between container flex-center-align">
        <div className="small muted footer-topline--item">{`sptrip.ru © 2017 - ${(new Date()).getFullYear()}`}</div>
        <div className="flex-end flex-center-align">
          <ConfigIcon className="pointer" onClick={() => location.assign('/admin/')}/>
          <StyledBottomBlock theme={theme}>
            <StyledArrowTop theme={theme} color={theme.color} size={10} isOpened onClick={() => { (document.documentElement || document.body).scrollTop = 0; }}/>
            <StyledThemeIcon theme={theme} size={20} color={theme.color} onClick={changeTheme}/>
          </StyledBottomBlock>
        </div>
      </div>
    </section>
  </footer>
);
