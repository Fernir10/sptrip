import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {computed, observable} from 'mobx';

import superAgent from './../../utils/superAgent';

import {ContactsData, Country, Departure, FlyDate, Nights, Peoples, Thanks} from './steps';

import './preselect.less';

class Preselect {
  constructor(props) {
    this.props = props;
    this.isUpdating = false;
    this.isActive = true;
    this.step = 0;
  }

  thank = new Thanks(this.attributes);

  @observable isUpdating = false;

  @observable success = false;
  @observable step = 0;

  @observable isActive = false;
  @observable info = {
    departureList: [],
    countryList: [],
    regionList: [],
    departure: 1,
    country: 4,
    region: null,
    date: null,
    nights: 7,
    childrens: [],
    peoples: 2,
    name: '',
    phone: '',
    email: '',
    errorName: '',
    errorPhone: '',
    errorEmain: ''
  };

  checkErrors = () => {
    this.success = true;

    this.info.errorName = this.info.name === '' ? 'Поле должно быть заполнено' : '';
    this.info.errorPhone = this.info.phone.length < 17 ? 'Поле должно быть заполнено полностью' : '';
    this.info.errorEmail = this.info.phone.length < 17 ? 'Поле должно быть заполнено полностью' : '';

    if (this.info.errorName !== '' || this.info.errorPhone !== '' || this.info.errorEmail !== '') {
      this.success = false;
    }
  };

  @observable attributes = {
    iconSize: 30,
    isUpdating: this.isUpdating,
    info: this.info,
    preselect: this,
    checkErrors: () => this.checkErrors(),
    back: () => {
      this.isUpdating = true;
      this.step--;
      this.reset();
      this.isUpdating = false;
    },
    next: () => {
      this.isUpdating = true;
      this.reset();
      if (this.isLastStep) {
        this.checkErrors();
        if (this.success) {
          this.success = false;

          superAgent.email({
            to: this.info.email,
            subject: `${this.countryName}, ${this.regionName} - ${this.info.nights} ночей, ${this.info.peoples} чел.`,
            body: `
          Имя: ${this.info.name}<br/><br/>
          Телефон: ${this.info.phone}<br/><br/>
          Email: <a href="mailto:${this.info.email}">${this.info.email}</a><br/><br/>
          Откуда: ${this.departureName}<br/><br/>
          Куда: ${this.countryName}, ${this.regionName}<br/><br/>
          Даты вылета: ${this.flyDates}<br/><br/>
          Ночей: ${this.info.nights}<br/><br/>
          Взрослых: ${this.info.peoples}<br/><br/>
          Детей: ${this.info.childrens}<br/><br/>
          `
          })
            .then((res) => {
              if (res && res.status && res.status === 'ok') {
                this.steps.push(this.thank);
                this.step++;
              }
              this.isUpdating = false;
            });
        }
      } else {
        this.step++;
      }
      this.isUpdating = false;
    }
  };


  @computed get departureName() {
    return this.departureList ? this.departureList.find((itm) => itm.value == this.info.departure).title : '';
  }

  @computed get countryName() {
    return this.countryList ? this.countryList.find((itm) => itm.value == this.info.country).title : '';
  }

  @computed get regionName() {
    return this.regionList ? this.regionList.find((itm) => itm.value == this.info.region).title : '';
  }

  @computed get flyDates() {
    const from = (new Date(this.info.date.from));
    const to = (new Date(this.info.date.to));

    return `${from.getDate()}.${from.getMonth()}.${from.getFullYear()} - ${to.getDate()}.${to.getMonth()}.${to.getFullYear()}`;
  }

  reset() {
    this.steps.remove(this.thank);
  }

  @observable steps = [
    new Departure(this.attributes),
    new Country(this.attributes),
    new FlyDate(this.attributes),
    new Nights(this.attributes),
    new Peoples(this.attributes),
    new ContactsData(this.attributes)
  ];

  @computed get isLastStep() {
    return this.steps.length === this.step + 1;
  }

  @computed get getCurrentStep() {
    return this.steps[this.step];
  }

  view = (props) => this.getCurrentStep.view(props);
}

const preselect = new Preselect();

@observer
export class PreselectOutput extends Component {
  render() {
    return preselect.view(this.props);
  }
}

export {
  preselect
};
