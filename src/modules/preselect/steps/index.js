export {Thanks} from './thankyou';
export {Departure} from './departure';
export {Country} from './country';
export {FlyDate} from './flydates';
export {Nights} from './nights';
export {Peoples} from './peoples';
export {ContactsData} from './contacts';
