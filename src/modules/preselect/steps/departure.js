import React from 'react';
import {Step} from './../step';

import {Select} from './../../../components';
import superAgent from '../../../utils/superAgent';


export class Departure {
  constructor(attributes) {
    this.attributes = attributes;
    this.icon = (
      <svg width={this.attributes.iconSize} height={this.attributes.iconSize} viewBox="0 0 485.213 485.212">
        <path d="M242.606,0C142.124,0,60.651,81.473,60.651,181.955s151.631,303.257,181.956,303.257 c30.326,0,181.955-202.775,181.955-303.257S343.089,0,242.606,0z M242.606,303.257c-66.9,0-121.302-54.433-121.302-121.302 S175.706,60.651,242.606,60.651c66.902,0,121.302,54.435,121.302,121.304S309.509,303.257,242.606,303.257z"/>
      </svg>
    );
  }

  stepName = 'Откуда';

  attributes = null;

  view = (props) => <DepartureView service={this} {...props} {...this.attributes}/>;
}

class DepartureView extends Step {
  constructor(props) {
    super(props);
    if (props.store && props.store.departures) {
      this.info.departureList = props.store.departures.map((itm) => ({value: itm.id, title: itm.name}));
      this.info.departure = 1;
    } else {
      superAgent.form({type: 'departure'})
        .then((data) => {
          if (data) {
            if (data.departures) {
              this.info.departureList = data.departures.map((itm) => ({value: itm.id, title: itm.name}));
              this.info.departure = 1;
            }
          }
        });
    }
  }

  onSelectCity = (val) => {
    this.info.departure = val;
  };

  view = () => (
    <Select
      theme={this.props.theme}
      className="bottom70"
      placeholder="Город вылета"
      size={10}
      onChange={this.onSelectCity}
      options={this.info.departureList}
      value={this.info.departure}
    />
  );
}
