import React from 'react';
import {Step} from './../step';

import {AutoComplete, Docker} from './../../../components';
import superAgent from '../../../utils/superAgent';


export class Country {
  constructor(attributes) {
    this.attributes = attributes;
    this.icon = (
      <svg width={this.attributes.iconSize} height={this.attributes.iconSize} viewBox="0 0 1000 1000">
        <path d="M500,990c-170.5,0-342.5-46.2-342.5-149.5c0-80.6,117.8-130.7,227.3-143l6-0.7L239.5,454.5c-4.9-8.7-9.5-18-13.5-27.4l-4-8.1l-0.6-3.2c-13.3-34.4-20-70.5-20-107.1C201.3,144,335.3,10,500,10s298.7,134,298.7,298.7c0,36.7-6.8,72.7-20.1,107.1l-2,5.2h0.2l-1.5,3.2c-4.4,10.3-9.2,20.1-14.4,29.5l-261,433.2l-61.8-101l-2.3,0.2c-100.1,8.6-164,36.2-180.8,51.6l-3,2.8l3,2.8c21.8,20.3,105.9,52.4,239,53.3l11.9,0l0,0c132.9-0.9,217.1-33,239.1-53.2l3.1-2.9l-3.2-2.8c-12.2-10.8-56.8-32.2-125.3-44.7l52.6-84.2c109.8,24.7,170.2,71.4,170.2,131.7C842.5,943.8,670.5,990,500,990z M500,147.6c-70.2,0-127.3,57.2-127.3,127.4c0,70.2,57.1,127.3,127.3,127.3c70.3,0,127.4-57.1,127.4-127.3C627.4,204.7,570.2,147.6,500,147.6z"/>
      </svg>
    );
  }

  stepName = 'Куда';

  attributes = null;

  view = (props) => <CountryView service={this} {...props} {...this.attributes}/>;
}

class CountryView extends Step {
  constructor(props) {
    super(props);

    const store = props.store ? props.store.search : null;

    if (store && store.countries && store.regions) {
      this.info.countryList = store.countries.map((itm) => ({value: itm.id, title: itm.name, visa: itm.visa}));
      this.info.regionList = store.regions.map((itm) => ({value: itm.id, title: itm.name}));

      this.info.region = this.info.regionList[0] ? this.info.regionList[0].value : null;
    } else {
      this.updateForm('country,region').then((data) => {
        if (data.countries) {
          this.info.countryList = data.countries.map((itm) => ({value: itm.id, title: itm.name, visa: itm.visa}));
        }

        if (data.regions) {
          this.info.regionList = data.regions.map((itm) => ({value: itm.id, title: itm.name}));
          this.info.region = this.info.regionList[0] ? this.info.regionList[0].value : null;
        }
      });
    }
  }

  updateForm = (type = 'country') => new Promise((resolve, reject) => {
    superAgent.form({
      type,
      departure: this.info.departure,
      ...(this.info.country && {country: this.info.country}),
      ...(this.info.country && {cndep: this.info.country})
    })
      .then(resolve)
      .catch(reject);
  });

  update = () => this.updateForm('region').then((dataRegions) => {
    if (dataRegions.regions) {
      this.info.regionList = dataRegions.regions.map((itm) => ({value: itm.id, title: itm.name}));
      this.info.region = this.info.regionList[0] ? this.info.regionList[0].value : null;
    }
  });

  onSelectCity = (val) => {
    this.info.country = val;
    this.update();
  };

  onSelectCurort = (val) => {
    this.info.region = val;
  };

  view(props) {
    const foundCountry = this.info.countryList.find((i) => i.value == this.info.country);
    const foundRegion = this.info.regionList.find((i) => i.value == this.info.region);

    return (
      <Docker className="bottom70">
        <AutoComplete
          theme={this.props.theme}
          className="bottom20"
          flags
          default={this.info.countryList}
          onChange={(val) => this.onSelectCity(val.id)}
          placeholder="Введите название страны куда вы хотите поехать"
          {...(foundCountry && {value: foundCountry.title})}
        />
        <AutoComplete
          theme={this.props.theme}
          className="bottom20"
          placeholder="Выберите курорт"
          default={this.info.regionList}
          onChange={(val) => this.onSelectCurort(val.id)}
          bordered
          {...(foundRegion && {value: foundRegion.title})}
        />
      </Docker>
    );
  }
}
