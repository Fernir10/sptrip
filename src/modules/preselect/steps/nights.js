import React from 'react';
import {Step} from './../step';

import {Range} from './../../../components';

export class Nights {
  constructor(attributes) {
    this.attributes = attributes;
    this.icon = (
      <svg width={this.attributes.iconSize} height={this.attributes.iconSize} viewBox="0 0 1000 1000">
        <path d="M500,10C229.8,10,10,229.8,10,500c0,270.2,219.8,490,490,490c270.2,0,490-219.8,490-490C990,229.8,770.2,10,500,10z M500,910.2c-226.2,0-410.2-184-410.2-410.2c0-226.2,184-410.2,410.2-410.2c226.2,0,410.2,184,410.2,410.2C910.2,726.1,726.2,910.2,500,910.2z M753.1,374c8.2,11.9,5.2,28.1-6.6,36.3L509.9,573.7c-4.4,3.1-9.6,4.6-14.8,4.6c-4.1,0-8.3-1-12.1-3c-8.6-4.5-14-13.4-14-23.1V202.5c0-14.4,11.7-26.1,26.1-26.1c14.4,0,26.1,11.7,26.1,26.1v300l195.6-135.1C728.7,359.2,744.9,362.1,753.1,374z"/>
      </svg>
    );
  }

  stepName = 'На сколько ночей?';


  attributes = null;

  view(props) {
    return <NightsView {...props} service={this} {...this.attributes}/>;
  }
}

const nightsTrans = {
  1: 'ночь',
  2: 'ночи',
  3: 'ночи',
  4: 'ночи'
};

class NightsView extends Step {
  view(props) {
    return (
      <Range
        theme={this.props.theme}
        className="bottom70"
        size="thin"
        onChange={(val) => this.info.nights = val}
        value={this.info.nights}
        text={`${this.info.nights} ${nightsTrans[this.info.nights] || 'ночей'}`}
        bordered
      />
    );
  }
}
