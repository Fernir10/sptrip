import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {Helmet} from 'react-helmet';
import {computed} from 'mobx';
import cn from 'classnames';
import ReactTooltip from 'react-tooltip';
import {Link} from 'react-router-dom';

import {ModifyColor} from './../../utils/color';

import {Breadcrumb, Button, Docker, Spinner} from '../../components';

@observer
export class Step extends Component {
  @computed get info() {
    return this.props.info;
  }

  changeStep(step) {
    const {preselect} = this.props;
    preselect.reset();
    preselect.step = step;
  }

  renderPrev() {
    return 'Назад';
  }

  renderNext() {
    return 'Следующий шаг';
  }

  icon = null;

  view(props) {
    this.props = props;
    return null;
  }

  render() {
    const {theme, preselect, back, next} = this.props;

    return (
      <div className="container-md">
        <Helmet>
          <title>{preselect.getCurrentStep.stepName}</title>
          <meta property="og:title" content={`SPTRIP.RU | ${preselect.getCurrentStep.stepName}`}/>
          <meta name="description" content="SPTRIP.RU | Модуль мгновенного расчета тура в несколько шагов"/>
        </Helmet>
        <div className="top20">
          <Breadcrumb bg={theme.blueColor} hover={ModifyColor(theme.blueColor, 10)} text={theme.whiteTextColor}>
            <Link to="/">На главную</Link>
            <span>Расчет тура</span>
          </Breadcrumb>
        </div>
        <div className="preselect-between">
          <Docker className="preselect-header">
            {
              preselect.steps.map((step, index) => (
                <div
                  key={`icon-${step.stepName}`}
                  className={cn('item', {select: preselect.getCurrentStep === step})}
                  onClick={() => this.changeStep(index)}
                  data-for="step"
                  data-tip={step.stepName}
                >{step.icon || step.stepName}</div>
              ))
            }
          </Docker>
          <ReactTooltip id="step" type="info"/>
        </div>
        <h1>{preselect.getCurrentStep.stepName}</h1>
        {preselect.isUpdating ? <Spinner/> : this.view()}
        <Docker className="bottom20">
          {(preselect.step > 0) && (
            <Button className="prev" color="#477ab9" onClick={back}>
              {this.renderPrev()}
            </Button>
          )}
          <Button className="next" onClick={next}>
            {this.renderNext()}
          </Button>
        </Docker>
        <div className="nomobile text-center top20">Телефон для частных лиц<br/>
          <b>+7 (495) 744-33-17</b></div>
        <div className="bottom20"/>
      </div>
    );
  }
}
