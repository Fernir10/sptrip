import React, {Fragment} from 'react';
import PropTypes from 'prop-types';

import {Img} from './../../components';
import {ErrorIcon} from './../../icons';

import './search.less';

const NotFound = () => (
  <Fragment>
    <h3>По заданным фильтрам туры не найдены</h3>
    <p>Попробуйте изменить параметры фильтров (другие курорты, питание, рейтинг + класс отеля и т.д.)</p>
  </Fragment>
);

const ErrorMessage = (props) => (
  <div className="error--message">
    <h2>Произошла ошибка</h2>
    <div className="flex">
      <ErrorIcon style={{marginRight: 10}}/>
      <span dangerouslySetInnerHTML={{__html: props.message}}/>
    </div>
    <Img src="/img/404-girl.png" style={{position: 'absolute', bottom: 0}}/>
  </div>
);

ErrorMessage.propTypes = {
  message: PropTypes.string
};

export {
  NotFound,
  ErrorMessage
};
