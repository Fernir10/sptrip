import React, {Fragment, PureComponent} from 'react';
import styled from 'styled-components';

import {Docker, Img} from './../../../components';
import {RightArrowIcon} from './../../../icons';
import {parseDateFull} from './../../../utils/format';

import './avia.less';

const StyledCharterLine = styled.div`
  background: ${(props) => props.theme.whiteBackgroundColor || '#fff'};
`;

class OneWay extends PureComponent {
  render() {
    const {
      theme,
      company: {thumb, name: companyname} = {},
      departure: {port: {name: portname} = {}, time: departuretime} = {},
      arrival: {port: {name: arrivalportname} = {}, time: arrivaltime} = {},
      datebackward,
      dateforward,
      number,
      plane,
      arrow
    } = this.props;

    return (
      <StyledCharterLine theme={theme} className="charter--line">
        <Docker className="charter--way">
          {thumb && (
            <div className="charter--icon">
              <Img
                src={thumb}
                title={companyname}
                alt={companyname}
              />
            </div>
          )}
          <div>{parseDateFull(dateforward || datebackward)}</div>
          <div><b>{portname} - {arrivalportname}</b></div>
          <div>{departuretime} - {arrivaltime}</div>
          <div>{plane} {number}</div>
        </Docker>
        {arrow && (
          <RightArrowIcon className="charter--arrow"/>
        )}
      </StyledCharterLine>
    );
  }
}

class Flight extends PureComponent {
  render() {
    return (
      <div className="charter">
        <div className="charter--block">
          {this.props.forward.map((frw, index) => (
            <OneWay
              theme={this.props.theme}
              key={`oneway-${index}`}
              arrow={this.props.forward.length > 1 && index < this.props.forward.length - 1}
              dateforward={this.props.dateforward}
              {...frw}
            />
          ))}
          <RightArrowIcon className="charter--arrow"/>
          {this.props.backward.map((bwd, index) => (
            <OneWay
              theme={this.props.theme}
              key={`oneway-bwd-${index}`}
              arrow={this.props.forward.length > 1 && index < this.props.backward.length - 1}
              datebackward={this.props.datebackward}
              {...bwd}
            />
          ))}
        </div>
      </div>
    );
  }
}

export class Avia extends PureComponent {
  render() {
    return (
      <Fragment>
        {
          this.props.flights.map((flight, i) => <Flight theme={this.props.theme} key={i} {...flight}/>)
        }
      </Fragment>
    );
  }
}

