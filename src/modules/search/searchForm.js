import React, {Component, Fragment} from 'react';
import {observer} from 'mobx-react';
import {computed, observable} from 'mobx';
import geolocator from 'geolocator';
import cn from 'classnames';
import styled from 'styled-components';

import superAgent from './../../utils/superAgent';

import {
  AutoComplete,
  Button,
  Calendar,
  Docker,
  PeopleSelect,
  Select,
  SelectGroup,
  StarsSelector
} from './../../components';

import {CloseIcon} from '../../icons';

const StyledOptionsPane = styled.div`
  @media only screen and (max-device-width: 1024px) {
    ${(props) => (props.visible ? `background-color: ${props.theme.backgroundColor};` : '')}
  }
`;

const WarningCard = styled.div`
    border-left: 6px solid ${(props) => props.theme.orangeColor};
    background: ${(props) => props.theme.whiteBackgroundColor};
    color: ${(props) => props.theme.color};
    border-radius: 2px;
    opacity: 0;
    max-height: 0;
    padding: 0 20px;
    visibility: hidden;
    pointer-events: none;
    transition: all .2s ease;
    margin: 0;
    
    ${(props) => (props.visible ? `
      padding: 10px 20px;
      max-height: 300px;
      margin-bottom: 10px;
      opacity: 1;
      visibility: visible;
      pointer-events: all;
    ` : '')}
`;

@observer
export class SearchForm extends Component {
  @observable.shallow hotelList = [];
  @observable.shallow departureList = [];
  @observable.shallow countryList = [];
  @observable.shallow regionList = [];
  @observable.shallow subRegionList = [];
  @observable.shallow mealList = [];

  @observable query = null;
  @observable showFilters = false;

  updateData = (type, props) => new Promise((resolve, reject) => {
    superAgent.form({
      type: type || 'departure,country,hotel,region,meal',
      departure: props.departure,
      cndep: props.departure,
      ...(props.country && {country: props.country}),
      ...(props.country && {hotcountry: props.country}),
      ...(props.country && {regcountry: props.country})
    })
      .then(({departures, countries, regions, subregions, hotels, meals}) => {
        if (departures) {
          this.departureList = departures;
        }
        if (countries) {
          this.countryList = countries.map((itm) => ({value: itm.id, title: itm.name, visa: itm.visa}));
        }
        if (regions) {
          this.regionList = regions;
        }
        if (subregions) {
          this.subRegionList = subregions;
        }
        if (hotels) {
          this.hotelList = hotels;
        }
        if (meals) {
          this.mealList = meals.map((itm) => ({value: itm.id, title: itm.russian}));
        }
        resolve();
      }).catch(() => {
        reject();
      });
  });

  constructor(props) {
    super(props);
    const {search} = props;

    if (search && search.country == this.props.country) {
      const {departures, countries, regions, subregions, hotels, meals} = search;

      if (departures) {
        this.departureList = departures;
      }
      if (countries) {
        this.countryList = countries.map((itm) => ({value: itm.id, title: itm.name, visa: itm.visa}));
      }
      if (regions) {
        this.regionList = regions;
      }
      if (subregions) {
        this.subRegionList = subregions;
      }
      if (hotels) {
        this.hotelList = hotels;
      }
      if (meals) {
        this.mealList = meals.map((itm) => ({value: itm.id, title: itm.russian}));
      }
    } else {
      this.updateData(null, this.props);
    }
  }

  updateDeparture = (callback = () => {}) => {
    if (!this.props.departure) {
      geolocator.config({
        language: 'ru',
        google: {
          version: '3',
          key: 'AIzaSyABL23TE0nj1_FacJI3VwBeImGbO97Igqo'
        }
      });

      geolocator.locateByIP({
        addressLookup: true
      }, (err, location) => {
        if (location && location.address && location.address.city) {
          const lookDeparture = this.departureList.find((itm) => itm.name.toString() == location.address.city);
          if (lookDeparture) {
            this.props.setDeparture(lookDeparture.id);
            if (callback) {
              callback();
            }
          }
        }
      });
    }
  };

  componentWillMount() {
    this.updateDeparture(() => {
      if (this.departureList.length === 0 || (this.props.country && this.hotelList.length === 0)) {
        this.updateData(null, this.props);
      }
    });
  }

  componentWillReceiveProps(newProps) {
    if (newProps.country != this.props.country) {
      this.updateData('hotel,region,subregion', {country: newProps.country});
    }
  }

  @computed get
  filledRegions() {
    if ((!this.props.region && this.countryName)) {
      return this.regionList.map((r) => r.id).join(',');
    }

    return null;
  }

  onSelectDeparture = (val) => {
    this.updateData(null, {departure: val, country: this.props.country});
    this.props.setDeparture(val);
  };

  onSelectTo = (val) => {
    if (val && val.id) {
      this.query = val;

      this.props.setCountry(val.country || val.id);

      if (val.rating) {
        if (val.region) {
          this.props.setRegion(val.region);
        }
        this.props.setQueryHotel(val.id);
      } else if (val.country && val.id.toString() !== val.country.toString()) {
        this.props.setRegion(val.id);
      }
    }
  };

  @computed get
  countryName() {
    const {country} = this.props;

    return this.countryList.find((c) => c.value == country);
  }

  @computed get
  hotelName() {
    const {queryhotel} = this.props;

    return queryhotel ? this.hotelList.find((c) => (c.id || '').toString() === (queryhotel || '').toString()) : null;
  }

  renderTopBlock = () => {
    const {
      theme,
      full,
      autosearch,
      hideDeparture,
      hideCountry,
      queryhotel,
      departure,
      country,
      dates: {from, to} = {},
      adults,
      child,
      childage1,
      childage2,
      childage3,
      stars,
      setCountry,
      setDates,
      nights: {from: nightsFrom} = {},
      setNights,
      setPeoples,
      setStars,
      startSearch
    } = this.props;

    const showParams = (!queryhotel && !!country);
    const showParamsHotel = showParams || queryhotel;

    return (
      <Docker>
        {(showParams && !hideDeparture) && (
          <Select
            theme={theme}
            bordered
            placeholder="Откуда"
            options={this.departureList.map((dep) => ({value: dep.id, title: dep.name}))}
            onChange={this.onSelectDeparture}
            value={departure}
          />
        )}
        {!hideCountry && (
          <AutoComplete
            flags
            theme={theme}
            res="/api/tv/all/"
            className="bordered"
            {...(!full && {title: 'Куда'})}
            departure={departure}
            default={this.countryList}
            onChange={this.onSelectTo}
            onRemove={() => setCountry(null)}
            placeholder="Введите название страны, отеля или города"
            value={this.hotelName ? this.hotelName.name : this.countryName ? this.countryName.title : ''}
          />
        )}
        {showParamsHotel && (
          <Calendar
            theme={theme}
            dateFrom={from}
            dateTo={to}
            onChange={setDates}
          />
        )}
        {showParamsHotel && (
          <Select
            theme={theme}
            bordered
            placeholder="Ночей"
            options={Array(29).fill(0).map((a, i) => ({title: i, value: i}))}
            onChange={(val) => setNights({from: val, to: val + 2})}
            value={nightsFrom}
          />
        )}
        {showParamsHotel && (
          <PeopleSelect
            theme={theme}
            placeholder="Туристы"
            adults={adults}
            child={child}
            childage1={childage1}
            childage2={childage2}
            childage3={childage3}
            onSelectPeoples={setPeoples}
          />
        )}
        {showParamsHotel && (
          <StarsSelector
            theme={theme}
            bordered
            onChange={setStars}
            value={stars}
            size={20}
            placeholder="Класс отеля от:"
          />
        )}
        {(showParams && !autosearch) && (
          <Button
            theme={theme}
            color="#fff"
            className="search--filters mobile"
            onClick={() => { this.showFilters = !this.showFilters; }}
          >
              Фильтры
          </Button>
        )}
        {showParamsHotel && (
          <Button
            theme={theme}
            onClick={() => startSearch(this.filledRegions)}
          >
              Искать
          </Button>
        )}
      </Docker>
    );
  };


  render() {
    const showParams = (!this.props.queryhotel && !!this.props.country && !this.props.autosearch);

    const {
      theme,
      queryhotel,
      rating,
      setRating,
      hideRegions,
      meal,
      setMeal,
      region,
      setRegion,
      beach,
      setBeach,
      hotel,
      setHotel,
      operatorList = [],
      operator,
      setOperator,
      children
    } = this.props;

    return (
      <Fragment>
        {this.renderTopBlock()}
        {showParams && (
          <div className={cn('search')}>
            <StyledOptionsPane theme={theme} className={cn('search-left', {'search-left--show': this.showFilters})} visible={this.showFilters}>
              <div className="search-left--close" onClick={() => { this.showFilters = !this.showFilters; }}>
                <CloseIcon
                  theme={theme}
                  color={theme.color}
                  size={12}
                />
              </div>
              {!!operatorList.length && (
                <Fragment>
                  <StarsSelector
                    theme={theme}
                    placeholder="Рейтинг от:"
                    className="bottom10"
                    selected="gold"
                    shadowed
                    size={20}
                    onChange={setRating}
                    value={rating}
                  />
                  <Select
                    theme={theme}
                    className="bottom10"
                    placeholder="Тип питания"
                    onChange={setMeal}
                    options={[{value: null, title: 'Любой'}, ...this.mealList]}
                    value={meal}
                  />
                </Fragment>
              )}
              {!hideRegions && (
                <Fragment>
                  <WarningCard
                    theme={theme}
                    visible={(!region && this.countryName)}
                  >
                    Внимание: не выбран ни один курорт, в выдаче неполная подборка туров!
                  </WarningCard>
                  <SelectGroup
                    theme={theme}
                    className="bottom10"
                    label="Курорт"
                    options={this.regionList || []}
                    onChange={setRegion}
                    value={region}
                  />
                </Fragment>
              )}
              {!!operatorList.length && (
                <SelectGroup
                  theme={theme}
                  className="bottom10"
                  label="Пляж"
                  options={[
                    {name: 'Песчаный', id: 1},
                    {name: 'Галечный', id: 2},
                    {name: 'Коралловый', id: 3},
                    {name: 'Муниципальный', id: 4},
                    {name: 'Городской', id: 5},
                    {name: 'Собственный', id: 6}
                  ]}
                  onChange={setBeach}
                  value={beach}
                />
              )}
              <SelectGroup
                theme={theme}
                search
                selected
                className="bottom10"
                label="Отель"
                label2="Рейтинг"
                options={this.hotelList}
                onChange={setHotel}
                value={hotel}
              />
              {!!operatorList.length && (
                <SelectGroup
                  theme={theme}
                  label="Туроператоры"
                  label2="Статус"
                  options={operatorList}
                  onChange={setOperator}
                  value={operator}
                />
              )}
            </StyledOptionsPane>
            <div className="search-right">
              {children}
            </div>
          </div>
        )}
        {!!queryhotel && (
          <div className="search--hotel">{children}</div>
        )}
      </Fragment>
    );
  }
}
