import React, {Fragment, PureComponent} from 'react';
import {Link} from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import styled from 'styled-components';

import {Arrow, Button, Img, Stars, Swiper} from './../../../components';
import {Tours} from './../tour';
import {parseDate, formatPrice} from './../../../utils/format';

import {AviaIcon, CondeyIcon, CoralIcon, LineIcon, SandIcon, StoneIcon, WifiIcon} from './../../../icons';

import './hotel.less';

const StyledLink = styled(Link)`color: ${(props) => props.theme.color || '#477ab9'}; text-decoration: none;`;
const StyledAviaIcon = styled(AviaIcon)`background: ${(props) => props.theme.orangeLightColor || 'orange'};`;
const StyledCondeyIcon = styled(CondeyIcon)`background: ${(props) => props.theme.orangeLightColor || 'orange'};`;
const StyledCoralIcon = styled(CoralIcon)`background: ${(props) => props.theme.orangeLightColor || 'orange'};`;
const StyledLineIcon = styled(LineIcon)`background: ${(props) => props.theme.orangeLightColor || 'orange'};`;
const StyledSandIcon = styled(SandIcon)`background: ${(props) => props.theme.orangeLightColor || 'orange'};`;
const StyledStoneIcon = styled(StoneIcon)`background: ${(props) => props.theme.orangeLightColor || 'orange'};`;
const StyledWifiIcon = styled(WifiIcon)`background: ${(props) => props.theme.orangeLightColor || 'orange'};`;

const StyledWrapper = styled.div`
  background-color: ${(props) => props.theme.whiteBackgroundColor};
  color: ${(props) => props.theme.color};
`;

export class Hotel extends PureComponent {
  state = {
    open: false
  };

  toggleInfo = () => {
    this.setState({open: !this.state.open});
  };

  componentDidMount() {
    ReactTooltip.rebuild();
  }

  render() {
    const {theme, images = [], beachtypes: {sand, stone, coral} = {}, wifi, airport, condey, line1, line2, line3} = this.props;
    const [{original: firstImage} = {}] = images;

    const {
      picturelink,
      hotelname,
      hotelrating,
      operators,
      mealcode,
      tours: {tour = []} = {},
      hotelcode,
      hoteldescription,
      currency,
      hotelstars,
      countryname,
      regionname,
      subregionname,
      price
    } = this.props;

    return (
      <StyledWrapper theme={theme} className="hotel--wrapper">
        <div className="hotel">
          <div className="flex-stretch">
            {hotelrating !== 0 && (
              <div className="hotel--rating">{hotelrating.slice(0, 3)}</div>
            )}
            {firstImage ? (
              <Swiper
                theme={theme}
                className="hotel--image"
                inner
                height={300}
                color="#0f3"
                fullscreen
              >
                {images.map((img) => (
                  <Img
                    key={img.original}
                    src={img.original}
                    alt={hotelname}
                  />
                ))}
              </Swiper>
            ) : (
              <Img
                lazy={!!hotelcode}
                className="hotel--image"
                src={firstImage || (picturelink.replace(/small/, 'medium') || '')}
                alt={hotelname}
              />
            )}
          </div>
          <div className="hotel--description flex-vertical-between">
            <h4 className="nomargin">
              <StyledLink
                theme={theme}
                to={{
                  pathname: `/hotel/${hotelcode}`,
                  state: {
                    operators,
                    mealcode,
                    tours: tour,
                    hotelcode
                  }
                }}
              >
                {hotelname} {hotelstars !== 0 && (
                  <Fragment>
                    {hotelstars}
                    <Stars style={{marginLeft: 10}} max={1} size={20} color="gold"/>
                  </Fragment>
                )}
              </StyledLink>
            </h4>
            {hotelcode ? (
              <Fragment>
                <div className="muted bottom10">{countryname}, {regionname}{subregionname ? `, ${subregionname}` : ''}</div>
                <div className="bottom10">с {parseDate(tour[0].flydate)} на {tour[0].nights} ночей</div>
              </Fragment>
            ) : (
              <Fragment>
                <div className="muted bottom10">...</div>
                <div className="muted bottom10">...</div>
              </Fragment>
            )}
            {hoteldescription && <span>{hoteldescription}</span>}
            <div className="flex-end-align flex-between">
              <div className="flex top10">
                {sand && <StyledSandIcon data-tip="Песок" color={theme.color} theme={theme} className="hotel--icon"/>}
                {stone && <StyledStoneIcon data-tip="Галька" color={theme.color} theme={theme} className="hotel--icon"/>}
                {coral && <StyledCoralIcon data-tip="Кораллы" color={theme.color} theme={theme} className="hotel--icon"/>}
                {wifi && <StyledWifiIcon data-tip="Wi-fi" color={theme.color} theme={theme} className="hotel--icon"/>}
                {airport && <StyledAviaIcon data-tip={`До аэропорта ${airport} км`} color={theme.color} theme={theme} className="hotel--icon" text={`${airport} км.`}/>}
                {condey && <StyledCondeyIcon data-tip="Кондиционер" color={theme.color} theme={theme} className="hotel--icon"/>}
                {line1 && <StyledLineIcon className="hotel--icon" color={theme.color} theme={theme} text="1 линия"/>}
                {line2 && <StyledLineIcon className="hotel--icon" color={theme.color} theme={theme} text="2 линия"/>}
                {line3 && <StyledLineIcon className="hotel--icon" color={theme.color} theme={theme} text="3 линия"/>}
                <ReactTooltip type="info"/>
              </div>
              <Button theme={theme} onClick={this.toggleInfo} color="#fff">
                {hotelcode ? `от ${formatPrice(price)} ${currency === 'RUB' ? 'руб' : currency}` : '...'}
                {!!hotelcode && (
                  <Arrow isOpened={this.state.open} size={10} style={{marginLeft: 10}}/>
                )}
              </Button>
            </div>
          </div>
        </div>
        {this.state.open && (
          <Tours
            theme={theme}
            operators={operators}
            mealcode={mealcode}
            tours={tour}
            hotelcode={hotelcode}
          />
        )}
      </StyledWrapper>
    );
  }
}
