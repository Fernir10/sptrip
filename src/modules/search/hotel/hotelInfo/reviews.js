import React, {Fragment, PureComponent} from 'react';
import cn from 'classnames';
import styled from 'styled-components';

import {DropLink} from '../../../../components';

import './reviews.less';

const StyledCat = styled.div`
  &:before {
    background: linear-gradient(to top, ${(props) => props.theme.whiteBackgroundColor || '#fff'} 0%, transparent 100%);
  }
`;

class Review extends PureComponent {
  state = {
    open: false,
    shouldOpen: false
  };

  initialHeight = 0;

  componentDidMount() {
    this.initialHeight = this.description.offsetHeight;

    if (this.initialHeight > 90) {
      this.setState({shouldOpen: true});
      this.description.classList.add('review--cat--hidden');
      this.description.style.position = 'absolute';
    }
  }

  render() {
    const {theme, name, traveltime, sourcelink, rate, content = ''} = this.props;

    return (
      <Fragment>
        <div className="review--header">
          <b>{name || 'unknown'}</b>
          {traveltime && (
            <span>&nbsp;был(а) в отеле {traveltime}</span>
          )}
          <a target="_blank" href={sourcelink}>источник</a>
        </div>
        <p><b>Оценка: {rate} из 10</b></p>
        <div
          className="review--cat"
          ref={(node) => { this.description = node; }}
          dangerouslySetInnerHTML={{__html: content}}
        />
        {this.state.shouldOpen && (
          <Fragment>
            <StyledCat
              theme={theme}
              className={cn('review--cat', {'review--cat--open': this.state.open, 'review--cat--close': !this.state.open})}
              style={{maxHeight: this.state.open ? this.initialHeight + 10 : 90}}
              dangerouslySetInnerHTML={{__html: content}}
            />
            <DropLink
              theme={theme}
              open={this.state.open}
              onClick={() => this.setState({open: !this.state.open})}
            >
              {this.state.open ? 'Свернуть' : 'Развернуть'}
            </DropLink>
          </Fragment>
        )}
      </Fragment>
    );
  }
}

export const Reviews = ({theme, hotelcode, reviews: {review = []} = {}}) => review.map((r, i) => (
  <Review theme={theme} key={`${hotelcode}-${i}`} {...r}/>
));
