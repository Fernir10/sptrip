import React, {Fragment, PureComponent} from 'react';
import {Helmet} from 'react-helmet';
import cn from 'classnames';
import styled from 'styled-components';

import {GMap, Swiper} from '../../../../components/index';

import './hotelinfo.less';

export {Reviews} from './reviews';

const StyledHash = styled.p`
  display: inline-block;
  box-shadow: 0 0 0 1px ${(props) => props.theme.borderColor};
  margin: 5px;
  padding: 5px;
  border-radius: 2px;
`;

const parseText = (text, theme) => {
  const textToParse = typeof text === 'string' ? text : Object.keys(text).map((key) => `${key} ${text[key]}`).join(' ');

  const parsedText = (textToParse || '')
    .replace(/<(?:.|\n)*?>/gim, '')
    .replace(/\(платно \)/gim, '$')
    .replace(/ \(бесплатно\)/gim, '')
    .replace(/ {2}/gim, ' ')
    .replace(/&nbsp;/gim, ' ')
    .replace(/&ndash;/gim, '-')
    .replace(/&ndash,/gim, '-')
    .replace(/; /gim, ';')
    .replace(/ ;/gim, ';')
    .replace(/;/gim, '; ')
    .replace(/(?:\r\n|\r|\n)/g, '')
    .split(/[; ][: ]/gi);

  return parsedText.map((itm) => (
    itm[itm.length - 1] === ':' ? (
      <p key={itm}><b>{itm}</b></p>
    ) : (
      <StyledHash theme={theme} key={itm}>{itm}</StyledHash>
    )
  ));
};


export class HotelInfo extends PureComponent {
  renderServices() {
    const {theme, services, servicefree, servicepay} = this.props;

    if (services) {
      return (<Fragment>
        <p><b>Услуги:</b></p>
        {parseText(services, theme)}
      </Fragment>);
    } else if (servicefree && servicepay) {
      return (<Fragment>
        <Fragment>
          <p><b>Бесплатные услуги:</b></p>
          {parseText(servicefree, theme)}
        </Fragment>
        <Fragment>
          <p><b>Платные услуги:</b></p>
          {parseText(servicepay, theme)}
        </Fragment>
      </Fragment>);
    }
    return null;
  }

  renderProps(props) {
    return props.filter((itm) => this.props[itm.prop])
      .map((itm, index) => (
        <Fragment key={`prop-${index}`}>
          <p><b>{itm.name}:</b></p>
          {parseText(this.props[itm.prop], this.props.theme)}
        </Fragment>
      ));
  }

  render() {
    const {theme, name, coord1, coord2, stars, country, placement, territory, inroom, images, build, repair, site, operators, mealcode, tours, hotelcode} = this.props;

    return (
      <Fragment>
        <Helmet>
          <meta name="description" content={`${name} ${stars}*, ${country} - ${placement || territory || inroom}`}/>
          <meta property="og:description" content={`${name} ${stars}*, ${country} - ${placement || territory || inroom}`}/>
        </Helmet>
        <div className="flex">
          {(images && images.map) && (
            <Swiper
              theme={theme}
              className={cn('hotelinfo--carousel', {'hotelinfo--carousel--full': !(coord1 && coord2)})}
              height={400}
              inner
              fullscreen
              images
              color="#0f4"
            >
              {images.map((img) => (
                <img key={img.original} src={img.original}/>
              ))}
            </Swiper>
          )}
          {(coord1 && coord2) && (
            <GMap
              coord1={coord1}
              coord2={coord2}
              style={{width: '50%', height: '400px', paddingLeft: 10}}
              className="desktop-only"
            />
          )}
        </div>
        <Fragment>
          {build && (
            <p className="muted">Открыт в {build} г.{repair && <span>, последний ремонт в {this.props.repair} г.</span>}</p>
          )}
          {this.renderProps([
            {name: 'Расположение', prop: 'placement'},
            {name: 'Территория', prop: 'territory'},
            {name: 'В номере', prop: 'inroom'},
            {name: 'Пляж', prop: 'beach'},
            {name: 'Детям', prop: 'child'},
            {name: 'Питание', prop: 'meallist'}
          ])}
          {this.renderServices()}
          {site && (
            <p><a target="_blank" rel="noopener noreferrer" href={site.match(/http/gi) ? site : `http://${site}`}>Перейти на страницу отеля</a></p>
          )}
        </Fragment>
      </Fragment>
    );
  }
}
