import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {observer} from 'mobx-react';
import {observable, toJS} from 'mobx';

import superAgent from './../../../utils/superAgent';
import {Tabs} from './../../../components';
import {Tours} from './../tour';

import {HotelInfo, Reviews} from './hotelInfo';

@observer
export class HotelFull extends Component {
  @observable.shallow hotelData = null;

  constructor(props) {
    super(props);
    const {match: {params: {hotelcode} = {}} = {}, store: {hotel: foundHotel} = {}} = props;

    if (foundHotel) {
      this.hotelData = foundHotel;
    } else {
      this.update(hotelcode);
    }
  }

  componentWillReceiveProps(newProps) {
    const {match: {params: {hotelcode} = {}} = {}} = newProps;

    if (hotelcode) {
      this.update(hotelcode);
    }
  }

  update = (hotelcode) => superAgent.hotel({hotelcode, reviews: 1})
    .then((hotel) => {
      this.hotelData = hotel;
    });

  render() {
    const tabsOptions = [];
    const {theme, notitle, location: {state} = {}} = this.props;

    if (this.hotelData) {
      const {reviews, name, stars} = this.hotelData;

      tabsOptions.push({
        title: 'Описание отеля',
        content: <HotelInfo theme={theme} {...toJS(this.hotelData)}/>
      });

      if (reviews) {
        tabsOptions.push({
          title: 'Отзывы туристов',
          content: <Reviews theme={theme} reviews={reviews}/>
        });
      }

      return (
        <Fragment>
          <Helmet>
            <title>{`Отель ${name} ${stars}* со скидкой ${(new Date()).getFullYear()}!`}</title>
            <meta property="og:title" content={`Отель ${name} ${stars}* со скидкой ${(new Date()).getFullYear()}!`}/>
          </Helmet>
          {this.hotelData && !notitle && (
            <h2>Отель {name} {stars}*</h2>
          )}
          {
            tabsOptions.length ? (
              <Tabs theme={theme} tabs={tabsOptions}/>
            ) : null
          }
          {(state && state.tours && this.hotelData) && (
            <Fragment>
              <h2>Предложения для этого отеля</h2>
              <Tours
                theme={theme}
                operators={this.hotelData.operators}
                mealcode={this.hotelData.mealcode}
                tours={state.tours}
                hotelcode={this.props.hotelcode}
              />
            </Fragment>
          )}
        </Fragment>
      );
    }

    return null;
  }
}

