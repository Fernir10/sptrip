import React, {Fragment} from 'react';
import io from 'socket.io-client';
import {computed, observable, toJS} from 'mobx';

import superAgent from './../../utils/superAgent';

import {calculateDate, formatDate} from './../../utils/format';

import {Pending, Progress} from './../../components';
import {ErrorMessage, NotFound} from './notFound';
import {HotelFull} from './../search/hotel/hotel';
import {Hotel} from './hotel';
import {Tours} from './tour';

import {SearchForm} from './searchForm';

import './search.less';

const beachRegex = [
  '',
  'песч',
  'гале',
  'корал',
  'муниц',
  'город',
  'собст'
];


const dummyHotels = Array(10).fill({
  hotelcode: 0,
  price: 0,
  hotelname: '...',
  hotelstars: 0,
  hotelrating: 0,
  hoteldescription: '...',
  picturelink: '/img/dummy/dummy-image.svg',
  fulldesclink: '...',
  reviewlink: '...',
  countrycode: 0,
  countryname: '...',
  regioncode: 0,
  regionname: '...',
  subregioncode: 0,
  subregionname: '...',
  isphoto: 1,
  isdescription: 1,
  isreviews: 1,
  iscoords: 0,
  currency: '...',
  tours: {
    tour: [{
      price: 0,
      nights: 0,
      operatorcode: 0,
      operatorname: '...',
      flydate: '...',
      placement: '...',
      adults: 0,
      child: 0,
      room: '...',
      tourname: '...',
      mealcode: 0,
      mealrussian: '...',
      meal: '...',
      tourid: 0,
      currency: '...',
      priceue: 0,
      visa: 0,
      fuelcharge: 0
    }]
  }
});

class Search {
  props = null;
  socket = null;
  pagesize = 10;
  body = null;
  pane = null;

  @observable status = null;
  @observable auto = null;
  @observable itemsCount = this.pagesize;

  @observable.shallow filteredData = null;
  @observable.shallow toursData = null;
  @observable.shallow operators = [];
  @observable hotelsfound = 0;
  @observable toursfound = 0;
  @observable progress = 0;
  @observable isUpdating = false;
  @observable hasError = false;
  @observable errorMessage = null;

  @observable departure = 1;
  @observable country = null;
  @observable datefrom = null;
  @observable dateto = null;
  @observable nights = {from: 7, to: 14};
  @observable adults = 2;
  @observable child = null;
  @observable childage1 = null;
  @observable childage2 = null;
  @observable childage3 = null;
  @observable region = null;
  @observable hotel = null;
  @observable queryhotel = null;
  @observable operator = null;
  @observable stars = 3;
  @observable meal = null;
  @observable rating = null;
  @observable beach = null;

  init = (props) => {
    this.props = props;

    this.body = document.querySelector('html');

    if (this.props.departure) {
      this.departure = this.props.departure;
    }

    if (this.props.country) {
      this.country = this.props.country;
    }

    if (this.props.region) {
      this.region = this.props.region;
    }

    if (typeof document !== 'undefined') {
      this.socket = io.connect();

      this.socket.on('connect', () => { this.isUpdating = false; });
      this.socket.on('disconnect', () => { this.isUpdating = true; });

      this.socket.on('search', (data) => {
        const {results: {result: {hotel = null} = {}} = {}} = data;

        if (hotel && this.toursData) {
          const toPush = hotel.filter((hot) => (!this.toursData.some((d) => d.hotelcode === hot.hotelcode)));

          if (toPush.length) {
            toPush.forEach((a) => {
              const searchstr = `${a.beach || ''} ${a.hoteldescription || ''} ${a.inroom || ''}`;

              a.beachtypes = {
                sand: !!searchstr.match(/песч/gi),
                stone: !!searchstr.match(/гале/gi),
                coral: !!searchstr.match(/корал/gi)
              };

              if (searchstr.match(/перв(.+) лини(.)/gi) || searchstr.match(/1(.+) лини(.)/gi)) {
                a.line1 = true;
              } else if (searchstr.match(/втор(.+) лини(.)/gi) || searchstr.match(/2(.+) лини(.)/gi)) {
                a.line2 = true;
              } else if (searchstr.match(/треть(.+) лини(.)/gi) || searchstr.match(/3(.+) лини(.)/gi)) {
                a.line3 = true;
              }

              a.wifi = !!searchstr.match(/(wifi|wi-fi)/gi);
              a.condey = !!searchstr.match(/кондиционер/gi);

              const searchstrAirport = `${a.placement || ''}`;
              const matchedAirport = searchstrAirport.match(/(\d+) км до аэропорта/gi) || searchstrAirport.match(/(\d+) км от аэропорта/gi) || searchstrAirport.match(/аэропортa: (\d+) км/gi) || searchstrAirport.match(/(\d+) км/gi);

              if (matchedAirport && matchedAirport[0]) {
                a.airport = matchedAirport[0].match(/(\d+)/)[0];
              }
            });

            this.toursData = [...this.toursData, ...toPush];
            this.applyFilters();
          }
        }

        const {status: {requestid, state = this.state, progress = this.progress, operators = this.operators, hotelsfound = this.hotelsfound, toursfound = this.toursfound} = {}} = data;

        if (hotelsfound !== this.hotelsfound) {
          this.hotelsfound = hotelsfound;
          this.socket.emit('getdata', requestid);
        }

        this.toursfound = toursfound;
        this.operators = operators;
        this.progress = progress;

        if (state) {
          this.status = state;
        }
      });

      if (this.queryhotel || this.auto) {
        this.startSearch();
      }
    }
  };

  reset = (callback = () => {}) => {
    this.itemsCount = this.pagesize;
    this.filteredData = null;
    this.toursData = null;
    this.progress = 0;
    this.operators = [];
    this.hasError = false;
    this.status = null;
    window.addEventListener('scroll', this.setMaxItems, {passive: true});
    this.reconnect();
    // .then(() => callback());
    callback();
  };

  reconnect = () => new Promise((resolve) => {
    this.socket.on('connect', () => {
      resolve();
    });
    this.socket.disconnect();
    this.socket.connect();
  });

  send = (data) => {
    if (this.socket && this.socket.connected) {
      this.socket.emit('search', data);
    }
  };

  updateState = () => {
    if (this.props && this.props.history) {
      this.props.history.push(this.getState());
    }

    return this.applyFilters();
  };

  found = () => (this.toursData ? this.toursData.length : null);

  getParam = (props, param, str) => {
    const {match: {params} = {}} = props;

    if (params && params[param] && params[param].toString() !== '0') {
      if (str) {
        return params[param].toString();
      }
      return Number(params[param].toString());
    } else if (props && props[param] && props[param].toString() !== '0') {
      if (str) {
        return props[param].toString();
      }
      return Number(props[param].toString());
    }

    return null;
  };

  setValues = (props) => {
    this.props = props;

    this.departure = this.getParam(props, 'departure') || 1;
    this.country = this.getParam(props, 'country') || 0;
    this.region = this.getParam(props, 'region', true) || 0;
    this.hotel = this.getParam(props, 'hotel', true) || 0;
    this.queryhotel = this.getParam(props, 'queryhotel', true) || 0;
    this.datefrom = this.getParam(props, 'datefrom') || (new Date()).getTime();
    this.dateto = this.getParam(props, 'dateto') || calculateDate(14).getTime();
    this.nights.from = this.getParam(props, 'nightsfrom') || 7;
    this.nights.to = this.getParam(props, 'nightsto') || 14;
    this.adults = this.getParam(props, 'adults') || 2;
    this.child = this.getParam(props, 'child');
    this.childage1 = this.getParam(props, 'childage1');
    this.childage2 = this.getParam(props, 'childage2');
    this.childage3 = this.getParam(props, 'childage3');
    this.beach = this.getParam(props, 'beach', true) || 0;
    this.auto = this.getParam(props, 'auto', true) || 0;
  };

  getState = () => {
    if (this.country) {
      return `/search/${this.departure || 0}/${this.country || 0}/${this.region || 0}/${this.hotel || 0}/${this.queryhotel || 0}/${this.datefrom || 0}/${this.dateto || 0}/${this.nights.from || 0}/${this.nights.to || 0}/${this.adults || 0}/${this.child || 0}/${this.childage1 || 0}/${this.childage2 || 0}/${this.childage3 || 0}/${this.beach || 0}/${this.auto || 0}`;
    }

    return '';
  };

  setDeparture = (val) => {
    this.itemsCount = this.pagesize;
    this.departure = val;
    this.region = null;
    this.hotel = null;
    this.queryhotel = null;
    this.operator = null;
    this.updateState();
    this.reset();
  };

  setCountry = (val) => {
    this.itemsCount = this.pagesize;
    this.country = val;
    this.region = null;
    this.hotel = null;
    this.queryhotel = null;
    this.operator = null;
    this.reset();
    this.updateState();
  };

  setRegion = (val) => {
    this.itemsCount = this.pagesize;
    this.region = val;
    this.hotel = null;
    this.updateState();
  };

  setHotel = (val) => {
    this.itemsCount = this.pagesize;
    this.hotel = val;
    this.operator = null;
    this.updateState();
  };

  setBeach = (val) => {
    this.itemsCount = this.pagesize;
    this.beach = val;
    this.updateState();
  };

  setQueryHotel = (val) => {
    this.itemsCount = this.pagesize;
    this.hotel = null;
    this.queryhotel = val;
    this.operator = null;
    this.updateState();
    this.startSearch();
  };

  setDates = (datefrom, dateto) => {
    this.itemsCount = this.pagesize;
    this.datefrom = datefrom;
    this.dateto = dateto;
    this.operator = null;
    this.updateState();
  };

  setNights = (val) => {
    this.itemsCount = this.pagesize;
    this.nights = val;
    this.operator = null;
    this.updateState();
  };

  setStars = (val) => {
    this.itemsCount = this.pagesize;
    this.stars = val;
    this.applyFilters();
  };

  setMeal = (val) => {
    this.itemsCount = this.pagesize;
    this.meal = val;
    this.applyFilters();
  };

  setRating = (val) => {
    this.itemsCount = this.pagesize;
    this.rating = val;
    this.applyFilters();
  };

  setPeoples = (val) => {
    this.itemsCount = this.pagesize;
    this.operator = null;
    this.adults = val.adults;
    this.child = val.child;
    switch (this.child) {
      case 1: {
        this.childage1 = val.childage1;
        this.childage2 = null;
        this.childage3 = null;
        break;
      }
      case 2: {
        this.childage1 = val.childage1;
        this.childage2 = val.childage2;
        this.childage3 = null;
        break;
      }
      case 3: {
        this.childage1 = val.childage1;
        this.childage2 = val.childage2;
        this.childage3 = val.childage3;
        break;
      }
      default: {
        this.childage1 = null;
        this.childage2 = null;
        this.childage3 = null;
        break;
      }
    }
    this.updateState();
  };

  setOperator = (val) => {
    this.itemsCount = this.pagesize;
    this.operator = val;
    this.applyFilters();
  };

  updateTours = (defaultRegions) => {
    if (this.departure && this.country) {
      this.reset(() => {
        this.status = 'searching';
        this.filteredData = dummyHotels;
        this.toursData = this.filteredData;

        this.isUpdating = true;

        superAgent.search({
          departure: this.departure,
          country: this.country,
          nightsfrom: this.nights.from,
          nightsto: this.nights.to,
          ...(this.hotel && {hotels: this.hotel}),
          ...(this.queryhotel && {hotels: this.queryhotel}),
          datefrom: formatDate(this.datefrom),
          dateto: formatDate(this.dateto),
          ...(this.region && {regions: this.region}),
          ...(defaultRegions && {regions: defaultRegions}),
          ...(this.adults && {adults: this.adults}),
          ...(this.child && {child: this.child}),
          ...((this.child && this.childage1) && {childage1: this.childage1}),
          ...((this.child && this.childage2) && {childage2: this.childage2}),
          ...((this.child && this.childage3) && {childage3: this.childage3})
        }).then((data) => {
          if (!data) {
            this.hasError = 'Возможно завис шлюз, нажмите "Искать" еще раз';
          } else if (data.error) {
            this.hasError = data.error.errormessage;
            this.isUpdating = false;
          } else {
            this.status = data.status;
            const result = data.results ? data.results.result : data.result;

            if (result) {
              result.hotel.forEach((a) => {
                const searchstr = `${a.beach || ''} ${a.hoteldescription || ''} ${a.inroom || ''}`;

                a.beachtypes = {
                  sand: !!searchstr.match(/песч/gi),
                  stone: !!searchstr.match(/гале/gi),
                  coral: !!searchstr.match(/корал/gi)
                };

                if (searchstr.match(/перв(.+) лини(.)/gi) || searchstr.match(/1(.+) лини(.)/gi)) {
                  a.line1 = true;
                } else if (searchstr.match(/втор(.+) лини(.)/gi) || searchstr.match(/2(.+) лини(.)/gi)) {
                  a.line2 = true;
                } else if (searchstr.match(/треть(.+) лини(.)/gi) || searchstr.match(/3(.+) лини(.)/gi)) {
                  a.line3 = true;
                }

                a.wifi = !!searchstr.match(/(wifi|wi-fi)/gi);
                a.condey = !!searchstr.match(/кондиционер/gi);

                const searchstrAirport = `${a.placement || ''}`;
                const matchedAirport = searchstrAirport.match(/(\d+) км до аэропорта/gi) || searchstrAirport.match(/(\d+) км от аэропорта/gi) || searchstrAirport.match(/аэропортa: (\d+) км/gi) || searchstrAirport.match(/(\d+) км/gi);

                if (matchedAirport && matchedAirport[0]) {
                  a.airport = matchedAirport[0].match(/(\d+)/)[0];
                }
              });

              this.toursData = result.hotel;
              this.filteredData = [];
              this.applyFilters();
            }

            const requestId = data.requestId || data.status.requestid;

            if (requestId) {
              this.send(requestId);
            }
          }

          this.isUpdating = false;
        }).catch(() => { this.isUpdating = false; });
      });
    } else {
      this.isUpdating = false;
    }
  };

  startSearch = (defaultRegions) => {
    if (this.props && (this.props.autosearch || this.auto)) {
      this.updateState();
    }

    this.updateTours(defaultRegions);
  };

  isSearching = () => this.status === 'searching';

  notFound = () => (!this.found() && this.progress === 100) || this.status === 'notfound';

  @computed get filteredCount() {
    return (this.filteredData || []).length;
  }

  applyFilters = (data = this.toursData) => {
    const beach = this.beach ? this.beach.toString().split(',') : null;
    const hotels = this.hotel ? this.hotel.toString().split(',') : null;
    const regions = this.region ? this.region.toString().split(',') : null;
    const operators = this.operator ? this.operator.toString().split(',') : null;

    let filtered = data ? toJS(data).slice(0) : [];

    if (filtered.length === 0) {
      return [];
    }

    filtered = filtered.filter((a) => Number(a.hotelstars) >= Number(this.stars));

    filtered = filtered.filter((a) => Number(a.hotelrating) >= Number(this.rating));

    if (this.meal) {
      filtered = filtered.filter((a) => a.tours.tour.some((t) => (t.mealcode || '').toString() === (this.meal || '').toString()));
    }

    if (operators) {
      filtered = filtered.filter((a) => a.tours.tour.some((t) => operators.indexOf(t.operatorcode.toString()) !== -1));
    }

    if (regions) {
      filtered = filtered.filter((a) => regions.indexOf(`${a.regioncode}`) !== -1);
    }

    if (hotels) {
      filtered = filtered.filter((a) => hotels.indexOf(`${a.hotelcode}`) !== -1);
    }

    if (beach) {
      filtered = filtered.filter((a) => {
        const beachStr = `${a.beach || ''} ${a.hoteldescription || ''}`;

        return beach.every((i) => !!beachStr.match(new RegExp(beachRegex[i]), 'gi'));
      });
    }

    this.filteredData = filtered.sort((a, b) => a.price - b.price);

    return filtered;
  };

  @computed
  get getHotelsPage() {
    return (this.filteredData || []).slice(0, this.itemsCount);
  }

  setMaxItems = () => {
    if (this.body && this.pane) {
      if (this.body.scrollTop + this.body.offsetHeight >= this.pane.offsetHeight - 200) {
        this.itemsCount = this.itemsCount + this.pagesize;
      }
    }
  };

  renderContent(props) {
    if (this.queryhotel) {
      return (
        <Fragment>
          <HotelFull
            theme={props.theme}
            notitle
            key={this.queryhotel}
            match={{params: {hotelcode: this.queryhotel}}}
          />
          {this.toursData ? <Fragment>
            <h2>Туры в этот отель</h2>
            {this.isSearching() && <Progress theme={this.props.theme} className="bottom20" title="Опрашиваем туроператоров..." value={this.progress}/>}
            <Tours theme={props.theme} operators={this.operator} mealcode={this.mealcode} tours={this.toursData[0].tours.tour} hotelcode={this.queryhotel}/>
            <br/>
          </Fragment> : null}
          {this.notFound() && <NotFound/>}
          {this.hasError && <ErrorMessage message={this.hasError}/>}
          {this.isUpdating && <Pending/>}
        </Fragment>
      );
    }

    return (
      <div className="search--pane" ref={(node) => { this.pane = node; }}>
        {this.found() && (
          <Fragment>
            <div className="search--found">Найдено {this.hotelsfound} отелей, {this.toursfound} туров.</div>
            <div className="search--found">Подобрано {this.filteredCount} вариантов из {this.found()} по заданным фильтрам. </div>
          </Fragment>
        )}
        {this.notFound() && <NotFound/>}
        {this.hasError && <ErrorMessage message={this.hasError}/>}
        {this.isSearching() && <Progress theme={this.props.theme} className="bottom20" title="Опрашиваем туроператоров..." value={this.progress}/>}
        {
          this.getHotelsPage.map((hotel, index) => (
            <Hotel
              theme={this.props.theme}
              key={`${hotel.hotelcode}-${index}`}
              operators={this.operator}
              mealcode={this.meal}
              {...hotel}
            />
          ))
        }
        {this.isUpdating && <Pending/>}
      </div>
    );
  }

  render(props) {
    return (
      <SearchForm
        {...this}
        {...props}
        {...{
          dates: {from: this.datefrom, to: this.dateto},
          operatorList: this.operators.filter((op) => ['finished', 'searching'].indexOf(op.status) !== -1)
        }}
      >
        {props.haschild ? props.children : this.renderContent(props)}
      </SearchForm>
    );
  }
}

const search = new Search();

export default search;
