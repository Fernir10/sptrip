import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {observer} from 'mobx-react';
import cn from 'classnames';
import styled from 'styled-components';

import {MinPricesBlock} from './../calendar/minPricesBlock';
import {MinPricesCalendar} from './../calendar/minPrices';

import search from './search';

const StyledBackground = styled.div`
  position: relative;
  background: url(${(props) => props.theme.name === 'light' ? '/img/clouds.jpg' : '/img/panorama_2018.jpg'}) no-repeat center / cover;
`;

@observer
export class Search extends Component {
  componentDidMount() {
    search.init(this.props);
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props);
  }

  componentWillReceiveProps(newProps) {
    search.setValues(newProps);
  }

  render() {
    const {theme, title, full, children, className, style, haschild} = this.props;

    return (
      <Fragment>
        {title && (
          <Helmet>
            <title>{title}</title>
            <meta property="og:title" content={`SPTRIP.RU | ${title}`}/>
            <meta name="description" content="Страница поиска туров"/>
            <meta property="og:description" content="Страница поиска туров"/>
          </Helmet>
        )}
        {full ? (
          <Fragment>
            <StyledBackground theme={theme} className="full">
              <div className="container flex-col flex-center flex-center-align">
                <div className="search--first">
                  {title && <h1 className="search--title">{title}</h1>}
                  {search.render(this.props)}
                </div>
                <MinPricesCalendar
                  {...this.props}
                  titleClassName="desktop-only"
                  className="desktop-only"
                  title="Календарь минимальных цен на путевки"
                />
                <MinPricesBlock
                  theme={this.props.theme}
                  small
                  {...this.props}
                />
              </div>
            </StyledBackground>
            <div className="container" style={{paddingTop: '1em'}}>
              {!haschild && children}
            </div>
          </Fragment>
        ) : (
          <div className={cn('container', className)} style={{paddingTop: '2em', ...style}}>
            {search.render(this.props)}
            {!haschild && children}
          </div>
        )}
      </Fragment>
    );
  }
}
