import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {FireIcon} from './../../../icons';
import {formatPrice, parseDate} from './../../../utils/format';


import {Img, Tabs} from './../../../components';

import './tour.less';

const StyledRow = styled.tr`
  border-bottom: 1px solid ${(props) => props.theme.borderColor || '#aaa'};
`;

export const Tour = ({theme, operatorcode, operatorname, room, nights, meal, mealrussian, hot, price, currency, tourid}) => (
  <StyledRow theme={theme} className="tours-tour">
    <td className="tours-tour--item">
      <Img
        style={{maxHeight: 35}}
        src={operatorcode ? `//tourvisor.ru/pics/operators/searchlogo/${operatorcode}.gif` : '/img/dummy/dummy-image.svg'}
        title={operatorname}
        alt={operatorname}
      />
    </td>
    <td className="tours-tour--item"><b>{room}</b></td>
    <td className="tours-tour--item">{!!nights ? `${nights} ночей` : '...'}</td>
    <td className="tours-tour--item">{mealrussian} {meal}</td>
    <td>
      <div className="flex nowrap flex-center-align">
        {!!(tourid && hot) ? <FireIcon size={20} style={{marginRight: 10}}/> : <span className="nomobile" style={{width: 30}}/>}
        <b>{tourid ? formatPrice(price, currency) : ''}</b>
      </div>
    </td>
    <td>{!!tourid && (
      <Link target="_blank" className="tours-tour--item" to={`/tour/${tourid}`}>Показать</Link>
    )}
    </td>
  </StyledRow>
);

export const Tours = ({theme, operators, tours = [], mealcode, hotelcode, list}) => {
  const operatorArray = (operators ? operators.toString() : '').split(',');

  const grouped = tours
    .filter((a) => operatorArray.indexOf(a.operatorcode.toString()) !== -1 || !operators)
    .filter((a) => ((a.mealcode || '').toString() === mealcode) || !mealcode)
    .sort((a, b) => a.flydate - b.flydate).reduce((r, a) => {
      r[a.flydate] = r[a.flydate] || [];
      r[a.flydate].push(a);
      return r;
    }, {});

  const tabsOptions = [];

  const ordered = {};

  Object.keys(grouped).sort().forEach((key) => {
    ordered[key] = grouped[key];
  });

  Object.keys(ordered).forEach((key) => {
    tabsOptions.push({
      title: parseDate(key),
      content: (
        <table className="tours">
          <tbody>
            {
              grouped[key].map((itm, index) => <Tour theme={theme} {...(index === 0 ? {hot: true} : {})} key={itm.tourid} {...itm} hotelcode={hotelcode}/>)
            }
          </tbody>
        </table>
      )
    });
  });

  return (
    <Fragment>
      {list ? (
        <table className="tours">
          <tbody>
            {
              tours
                .filter((a) => operatorArray.indexOf(a.operatorcode.toString()) !== -1 || !operators)
                .filter((a) => ((a.mealcode || '').toString() === mealcode) || !mealcode)
                .map((itm, index) => <Tour theme={theme} {...(index === 0 ? {hot: true} : {})} key={itm.tourid} {...itm} hotelcode={hotelcode}/>)
            }
          </tbody>
        </table>
      ) : (
        <Tabs theme={theme} tabs={tabsOptions}/>
      )}
    </Fragment>
  );
};
