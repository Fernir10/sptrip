import React, {Component, Fragment} from 'react';
import {Link} from 'react-router-dom';
import {renderToString} from 'react-dom/server';
import {observer} from 'mobx-react';
import {observable, toJS} from 'mobx';
import PropTypes from 'prop-types';

import superAgent from './../../../../utils/superAgent';
import {formatPrice} from './../../../../utils/format';

import {Button, Docker, Gender, Input, Pending, Select} from '../../../../components';

const mustFillText = 'Поле должно быть заполнено';

const Tourist = observer(({theme, tourist, error, index, checkInput = () => {}, countries, documents, document}) => (
  <Fragment>
    <p>{index + 1} турист:</p>
    <Docker className="top20">
      <Input
        theme={theme}
        errorMessage={error.surname}
        onError={(e) => { error.surname = e === 'pattern' ? 'Недопустимый символ!' : ''; }}
        placeholder="Фамилия"
        capitalize
        pattern={(tourist.document === 2 || tourist.document === 1) ? /а-яА-Я\s-./ : /a-zA-Z\s-./}
        onChange={(val) => {
          tourist.surname = val;
          checkInput();
        }}
        value={tourist.surname}
      />
      <Input
        theme={theme}
        errorMessage={error.name}
        onError={(e) => { error.name = e === 'pattern' ? 'Недопустимый символ!' : ''; }}
        placeholder="Имя"
        capitalize
        pattern={(tourist.document === 2 || tourist.document === 1) ? /а-яА-Я\s-./ : /a-zA-Z\s-./}
        onChange={(val) => {
          tourist.name = val;
          checkInput();
        }}
        value={tourist.name}
      />
      <Gender
        theme={theme}
        onChange={(val) => {
          tourist.gender = val;
        }}
        value={tourist.gender}
      />
      <Input
        theme={theme}
        errorMessage={error.birthdate}
        placeholder="Дата рождения"
        pattern="0-9" mask="99.99.9999"
        onChange={(val) => {
          tourist.birthdate = val;
          checkInput();
        }}
        value={tourist.birthdate}
      />
      <Select
        theme={theme}
        bordered
        placeholder="Гражданство"
        onChange={(val) => {
          tourist.country = val;
        }}
        value={tourist.country}
        options={countries}
      />
    </Docker>
    <Docker>
      <Select
        theme={theme}
        bordered
        placeholder="Документ"
        onChange={(val) => {
          tourist.document = val;
        }}
        value={tourist.document}
        options={documents}
      />
      <Input
        theme={theme}
        errorMessage={error.passportSerial}
        placeholder="Серия"
        mask={document.serial}
        onChange={(val) => {
          tourist.passportSerial = val;
          checkInput();
        }}
        value={tourist.passportSerial}
      />
      <Input
        theme={theme}
        errorMessage={error.passportNumber}
        placeholder="Номер"
        mask={document.number}
        onChange={(val) => {
          tourist.passportNumber = val;
          checkInput();
        }}
        value={tourist.passportNumber}
      />
      <Input
        theme={theme}
        errorMessage={error.passportStart}
        placeholder="Выдан"
        mask="99.99.9999"
        onChange={(val) => {
          tourist.passportStart = val;
          checkInput();
        }}
        value={tourist.passportStart}
      />
      <Input
        theme={theme}
        errorMessage={error.passportEnd}
        disabled={tourist.document === 2 || tourist.document === 1 ? 'true' : ''}
        placeholder="Истекает"
        mask="99.99.9999"
        onChange={(val) => {
          tourist.passportEnd = val;
          checkInput();
        }}
        value={tourist.passportEnd}
      />
    </Docker>
  </Fragment>
));

@observer
export class OrderForm extends Component {
  @observable wasSend = false;

  @observable buyer = {
    name: '',
    surname: '',
    phone: '',
    email: ''
  };

  @observable fillTourists = false;
  @observable tourists = null;
  @observable errors = {};

  startCheck = false;

  static propTypes = {
    isUpdating: PropTypes.bool,
    match: PropTypes.shape({
      params: PropTypes.object
    }),
    hotelname: PropTypes.string,
    countryname: PropTypes.string,
    price: PropTypes.string,
    currency: PropTypes.string,
    country: PropTypes.string,
    adults: PropTypes.string,
    child: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.tourists = Array(Number(this.props.adults) + Number(this.props.child)).fill(0).map(() => ({
      surname: '',
      name: '',
      gender: 0,
      birthdate: '',
      country: 47,
      passportSerial: '',
      passportNumber: '',
      passportEnd: '',
      passportStart: '',
      document: 0
    }));

    this.errors = {buyer: toJS(this.buyer), tourists: toJS(this.tourists)};
  }

  documents = [
    {title: 'Загранпаспорт РФ', value: 0, serial: '11-11', number: '1111111'},
    {title: 'Паспорт РФ', value: 1, serial: '11-11', number: '111111'},
    {title: 'Свидетельство о рождении', value: 2, serial: '##-###', number: '111111'},
    {title: 'Другой документ', value: 3, serial: '#####', number: '1111111'}
  ];

  sendMessage = () => {
    const {country, countries, tourid, hotelname, countryname, price, currency} = this.props;

    this.wasSend = false;

    if (!this.checkErrors()) {
      return;
    }

    superAgent.email({
      to: this.buyer.email,
      subject: `Ваш заказ ${hotelname}, ${countryname}, ${formatPrice(price, currency)}`,
      body: renderToString(
        <Fragment>
          <h1>{formatPrice(price, currency)}</h1>
          <p><a target="_blank" href={`/tour/${tourid}`}>{`https://sptrip.ru/tour/${tourid}`}</a></p>
          <h3>{hotelname}, {country}</h3>
          <p><b>Покупатель:</b></p>
          <p><b>Фамилия:</b> {this.buyer.surname}</p>
          <p><b>Имя:</b> {this.buyer.name}</p>
          <p><b>Телефон:</b> {this.buyer.phone}</p>
          <p><a href={`mailto:${this.buyer.email}`}>{this.buyer.email}</a></p>
          {this.fillTourists && (
            <Fragment>
              <br/>
              <p><b>Туристы:</b></p>
              {
                this.tourists ? this.tourists.map((tourist, i) => (
                  <div key={`tourists-${i}`} className="flex-row">
                    <p><b>Фамилия:</b> {tourist.surname}</p>
                    <p><b>Имя:</b> {tourist.name}</p>
                    <p><b>Пол:</b> {tourist.gender ? 'мужской' : 'женский'}</p>
                    <p><b>Дата рождения:</b> {tourist.birthdate}</p>
                    <p><b>Гражданство:</b> {countries.find((c) => c.value.toString() === tourist.country.toString()).title}</p>
                    <p><b>Документ:</b> {this.documents[tourist.document]} {tourist.passportSerial} {tourist.passportNumber}</p>
                    <p><b>Выдан:</b> {tourist.passportStart}</p>
                    <p><b>Истекает:</b> {tourist.passportEnd}</p>
                  </div>
                )) : null
              }
            </Fragment>
          )}
        </Fragment>
      )
    }).then(() => {
      this.wasSend = true;
    });
  };

  checkInput = () => {
    if (this.startCheck) {
      this.checkErrors();
    }
  };

  checkErrors = () => {
    this.startCheck = true;
    let success = true;

    ['name', 'surname', 'phone', 'email'].forEach((key) => {
      this.errors.buyer[key] = '';
      if (this.buyer[key] === '') {
        this.errors.buyer[key] = mustFillText;
        success = false;
      }
    });

    if (this.tourists && this.fillTourists) {
      this.tourists.forEach((tourist, index) => {
        ['name', 'surname', 'birthdate', 'passportSerial', 'passportNumber', 'passportStart', 'passportEnd'].forEach((key) => {
          this.errors.tourists[index][key] = '';
          if (tourist[key] === '') {
            this.errors.tourists[index][key] = mustFillText;
            success = false;
          }
        });
      });
    }

    return success;
  };

  renderTourists = () => {
    if (this.tourists) {
      return this.tourists.map((tourist, i) => (
        <Tourist
          theme={this.props.theme}
          key={`tourist-${i}`}
          tourist={tourist}
          index={i}
          error={this.errors.tourists[i]}
          checkInput={this.checkInput}
          documents={this.documents}
          countries={this.props.countries}
          document={this.documents[tourist.document]}
        />
      ));
    }

    return null;
  };

  renderBuyer = () => (
    <Docker>
      <Input
        theme={this.props.theme}
        errorMessage={this.errors.buyer.surname}
        onError={(e) => { this.errors.buyer.surname = e === 'pattern' ? 'Недопустимый символ!' : ''; }}
        placeholder="Фамилия"
        capitalize
        pattern="а-яА-ЯёЁ\s-."
        onChange={(val) => {
          this.buyer.surname = val;
          this.checkInput();
        }}
        value={this.buyer.surname}
      />
      <Input
        theme={this.props.theme}
        errorMessage={this.errors.buyer.name}
        onError={(e) => { this.errors.buyer.name = e === 'pattern' ? 'Недопустимый символ!' : ''; }}
        placeholder="Имя"
        capitalize
        pattern="а-яА-ЯёЁ\s-."
        onChange={(val) => {
          this.buyer.name = val;
          this.checkInput();
        }}
        value={this.buyer.name}
      />
      <Input
        theme={this.props.theme}
        errorMessage={this.errors.buyer.email}
        placeholder="E-mail"
        onChange={(val) => {
          this.buyer.email = val;
          this.checkInput();
        }}
        value={this.buyer.email}
      />
      <Input
        theme={this.props.theme}
        errorMessage={this.errors.buyer.phone}
        mask="9 (999) 999-99-99"
        placeholder="Телефон"
        onChange={(val) => {
          this.buyer.phone = val;
          this.checkInput();
        }}
        value={this.buyer.phone}
      />
    </Docker>
  );

  render() {
    return (
      <Fragment>{this.props.isUpdating ? <Pending/> : (
        <Fragment>
          {!this.wasSend ? <Fragment>
            <h2>Забронировать этот тур прямо сейчас</h2>
            <h3>Покупатель</h3>
            <p className="tourinfo--warn">На это имя будет заключен договор. Заполняется на русском языке.</p>
            {this.renderBuyer()}
            {this.fillTourists && (
              <Fragment>
                <div className="flex flex-between">
                  <h3>Паспорта туристов</h3>
                  <Button className="flex-padded" onClick={() => { this.fillTourists = !this.fillTourists; }}>Не заполнять паспорта туристов</Button>
                </div>
                <p className="tourinfo--warn">Заполняется строго как в заграничном паспорте латиницей.</p>
                {this.renderTourists()}
              </Fragment>
            )}
            <div className="flex-row">
              <div className="col"/>
              {!this.fillTourists && (
                <Button theme={this.props.theme} className="flex-padded" onClick={() => { this.fillTourists = !this.fillTourists; }}>Заполнить паспорта туристов</Button>
              )}
              <Button theme={this.props.theme} onClick={this.sendMessage}>Забронировать</Button>
            </div>
            <Link
              to={{
                pathname: this.props.location.pathname,
                state: {modal: true, returnTo: this.props.location.pathname}
              }}
            >
              Отправляя запрос, Вы&nbsp;подтверждаете &laquo;Согласие на&nbsp;обработку персональных данных&raquo;
            </Link>
          </Fragment> : <h3>Спасибо, заявка отправлена успешно!</h3>
          }
        </Fragment>
      )}
      </Fragment>
    );
  }
}
