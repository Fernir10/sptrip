import React, {Component, Fragment} from 'react';

import {observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import {observable} from 'mobx';
import {Helmet} from 'react-helmet';
import PropTypes from 'prop-types';
import styled from 'styled-components';


import superAgent from './../../../../utils/superAgent';

import {ErrorMessage} from './../../notFound';

import {Pending, Spinner, Swiper} from './../../../../components';
import {DatesIcon, MealIcon, NightsIcon, PeopleIcon, PointIcon, RoomIcon} from '../../../../icons/index';
import {Avia} from '../../avia/index';
import {OrderForm} from './orderForm';
import {formatPrice, parseDateFull} from '../../../../utils/format';


import './tourinfo.less';

const PaymentTable = styled.table`
  background-color: ${(props) => props.theme.whiteBackgroundColor || '#fff'};
`;

const PaymentTableLast = styled.tr`
  background-color: ${(props) => props.theme.borderColor || '#d7e2dc'};
  color: ${(props) => props.theme.whiteTextColor || '#d7e2dc'};
`;

const OperatorIcon = styled.div`
    background: #ffffff url(//tourvisor.ru/pics/operators/mobilelogo/${(props) => props.operatorcode}.png) no-repeat center center/ contain;
    width: 120px;
    height: 40px;
    margin-right: 10px;
`;

@observer
export class TourInfo extends Component {
  @observable error = null;
  @observable tourData = null;
  @observable isUpdating = false;
  @observable isUpdatingDetails = false;

  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.object
    })
  };

  meals = [];
  @observable.shallow countries = [];

  flags = {
    notransfer: 'Трансфер',
    nomedinsurance: 'Медицинская страховка',
    noflight: 'Авиаперелет',
    nomeal: 'Питание'
  };

  constructor(props) {
    super(props);

    const {theme, match: {params: {tourid} = {}} = {}, store: {meals, countries, flights, tourinfo, errormessage, tour} = {}} = props;

    if (errormessage) {
      this.error = errormessage.toString() === 'Wrong (obsolete) TourID.' ? 'Неверный (устаревший) идентификатор тура!' : errormessage;
      return;
    }

    if (tour && !this.tourData) {
      if (meals) {
        this.meals = meals;
      }

      if (countries) {
        this.countries = countries;
      }

      this.tourData = tour;

      if (flights) {
        this.tourData.flights = flights ? flights[0] : null;
      }

      if (tourinfo) {
        this.tourData.tourinfo = tourinfo;
        this.tourData.flags = tourinfo.flags;
      }

      this.tourData.tourid = tourid;
    } else {
      this.update();
    }
  }

  componentWillMount() {
    const {match: {params: {tourid} = {}} = {}, store: {tour} = {}} = this.props;

    if (tour && !this.tourData) {
      this.update();
    }
  }

  update = () => {
    this.isUpdating = true;
    this.error = null;

    superAgent.form({type: 'meal,country'}).then((data) => {
      const {meals, countries} = data;

      this.meals = meals;
      this.countries = countries;

      const {match: {params: {tourid} = {}} = {}} = this.props;

      superAgent.tour({tourid}).then(({error: {errormessage} = {}, tour, flights, tourinfo}) => {
        if (errormessage) {
          this.error = errormessage;
          this.isUpdating = false;
          return;
        }

        if (tour) {
          this.tourData = tour;
          this.tourData.tourid = tourid;
        }

        this.isUpdatingDetails = true;

        superAgent.tourdetails({tourid: this.props.match.params.tourid})
          .then(({error: {errormessage} = {}, flights, tourinfo}) => {
            if (errormessage) {
              this.error = errormessage;
              this.isUpdating = false;
              return;
            }

            if (flights) {
              this.tourData.flights = flights ? flights[0] : null;
            }

            if (tourinfo) {
              this.tourData.tourinfo = tourinfo;
              this.tourData.flags = tourinfo.flags;
            }

            this.isUpdatingDetails = false;
          })
          .catch(() => {
            this.isUpdatingDetails = false;
          });
      });

      this.isUpdating = false;
    }).catch(() => {
      this.isUpdating = false;
    });
  };

  render() {
    const {
      meal,
      visacharge,
      tourinfo: {addpayments} = {},
      fuelcharge,
      price,
      currency,
      flags,
      tourname,
      hotelname,
      hotelcode,
      hoteldescription,
      hotelpicturemedium,
      images = [],
      operatorcode,
      departurename,
      countryname,
      hotelregionname,
      flydate,
      nights,
      room,
      adults,
      child,
      flights
    } = this.tourData || {};

    const mealName = meal && this.meals ? this.meals.find((itm) => itm.name.toString() === meal) : [];

    return (
      <div className="container">
        {this.isUpdating ? <Pending/>
          : this.tourData && !this.error ? (
            <Fragment>
              <Helmet>
                <title>{`Отель ${hotelname}, ${tourname}`}</title>
                <meta name="description" content={hoteldescription}/>
                <meta property="og:title" content={`Тур в отель ${hotelname}, ${tourname}`}/>
                <meta property="og:description" content={hoteldescription}/>
                <meta property="og:image" content={`https:${hotelpicturemedium}`}/>
              </Helmet>
              <h1 className="flex">
                <OperatorIcon operatorcode={operatorcode}/>
                {hotelname}
              </h1>
              <div className="flex-row">
                <div className="tourinfo--carousel">
                  {images && images.map ? (
                    <Swiper
                      theme={this.props.theme}
                      height={400}
                      inner
                      color="#0f3"
                      images
                      fullscreen
                    >
                      {images.map((img) => (<img key={img.original} src={img.original || null}/>))}
                    </Swiper>
                  ) : (hotelpicturemedium ? (
                    <div style={{height: 400, background: `url(${hotelpicturemedium}) no-repeat center / cover`}}/>
                  ) : null)}
                </div>
                <div className="tourinfo--carousel--after">
                  {tourname ? <div className="muted">{tourname}</div> : null}
                  {hoteldescription && (
                    <Fragment>
                      <br/>
                      <p dangerouslySetInnerHTML={{__html: hoteldescription}}/>
                    </Fragment>
                  )}
                  <br/>
                  <p><Link to={`/hotel/${hotelcode}`}>Описание отеля</Link></p>
                  <br/>
                  <table>
                    <tbody>
                      <tr><td><PointIcon/></td><td><b>Перелет:</b></td><td>&nbsp;&nbsp;{departurename} &rarr; {countryname}, {hotelregionname}</td></tr>
                      <tr><td><DatesIcon/></td><td><b>Дата вылета:</b></td><td>&nbsp;&nbsp;{parseDateFull(flydate)}</td></tr>
                      <tr><td><NightsIcon/></td><td><b>Проживание:</b></td><td>&nbsp;&nbsp;{nights} ночей</td></tr>
                      {mealName ? <tr><td><MealIcon/></td><td><b>Питание:</b></td><td>&nbsp;&nbsp;{mealName.russianfull}</td></tr> : null}
                      <tr><td><RoomIcon/></td><td><b>Номер:</b></td><td>&nbsp;&nbsp;{room}</td></tr>
                      <tr><td><PeopleIcon/></td><td><b>Взрослых:</b></td><td>&nbsp;&nbsp;{adults}</td></tr>
                      {child > 0 && <tr><td><PeopleIcon/></td><td><b>Детей:</b></td><td>&nbsp;&nbsp;{child}</td></tr>}
                    </tbody>
                  </table>
                </div>
              </div>
              <h2>Информация о рейсах</h2>
              {flights && flights.forward && flights.dateforward ? (
                <Avia theme={this.props.theme} {...{flights: [flights]}}/>
              ) : (
                <Fragment>
                  {this.isUpdatingDetails ? <Spinner/> : <p>Не удалось актуализировать перелет, уточните у менеджера</p>}
                </Fragment>
              )}
              <h2>Стоимость поездки</h2>
              <PaymentTable theme={this.props.theme} className="tourinfo--payment">
                <tbody>
                  {flags ? (
                    <tr>
                      <td>{Object.keys(flags).filter((key) => !flags[key]).map((key) => this.flags[key]).join(', ')}</td>
                      <td className="nowrap">{formatPrice(price, currency)}</td>
                    </tr>
                  ) : null}
                  {Number(fuelcharge) > 0 ? <tr>
                    <td>Топливный сбор</td>
                    <td className="nowrap">{fuelcharge} {currency === 'RUB' ? 'руб' : currency}</td>
                  </tr> : null}
                  {addpayments ?
                    <tr>
                      <td colSpan={2}>
                        {addpayments.map((pay) => (
                          <p><b>{pay.name}:</b> <span className="nowrap">{pay.amount}</span></p>
                        ))}
                      </td>
                    </tr> : null}
                  {Number(visacharge) > 0 ? (
                    <tr>
                      <td>Виза</td>
                      <td className="nowrap">{visacharge} руб. на чел.</td>
                    </tr>
                  ) : null}
                  <PaymentTableLast theme={this.props.theme} className="tourinfo--payment--last">
                    <td>Итого</td>
                    <td className="big nowrap">{formatPrice(Number(price) + (Number(visacharge) * (Number(adults) + Number(child))), currency)}</td>
                  </PaymentTableLast>
                </tbody>
              </PaymentTable>
              <OrderForm
                {...this.props}
                isUpdating={this.isUpdating}
                tourid={this.props.match.params.tourid}
                {...this.tourData}
                countries={this.countries.map((itm) => ({value: itm.id, title: itm.name}))}
              />
              <br/><br/>
            </Fragment>
          ) : this.error ? (
            <Fragment>
              <Helmet>
                <title>{this.error}</title>
              </Helmet>
              <ErrorMessage message={this.error}/>
            </Fragment>
          ) : null}
      </div>
    );
  }
}
