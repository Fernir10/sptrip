import React, {PureComponent} from 'react';

export class Yametrika extends PureComponent {
  componentDidMount() {
    this.callMetrika(document, window, 'yandex_metrika_callbacks');
  }

  callMetrika(d, w, c) {
    (w[c] = w[c] || []).push(() => {
      try {
        w[`yaCounter${this.props.counter}`] = new Ya.Metrika({
          id: this.props.counter,
          clickmap: true,
          trackLinks: true,
          accurateTrackBounce: true
        });
      } catch (e) { }
    });

    const n = d.getElementsByTagName('script')[0];
    const s = d.createElement('script');
    const f = () => { n.parentNode.insertBefore(s, n); };

    s.type = 'text/javascript';
    s.async = true;
    s.src = 'https://mc.yandex.ru/metrika/watch.js';

    if (w.opera === '[object Opera]') {
      d.addEventListener('DOMContentLoaded', f, {passive: true});
    } else {
      f();
    }
  }

  render = () => null;
}
