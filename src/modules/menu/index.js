import React, {Fragment, PureComponent} from 'react';
import {Link} from 'react-router-dom';
import cn from 'classnames';
import clickOutside from 'react-click-outside';
import styled from 'styled-components';

import {ModifyColor} from './../../utils/color';
import {CloseIcon, HamburgerIcon} from './../../icons';
import {Arrow, Img} from './../../components';

import './menu.less';

const StyledNav = styled.nav`
  background-color: ${(props) => props.theme.blueColor};
`;

const StyledMenuItem = styled.div`
  &:before {
    border-left: 1px solid ${(props) => props.theme.orangeLightColor};
  }

  @media only screen and (max-device-width: 992px) {
    &:before {
      border-left: none;
    }
  }
`;

const StyledDropdown = styled.div`
  background-color: ${(props) => ModifyColor(props.theme.blueColor, -10)};
  
  @media only screen and (min-device-width: 740px) {
    column-count: 3;
  }
`;

const StyledBody = styled.div`
  @media only screen and (max-device-width: 992px) {
    background-color: ${(props) => ModifyColor(props.theme.blueColor, -5)};
  }
`;

const StyledDropdownText = styled.div`
  color: ${(props) => props.theme.whiteTextColor};
`;

const StyledLink = styled(Link)`
  color: ${(props) => props.theme.whiteTextColor};
  @media only screen and (min-device-width: 992px) {
    display: block;
  }
 
  &:not(:last-child):after {
    border-left: 1px solid ${(props) => props.theme.orangeLightColor};
  }
`;

const MenuItem = ({children = [], href = '', aref, type = '', style, icon, title, onClick, theme}) => (
  <Fragment>
    {children.length ? (
      <StyledMenuItem
        theme={theme}
        className="menu-item-container menu-item-container--column"
      >
        <MenuItemsDropdown
          theme={theme}
          onClick={onClick}
          menu={children}
          content={(
            <Fragment>
              {icon}{title}
            </Fragment>
          )}
        />
      </StyledMenuItem>
    ) : (
      <StyledMenuItem
        theme={theme}
        className="menu-item-container"
        onClick={onClick}
      >
        {href !== '' ? (
          <StyledLink theme={theme} className={cn('menu-item', `${type}`)} to={href} style={style}>{icon}{title}</StyledLink>
        ) : (
          <a className={cn('menu-item', `${type}`)} href={aref}>{icon}{title}</a>
        )}
      </StyledMenuItem>
    )}
  </Fragment>
);

@clickOutside
class MenuItemsDropdown extends PureComponent {
  state = {
    isOpen: null
  };

  toggleMenu = () => this.setState({isOpen: !this.state.isOpen});

  closeMenu = () => this.setState({isOpen: false});

  handleClickOutside() {
    this.closeMenu();
  }

  render() {
    const {menu = [], content, onClick, theme} = this.props;

    return (
      <Fragment>
        <StyledDropdownText theme={theme} className="menu-item" onClick={this.toggleMenu}>
          {content}
          <Arrow size={10} color={theme.whiteTextColor} style={{marginLeft: '10px'}} isOpened={this.state.isOpen}/>
        </StyledDropdownText>
        <StyledDropdown theme={theme} className={cn('menu--body--dropdown', {'menu--body--dropdown--visible': (menu.length > 0 && this.state.isOpen)})}>
          {menu.map((item) => (
            <MenuItem
              theme={theme}
              key={item.title}
              {...item}
              {...this.props}
              onClick={onClick}
            />
          ))}
        </StyledDropdown>
      </Fragment>
    );
  }
}

class MenuItems extends PureComponent {
  state = {
    isOpen: null
  };

  componentDidMount() {
    this.htmlStyle = document.querySelector('html').style;
  }

  openMenu = () => {
    this.setState({isOpen: true}, () => {
      this.htmlStyle.overflow = this.state.isOpen ? 'hidden' : '';
    });
  };

  closeMenu = () => {
    this.setState({isOpen: false}, () => {
      this.htmlStyle.overflow = this.state.isOpen ? 'hidden' : '';
      window.scrollTo(0, 0);
    });
  };

  render() {
    const {menu = [], nologo, theme} = this.props;

    return (
      <Fragment>
        {!this.state.isOpen && (
          <Fragment>
            {menu.length > 0 && <HamburgerIcon className="menu-hamburger" onClick={this.openMenu}/>}
            {
              !nologo && (
                <StyledLink theme={theme} to="/" className="menu--logo--link">
                  <Img src="/img/logo3.svg" className="menu--logo" alt="SPTRIP.RU"/>sptrip.ru
                </StyledLink>
              )
            }
          </Fragment>
        )}
        {menu.length > 0 && (
          <StyledBody
            theme={theme}
            ref={(node) => {
              this.body = node;
            }}
            className={cn('menu--body', {'menu--body--open': this.state.isOpen, 'menu--body--close': this.state.isOpen === false})}
            onAnimationEnd={() => {
              if (this.state.isOpen === false) {
                this.setState({
                  isOpen: null
                });
              }
            }}
          >
            {
              menu.map((item) => <MenuItem theme={theme} onClick={this.closeMenu} key={item.title} {...item} {...this.props}/>)
            }
            <CloseIcon
              size={15}
              className="menu--close"
              color="#fff"
              onClick={this.closeMenu}
            />
          </StyledBody>
        )}
      </Fragment>
    );
  }
}

export const Menu = ({history, color, menu, nologo, theme}) => (
  <header className="menu--header">
    <StyledNav className="menu" theme={theme}>
      <MenuItems history={history} menu={menu} nologo={nologo} theme={theme}/>
    </StyledNav>
  </header>
);
