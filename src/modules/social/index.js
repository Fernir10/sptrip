import React, {Fragment, PureComponent} from 'react';

import {FacebookIcon, InstagramIcon, OKIcon, TelegramIcon, VKIcon} from './../../icons';

import './social.less';

export const Social = (props) => (
  <div {...props}>
    <div className="social_block">
      <VKIcon onClick={() => window.open('https://vk.com/sp_trip')} className="social_icon"/>
      <FacebookIcon onClick={() => window.open('https://www.facebook.com/sptrip.ru')} className="social_icon"/>
      <OKIcon onClick={() => window.open('https://ok.ru/sptrip')} className="social_icon"/>
      <InstagramIcon onClick={() => window.open('https://www.instagram.com/sptrip.ru/')} className="social_icon"/>
      <TelegramIcon onClick={() => window.open('https://t.me/sptrip')} className="social_icon"/>
    </div>
  </div>
);


class SocialButton extends PureComponent {
  render() {
    const Icon = this.props.icon;

    return (
      <div onClick={() => location.assign(this.props.to)} className="social--block" style={{background: this.props.background}}>
        <Icon size={35} color="#fff" className="social_icon social_icon--white"/>
        <br/>
        {this.props.children}
      </div>
    );
  }
}

export class SocialBlock extends PureComponent {
  render() {
    return (
      <Fragment>
        <h2 className="text-center">Подписывайтесь и узнавайте первыми о самых горячих предложениях!</h2>
        <div className="social">
          <SocialButton to="https://vk.com/sp_trip" background={'#4a719e'} icon={VKIcon}>
            VK.com
          </SocialButton>

          <SocialButton
            to="https://www.instagram.com/sptrip.ru/"
            background={'radial-gradient(ellipse at left 30% bottom -20%, rgba(254,191,86,1) 3%,rgba(239,33,69,1) 44%,rgba(196,39,144,1) 65%,rgba(82,66,214,1) 100%)'}
            icon={InstagramIcon}
          >
            Instagram
          </SocialButton>

          <SocialButton to="https://www.facebook.com/sptrip.ru" background={'#395693'} icon={FacebookIcon}>
            Facebook
          </SocialButton>

          <SocialButton to="https://ok.ru/sptrip" background={'#e77d0c'} icon={OKIcon}>
            OK.ru
          </SocialButton>

          <SocialButton to="https://t.me/sptrip" background={'#0288c7'} icon={TelegramIcon}>
            Telegram
          </SocialButton>

        </div>
      </Fragment>
    );
  }
}
