import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import cn from 'classnames';

import {ModifyColor} from './../../utils/color';

import {Breadcrumb, Swiper} from './../../components';
import {PhotoIcon} from './../../icons';

import './reviews.less';

import reviews from './reviews.json';


const Review = ({isSlide, name, icon, text}) => (
  <div className={cn('revscr--item', {'revscr--item--slide': isSlide})}>
    <div className="revscr--name">{name}</div>
    {icon !== '' ? <div className="revscr--img" style={{background: `url(${icon}) no-repeat center center / cover`}}/> : <PhotoIcon className="revscr--img"/>}
    {text}
  </div>
);


const StyledLink = styled(Link)`
  color: ${(props) => props.theme.color || '#477ab9'};
  text-align: center;
  display: block;
  font-size: 140%;
  margin-top: 20px;
`;

const StyledSection = styled.section`
  margin: 3em 0;
`;

export const ReviewsBlock = ({theme}) => (
  <StyledSection>
    <h2 className="text-center">Отзывы наших туристов</h2>
    <Swiper theme={theme} height={300}>
      {reviews.map((rev, index) => <Review isSlide index={index} key={rev.name} {...rev}/>)}
    </Swiper>
    <StyledLink theme={theme} to="/reviews">Все отзывы</StyledLink>
  </StyledSection>
);


export const ReviewsPage = ({theme}) => (
  <Fragment>
    <Helmet>
      <title>Отзывы наших туристов</title>
      <meta property="og:title" content="SPTRIP.RU | Отзывы наших туристов"/>
      <meta name="description" content="SPTRIP.RU | Отзывы наших туристов"/>
    </Helmet>
    <div className="container top20">
      <Breadcrumb bg={theme.blueColor} hover={ModifyColor(theme.blueColor, 10)} text={theme.whiteTextColor}>
        <Link to="/">На главную</Link>
        <span>Отзывы</span>
      </Breadcrumb>
    </div>
    <div className="container">
      <StyledSection>
        <h2 className="text-center">Отзывы наших туристов</h2>
        {reviews.map((rev, index) => <Review index={index} key={rev.name} {...rev}/>)}
      </StyledSection>
    </div>
  </Fragment>
);
