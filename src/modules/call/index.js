import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';
import {renderToString} from 'react-dom/server';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import styled from 'styled-components';

import {ModifyColor, rgba} from './../../utils/color';
import {Breadcrumb, Button, Docker, Input} from './../../components';

import superAgent from './../../utils/superAgent';

import './call.less';

const StyledLink = styled(Link)`
  display: inline-block;
  margin: 20px 0;
  font-size: 90%;
  color: ${(props) => props.theme.color};
  text-decoration: none;
  border-bottom: 1px solid ${(props) => rgba(props.theme.color, 0.3)};
  transition: border .2s ease;
  
  &:hover {
    border-bottom: 1px solid ${(props) => rgba(props.theme.color, 1)};
  }
`;

@observer
export class CallUs extends Component {
  state = {
    name: '',
    phone: '',
    success: false
  };

  @observable errorName = null;
  @observable errorPhone = null;

  checkErrors = () => {
    this.errorName = this.state.name === '' ? 'Поле должно быть заполнено' : '';
    this.errorPhone = this.state.phone === '' ? 'Поле должно быть заполнено' : '';
  };

  sendMessage = () => {
    this.setState({success: false});

    this.checkErrors();

    if (this.errorName !== '' || this.errorPhone !== '') {
      return;
    }

    superAgent.email({
      subject: `Перезвонить - ${this.state.name}: ${this.state.phone}`,
      body: renderToString(
        <Fragment>
          <p>{this.state.name}</p>
          <p>{this.state.phone}</p>
        </Fragment>
      )
    })
      .then(() => {
        this.setState({success: true});
      })
      .catch(() => {
        this.setState({success: false});
      });
  };

  render() {
    const {theme, onClose, location} = this.props;

    return (
      <div className="container-md top20">
        <Breadcrumb bg={theme.blueColor} hover={ModifyColor(theme.blueColor, 10)} text={theme.whiteTextColor}>
          <Link to="/">На главную</Link>
          <span>Заказать звонок</span>
        </Breadcrumb>
        { this.state.success ?
          (
            <Fragment>
              <h1>Ожидайте звонка</h1>
              <Button onClick={onClose}>Закрыть</Button>
            </Fragment>
          ) : (
            <Fragment>
              <Helmet>
                <title>Оставьте нам ваши контактные данные и мы обязательно перезвоним Вам!</title>
                <meta property="og:title" content="SPTRIP.RU | Оставьте нам ваши контактные данные и мы обязательно перезвоним Вам!"/>
              </Helmet>
              <h2 className="text-center">Оставьте нам ваши контактные данные и мы обязательно перезвоним Вам!</h2>
              <Docker columns>
                <Docker className="bottom20">
                  <Input
                    theme={theme}
                    errorMessage={this.errorName}
                    style={{marginBottom: 10}}
                    placeholder="Имя"
                    capitalize
                    onChange={(val) => this.setState({name: val}, this.checkErrors)}
                    value={this.state.name}
                  />
                  <Input
                    theme={theme}
                    errorMessage={this.errorPhone}
                    style={{marginBottom: 10}}
                    mask="+1 (111) 111-11-11"
                    placeholder="Телефон"
                    onChange={(val) => this.setState({phone: val}, this.checkErrors)}
                    value={this.state.phone}
                  />
                </Docker>
                <Button theme={theme} onClick={this.sendMessage}>Жду звонка!</Button>
              </Docker>
              <StyledLink
                theme={theme}
                to={{
                  pathname: location.pathname,
                  state: {modal: true, returnTo: location.pathname}
                }}
              >
                Отправляя запрос, Вы подтверждаете &laquo;Согласие на обработку персональных данных&raquo;
              </StyledLink>
            </Fragment>
          )
        }
      </div>
    );
  }
}
