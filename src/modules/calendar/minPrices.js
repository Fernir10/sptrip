import React, {Component, Fragment, PureComponent} from 'react';
import ReactTooltip from 'react-tooltip';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import cn from 'classnames';
import styled from 'styled-components';

import superAgent from './../../utils/superAgent';
import {calcDate, formatPrice} from './../../utils/format';

import {Arrow, Flag, Select, Tabs} from './../../components';
import {SunIcon, VisaIcon, WaterIcon} from './../../icons';

import './minPrices.less';


const StyledTabs = styled(Tabs)`
  color: ${(props) => props.theme.color};
  padding: 1em 2em;
  background: linear-gradient(${(props) => props.theme.whiteBackgroundColor}, transparent) !important;
`;

const Column = ({height, history, country, index, curMonth, year, price, className}) => (
  <div
    style={{height}}
    {...({'data-content': index + 1})}
    className={cn('mp--c', className)}
    {...(price > 0 && {
      'data-tip': `от ${formatPrice(price || 0)} руб 6 ночей`,
      onClick: () => history.push(`/search/1/${country}/0/0/0/${calcDate(`${index + 1}.${curMonth + 1}.${year}`).getTime()}/${calcDate(`${index + 1}.${curMonth + 1}.${year}`, 14).getTime()}/7/14/1/0/0/0/0/0/1`)
    })}
  />
);

class Diagram extends PureComponent {
  render() {
    const {theme, history, columns = [], id: country = 1, year = '2018', temp = '+30', tempwater = '+26', monthName = 'Июнь', visa = 0, updated = '22.02.2011', countryName = 'Сибирь', changeMonth = () => {}, curMonth = 6} = this.props;

    const maxPrice = Math.max(...columns.filter(({price = 0}) => price > 0).map(({price = 0}) => price));
    const minPrice = Math.min(...columns.filter(({price = 0}) => price > 0).map(({price = 0}) => price));

    return (
      <Fragment>
        <div className="flex flex-between">
          <h3 className="flex flex-nowrap nomargin">
            <Flag size="medium" country={country} style={{marginRight: '15px'}}/>
            <span>
              {countryName}. {monthName} {year}
            </span>
            <span
              data-tip="Требуется виза"
            >
              {!!visa && (
                <VisaIcon
                  style={{marginLeft: 10}}
                />
              )}
            </span>
          </h3>
          <h3 className="flex nomargin flex-end">{isFinite(minPrice) ? `от ${formatPrice(minPrice)} руб.` : Array(30).fill('\u00a0').join('')}</h3>
        </div>
        <div className="flex flex-between">
          <div/>
          <h3 className="inline-flex flex-end">
            <div
              className="flex"
              data-tip="Температура воздуха"
            >
              <SunIcon/> {temp}
            </div>
            <div
              className="flex"
              style={{marginLeft: 20}}
              data-tip="Температура воды"
            >
              <WaterIcon/> {tempwater}
            </div>
          </h3>
        </div>
        <div className="mp">
          <Arrow
            size={20}
            horizontal
            color={theme.color}
            className="mp--btn"
            onClick={() => changeMonth(-1)}
          >
            &larr;
          </Arrow>
          <div className="mp--gridbg mp--gridbg--noborder">
            <div className="muted small">{isFinite(maxPrice) && `максимальная цена: ${formatPrice(maxPrice)} руб.`}</div>
          </div>
          <div className="mp--diag">
            <div className="mp--gridbg">
              {
                columns.map((column, index) => {
                  const height = Number(column.price || '0') / (maxPrice / 100);

                  return (
                    <Fragment key={`column-${index}`}>
                      <Column
                        {...{
                          price: column.price,
                          country,
                          history,
                          index,
                          curMonth,
                          year,
                          height: (column.price > 0) ? height : 0
                        }}
                      />
                    </Fragment>
                  );
                })
              }
            </div>
          </div>
          <Arrow
            size={20}
            color={theme.color}
            horizontal
            right
            className="mp--btn"
            onClick={() => changeMonth(1)}
          >
            &rarr;
          </Arrow>
        </div>
        <p className="flex muted small flex-end">* График цен: от 6 ночей на одного человека{updated && (`, обновлено ${updated}`)}</p>
      </Fragment>
    );
  }
}

@observer
export class MinPricesCalendar extends Component {
  lastIndex = 5;
  @observable.shallow list = [];
  @observable.shallow countryList = [];
  @observable isUpdating = false;
  @observable curMonth = (new Date()).getMonth();
  @observable curCountry = null;
  @observable selected = 0;

  country = (code) => {
    const country = this.countryList.find((c) => Number(c.id) === code);
    return country ? country.name : null;
  };

  constructor(props) {
    super(props);

    const {store: {calendar, search: {countries} = {}} = {}} = props;

    if (countries) {
      this.countryList = countries;
    } else {
      superAgent.form({type: 'country'}).then(({countries = []}) => this.countryList = countries);
    }

    if (calendar) {
      this.list = calendar;

      if (typeof window !== 'undefined') {
        ReactTooltip.rebuild();
      }
    } else {
      this.updateList();
    }
  }

  updateList = () => {
    this.isUpdating = true;
    superAgent.calendar({month: this.curMonth}).then((cal) => {
      this.list = cal;
      this.isUpdating = false;

      if (typeof window !== 'undefined') {
        ReactTooltip.rebuild();
      }
    });
  };

  changeMonth = (val) => {
    const sumMonth = this.curMonth + val;

    if (sumMonth < 0) {
      this.curMonth = 11;
    } else if (sumMonth > 11) {
      this.curMonth = 0;
    } else {
      this.curMonth = sumMonth;
    }

    this.updateList();
  };


  renderCountry = (foundCountry, props) => (
    <Diagram
      {...props}
      {...foundCountry}
      changeMonth={this.changeMonth}
      curMonth={this.curMonth}
    />
  );

  render() {
    const {theme, title, className, titleClassName, style} = this.props;
    const tabsOptions = [];

    const priorCountry = {Турция: 2000, Таиланд: 3000, Греция: 4000, Кипр: 5000, Тунис: 6000};

    const sorted = this.list.sort((a, b) => {
      const aWeight = (priorCountry[a.countryName] || 0) + a.id;
      const bWeight = (priorCountry[b.countryName] || 0) + b.id;

      return aWeight > bWeight ? -1 : 1;
    });

    sorted.slice(0, this.lastIndex).map((countryBlock) => {
      const {id, countryName} = countryBlock;

      return tabsOptions.push({
        key: id,
        title: (
          countryName
        ),
        content: (
          <Diagram
            {...this.props}
            {...countryBlock}
            curMonth={this.curMonth}
            changeMonth={this.changeMonth}
          />
        )
      });
    });

    const foundCountry = sorted.slice(this.lastIndex + 1, sorted.length).find((c) => c.id === this.curCountry);

    tabsOptions.push({
      key: 'select',
      title: (
        <Select
          theme={this.props.theme}
          size={8}
          nobg
          onChange={(val) => {
            this.curCountry = val;
            this.selected = this.lastIndex;
            this.updateList();
          }}
          value={this.curCountry}
          options={sorted.slice(this.lastIndex + 1, sorted.length).map((c) => ({value: c.id, title: c.countryName}))}
          style={{minWidth: 'auto'}}
        />
      ),
      content: (
        <Fragment>
          {this.renderCountry(foundCountry, this.props)}
        </Fragment>
      )
    });

    return (
      <Fragment>
        {tabsOptions && tabsOptions.length ? (
          <Fragment>
            {title && (
              <h2 className={titleClassName}>{title}</h2>
            )}
            <StyledTabs
              theme={theme}
              selected={this.selected}
              auto
              nobg
              parentClassName={cn('bottom20 top20 noselect', className)}
              tabs={tabsOptions}
              onClick={(index) => {
                if (index !== this.lastIndex) {
                  this.selected = index;
                }
                ReactTooltip.rebuild();

                return false;
              }}
              className="nopadding top20"
            />
            <ReactTooltip type="info"/>
          </Fragment>
        ) : null}
      </Fragment>
    );
  }
}
