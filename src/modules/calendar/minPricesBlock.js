import React, {Fragment, PureComponent} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {formatPrice} from './../../utils/format';
import {Card, Columns, Flag} from './../../components';

const StyledFlag = styled(Flag)`
  line-height: 50px;
  vertical-align: middle;
  margin-right: 10px;
  float: left;
`;

const StyledLink = styled(Link)`
  overflow: hidden;
`;


export class MinPricesBlock extends PureComponent {
  renderCountry = (block) => {
    const minPrice = Math.min(...block.columns.filter(({price = 0}) => price > 0).map(({price = 0}) => price));
    const replacedImage = block.img
      .replace('http:', '')
      .replace('url(', '')
      .replace(')', '')
      .replace('1600', '')
      .replace('slider', 'minprice')
      .replace('panorama', 'minprice');

    const img = `${replacedImage.slice(0, replacedImage.length - 5)}1.jpg`;

    return (
      <Card
        theme={this.props.theme}
        key={block.id}
        location={`/search/1/${block.id}`}
        margin={10}
        picture={img}
      >
        <div className="flex flex-between">
          <b>
            {block.countryName}
          </b>
          <div>
            от&nbsp;<b>{formatPrice(minPrice)}&nbsp;руб.</b>
          </div>
        </div>
      </Card>
    );
  };

  renderSmallCountry = (block, theme) => {
    const minPrice = Math.min(...block.columns.filter(({price = 0}) => price > 0).map(({price = 0}) => price));

    return (
      <StyledLink
        theme={theme}
        key={block.id}
        to={`/search/1/${block.id}`}
        className="mp--small"
      >
        <StyledFlag country={block.id} size="medium"/>
        <span className="desktop-only" style={{float: 'left'}}>{block.countryName} &mdash;&nbsp;от&nbsp;</span>
        {formatPrice(minPrice)}&nbsp;руб.
      </StyledLink>
    );
  };

  render() {
    const {theme, small, store: {calendar = []} = {}} = this.props;
    const startIndex = 1;
    const priorCountry = {Турция: 2000, Таиланд: 3000, Греция: 4000, Кипр: 5000, Тунис: 6000};

    const sorted = calendar.sort((a, b) => {
      const aWeight = (priorCountry[a.countryName] || 0) - a.id;
      const bWeight = (priorCountry[b.countryName] || 0) - b.id;

      return aWeight > bWeight ? -1 : 1;
    }).filter((c) => isFinite(Math.min(...c.columns.filter(({price = 0}) => price > 0).map(({price = 0}) => price))))
      .filter((c) => !c.visa);

    return (
      <Fragment>
        {!small ?
          (
            <div className="flex">
              {sorted.slice(startIndex, startIndex + 8).map((block) => this.renderCountry(block, theme))}
            </div>
          )
          : (
            <Columns className="top70">
              {sorted.slice(startIndex, startIndex + 8).map((block) => this.renderSmallCountry(block, theme))}
            </Columns>
          )}
      </Fragment>
    );
  }
}
