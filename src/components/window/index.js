import React, {PureComponent} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {CloseIcon} from './../../icons';

import './window.less';

const StyledWindow = styled.div`
  background-color: ${(props) => props.theme.backgroundColor || '#fff'};
  color: ${(props) => props.theme.color || '#000'};
`;

export class Window extends PureComponent {
  state = {
    isOpen: false
  };

  componentDidMount() {
    this.htmlStyle = document.querySelector('html').style;
    this.htmlStyle.overflow = 'hidden';

    document.addEventListener('keydown', (e) => {
      if (e.keyCode === 27) {
        this.props.history.push(this.props.location.state.returnTo);
      }
    }, {passive: true});
  }

  componentWillUnmount() {
    this.htmlStyle.overflow = '';
  }

  renderContent() {
    return null;
  }

  render() {
    const {location: {state: {returnTo} = {}} = {}, theme} = this.props;

    return (
      <div className="window--overlay">
        <StyledWindow theme={theme} className="window">
          <div className="window__content">
            {this.renderContent()}
          </div>
          <Link className="window__close" to={returnTo}>
            <CloseIcon color={theme.color} size={14}/>
          </Link>
        </StyledWindow>
      </div>
    );
  }
}
