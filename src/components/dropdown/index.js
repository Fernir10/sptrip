import React, {Fragment, PureComponent} from 'react';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';

const StyledDropdown = styled.div`
  user-select: none;
  position: absolute;
  align-self: flex-start;
  top: 100%;
  left: 0;
  right: 0;
  max-height: ${(props) => (props.height ? (Number(props.height) ? `${props.height}px` : props.height) : '250px')};
  background: ${(props) => props.theme.whiteBackgroundColor || '#fff'} !important;
  color: ${(props) => props.theme.color} !important;
  box-shadow: 0 0 0 1px ${(props) => props.theme.borderColor};
  margin-top: 5px;
  cursor: pointer;
  z-index: 1;
  overflow-y: auto;
  min-width: fit-content;
  ${(props) => toCSS(props.style)}
  will-change: opacity;
  transform: translateZ(0);
  backface-visibility: hidden;
  perspective: 1000px;
  transition: opacity .3s ease;
  
  ${(props) => (props.visible ? `
    visibility: visible;
    pointer-events: all;
    opacity: 1;
  ` : `
    visibility: hidden;
    pointer-events: none;
    opacity: 0;
  `)}
`;

export class Dropdown extends PureComponent {
  state = {
    itemsMaxCount: 10
  };

  setMaxItems = () => {
    if (this.list.scrollTop + this.list.offsetHeight >= this.list.scrollHeight - 30) {
      this.setState({itemsMaxCount: this.state.itemsMaxCount + 20});
    }
  };

  render() {
    const {theme, height, isOpened, className, style, children} = this.props;

    return (
      <Fragment>
        <StyledDropdown
          visible={isOpened}
          theme={theme}
          height={height}
          className={className}
          style={style}
          innerRef={(node) => { this.list = node; }}
          onScroll={this.setMaxItems}
        >
          {children && children.slice ? children.slice(0, this.state.itemsMaxCount).map((item) => item) : null}
        </StyledDropdown>
      </Fragment>
    );
  }
}
