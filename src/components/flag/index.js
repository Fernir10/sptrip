import styled from 'styled-components';

const sizes = {
  small: {width: 20, height: 13},
  medium: {width: 40, height: 26},
  big: {width: 60, height: 40}
};

export const Flag = styled.div`
  width: ${(props) => sizes[props.size].width}px;
  height: ${(props) => sizes[props.size].height}px;
  box-shadow: 0 0 0 1px rgba(0,0,0, .1);
  display: inline-block;
  background-image: url(/img/flags-${(props) => props.size}.jpg);
  background-repeat: no-repeat;
  ${(props) => (props.country ? `background-position: 0px ${(props.country - 1) * -sizes[props.size].height}px` : '')};
`;
