import styled from 'styled-components';

export const Panorama = styled.div`
  position: absolute;
  min-height: 220px;
  left: 0;
  right: 0;
  background: no-repeat 60%;
  background-size: cover;
  transform: translate3d(0, 0, 0);
  background: url(${(props) => props.img}) top center / cover no-repeat;
  height: ${(props) => props.size || 220}px;
  
  &:after {
    content: '';
    height: 100%;
    background: linear-gradient(to bottom, transparent 0%, rgba(250, 249, 240, 0.67) 30%, ${(props) => props.theme.backgroundColor} 100%);
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    display: block;
    z-index: 0;
  }
  
  @media only screen and (max-device-width: 600px) {
    min-height: 190px;
  }
`;
