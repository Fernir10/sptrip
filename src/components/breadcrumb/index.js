import React from 'react';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';

const StyledUL = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  ${(props) => toCSS(props.style)}
`;

const StyledLI = styled.li`
    background: ${(props) => props.bg};
    margin: ${(props) => props.size / 3}px ${(props) => props.size}px;
    display: inline-block;
    position: relative;
    cursor: pointer;
    height: ${(props) => props.size * 4}px;
    line-height: ${(props) => props.size * 4}px;
    text-decoration: none;
    color: ${(props) => props.text};

    &:hover {
      background: ${(props) => props.hover};

      &:before {
        border-right-color: ${(props) => props.hover};
      }

      &:after {
        border-right-color: ${(props) => props.hover};
      }

      & > * {
        &:before {
          border-left-color: ${(props) => props.hover};
        }
      }
    }

    &:before {
      position: absolute;
      content: '';
      width: 0;
      height: 0;
      border-top: 0 solid transparent;
      border-bottom: ${(props) => props.size * 2 + 1}px solid transparent;
      border-right: ${(props) => props.size}px solid ${(props) => props.bg};
      left: -${(props) => props.size}px;
      top: 0;
    }

    &:after {
      position: absolute;
      content: '';
      width: 0;
      height: 0;
      border-top: ${(props) => props.size * 2}px solid transparent;
      border-bottom: 0 solid transparent;
      border-right: ${(props) => props.size}px solid ${(props) => props.bg};
      left: -${(props) => props.size}px;
      bottom: 0;
    }

    & > * {
      display: block;
      height: ${(props) => props.size * 4}px;
      line-height: ${(props) => props.size * 4}px;
      margin: 0 ${(props) => props.size}px;
      text-decoration: inherit;
      color: inherit;
      font-size: ${(props) => props.size * 2}px;

      &:before {
        position: absolute;
        content: '';
        width: 0;
        height: 0;
        border-top: ${(props) => props.size * 2}px solid transparent;
        border-bottom: ${(props) => props.size * 2}px solid transparent;
        border-left: ${(props) => props.size}px solid ${(props) => props.bg};
        right: -${(props) => props.size}px;
        top: 0;
      }
    }
`;

export const Breadcrumb = ({size = 7, children = [], bg = '#eee', hover = '#fff', text = '#000'}) => (
  <StyledUL size={size}>
    {children.map((c, index) => (
      <StyledLI
        size={size}
        key={`bread-${index}`}
        bg={bg}
        hover={hover}
        text={text}
      >
        {c}
      </StyledLI>
    ))}
  </StyledUL>
);
