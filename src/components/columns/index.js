import React from 'react';
import styled from 'styled-components';

export const Columns = styled.div`
  @media only screen and (max-device-width: 440px) { column-count: 2; }
  @media only screen and (min-device-width: 440px) and (max-device-width: 720px) { column-count: 2; }
  @media only screen and (min-device-width: 720px) and (max-device-width: 1024px) { column-count: 3; }
  @media only screen and (min-device-width: 1024px) { column-count: 4; }

  width: 100%;
`;