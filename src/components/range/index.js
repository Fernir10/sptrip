import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import {formItem} from './../../decorators/formitem';

import './range.less';

@formItem
export class Range extends PureComponent {
    static propTypes = {
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      onChange: PropTypes.func,
      text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      style: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
      className: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    };

    setValue(value) {
      if (this.props.max && this.props.min) {
        if (value <= this.props.max && value >= this.props.min) {
          const tmpValue = Math.max(1, value);
          this.props.onChange(tmpValue);
        }
      } else if (this.props.max) {
        if (value <= this.props.max) {
          const tmpValue = Math.max(1, value);
          this.props.onChange(tmpValue);
        }
      } else if (this.props.min) {
        if (value >= this.props.min) {
          const tmpValue = Math.max(1, value);
          this.props.onChange(tmpValue);
        }
      } else {
        const tmpValue = Math.max(1, value);
        this.props.onChange(tmpValue);
      }
    }

    render() {
      const btnStyle = cn('range-button', {'range-button--small': this.props.small});
      return (
        <div className={cn('range', this.props.className)} style={this.props.style}>
          <div
            className={cn('range-left', btnStyle)}
            onClick={() => this.setValue(this.props.value - 1)}
            onMouseDown={() => this.int1 = setInterval(() => this.setValue(this.props.value - 1), 200)}
            onMouseUp={() => clearInterval(this.int1)}
          >
            +
          </div>
          <div className={cn('range-value', {'range-value--small': this.props.small})}>{this.props.text}</div>
          <div
            className={cn('range-right', btnStyle)}
            onClick={() => this.setValue(this.props.value + 1)}
            onMouseDown={() => this.int2 = setInterval(() => this.setValue(this.props.value + 1), 200)}
            onMouseUp={() => clearInterval(this.int2)}
          >
            +
          </div>
        </div>
      );
    }
}
