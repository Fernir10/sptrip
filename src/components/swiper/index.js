import React, {PureComponent} from 'react';
import cn from 'classnames';

import {Arrow} from './..';

import './swiper.less';
import {FullScreenIcon} from '../../icons';

export class Swiper extends PureComponent {
  state = {
    fullscreen: false,
    currentIndex: 0
  };

  maxDiff = 100;
  htmlStyle = null;
  isMoving = false;
  current = null;
  animationBlock = null;
  side = null;
  startPointX = 0;
  startPointY = 0;
  diffX = 0;
  diffY = 0;
  down = false;

  componentDidMount() {
    this.htmlStyle = document.querySelector('html').style;
  }

  moveToSide = (side = 'right') => {
    this.isMoving = true;
    this.current = this.state.currentIndex;
    this.side = side;

    this.moveNeedUpdate = true;
    this.animationBlock.style.transition = 'transform .5s ease';
    this.animationBlock.style.transform = `translate3d(${side === 'right' ? '-100%' : '100%'}, 0, 0)`;
  };

  animationend = () => {
    const {children: blocks = []} = this.props;

    if (this.side === 'left') {
      this.current--;
    } else {
      this.current++;
    }

    if (this.moveNeedUpdate) {
      this.setState({
        currentIndex: this.side === 'left' ? (this.current < 0 ? blocks.length - 1 : this.current) : (this.current > blocks.length - 1 ? 0 : this.current)
      }, () => {
        this.current = this.state.currentIndex;
      });

      this.animationBlock.style.transition = '';
      this.animationBlock.style.transform = '';
      this.isMoving = false;
      this.moveNeedUpdate = false;
    }
  };

  doMove = (side = 'left') => {
    if (!this.isMoving) {
      this.moveToSide(side, true);
    }
  };

  touchmove = (e) => {
    if (!e.touches) {
      e.preventDefault();
    }

    const touch = e.touches && e.touches[0] ? e.touches[0] : {screenX: e.clientX, screenY: e.clientY};

    if (this.down && touch) {
      this.diffX = touch.screenX - this.startPointX;
      this.diffY = Math.abs(touch.screenY - this.startPointY);
      if (Math.abs(this.diffY) < this.maxDiff) {
        this.animationBlock.style.transform = `translate3d(${this.diffX}px, 0, 0)`;
      }
    }
  };

  touchstart = (e) => {
    if (!e.touches) {
      e.preventDefault();
    }

    const touch = e.touches && e.touches[0] ? e.touches[0] : {screenX: e.clientX, screenY: e.clientY};
    this.down = true;
    this.animationBlock.style.transition = '';
    this.startPointX = touch ? touch.screenX : 0;
    this.startPointY = touch ? touch.screenY : 0;
    this.moveNeedUpdate = false;
  };

  touchend = (e) => {
    if (!e.touches) {
      e.preventDefault();
    }

    if (this.down) {
      this.down = false;

      if (Math.abs(this.diffY) < this.maxDiff && Math.abs(this.diffX) > this.maxDiff) {
        this.isMoving = true;
        this.side = this.diffX < 0 ? 'right' : 'left';
        this.animationBlock.style.transition = 'transform .2s ease';
        this.animationBlock.style.transform = `translate3d(${this.side === 'right' ? '-100%' : '100%'}, 0, 0)`;
        this.moveNeedUpdate = true;
      } else {
        this.isMoving = false;
        this.moveNeedUpdate = false;
        this.animationBlock.style.transition = '';
        this.animationBlock.style.transform = 'translate3d(0, 0, 0)';
      }
    }
  };

  fullscreen = (e) => {
    e.stopPropagation();
    this.setState({fullscreen: !this.state.fullscreen}, () => {
      this.htmlStyle.overflow = this.state.fullscreen ? 'hidden' : '';
    });
  };

  renderOutput = () => {
    const {children: blocks = []} = this.props;

    return [
      <div className="swiper--item" key="pane1">{blocks[this.state.currentIndex - 1] || blocks[blocks.length - 1]}</div>,
      <div className="swiper--item" key="pane2">{blocks[this.state.currentIndex]}</div>,
      <div className="swiper--item" key="pane3">{blocks[this.state.currentIndex + 1] || blocks[0]}</div>
    ];
  };

  renderThumbs = () => {
    const {children: blocks = []} = this.props;

    const outarr = [
      blocks[this.state.currentIndex - 1] || blocks[blocks.length - 1],
      blocks[this.state.currentIndex],
      blocks[this.state.currentIndex + 1] || blocks[0]
    ];

    return outarr.map((itm, index) => (
      <div
        className="swiper--thumb"
        onClick={() => {
          if (index === 0) {
            this.moveToSide('left');
          } else if (index === 2) {
            this.moveToSide('right');
          }
        }}
        key={`pane${index}`}
      >
        {itm}
      </div>
    ));
  };

  render() {
    const {theme, images, fullscreen, inner, color, thumbs, style, className} = this.props;

    return (
      <div className={cn({'swiper--full': this.state.fullscreen, 'swiper--images': images}, className)}>
        <div style={style} className={cn('swiper', {nothumbs: !thumbs})}>
          {!inner && (
            <Arrow color={color || theme.color} horizontal size={30} className="swiper--left" onClick={() => this.doMove('left')}/>
          )}
          <div
            className="swiper--pane"
            ref={(pane) => { this.pane = pane; }}
          >
            {inner && (
              <div className="swiper--left--inner" onClick={() => this.doMove('left')}>
                <Arrow color={color || theme.color} horizontal size={30} className="swiper--left"/>
              </div>
            )}
            <div
              className="swiper--pane--body"
              ref={(body) => { this.animationBlock = body; }}
              onTransitionEnd={this.animationend}
              onMouseDown={this.touchstart}
              onMouseMove={this.touchmove}
              onMouseUp={this.touchend}
              onMouseLeave={this.touchend}
              onTouchStart={this.touchstart}
              onTouchMove={this.touchmove}
              onTouchEnd={this.touchend}
            >
              {this.renderOutput()}
            </div>
            {inner && (<div className="swiper--right--inner" onClick={() => this.doMove('right')}>
              <Arrow color={color || theme.color} horizontal right size={30} className="swiper--right"/>
            </div>)}
            {fullscreen && (<FullScreenIcon color={color || theme.color} className={cn('swiper--fullscreen')} open={this.state.fullscreen} onClick={this.fullscreen}/>)}
          </div>
          {!inner && (
            <Arrow color={color || theme.color} horizontal right size={30} className="swiper--right" onClick={() => this.doMove('right')}/>
          )}
        </div>
        {thumbs && (
          <div className="swiper--thumbs">
            <div className="swiper--thumbs--body">
              {this.renderThumbs()}
            </div>
          </div>
        )}
      </div>
    );
  }
}
