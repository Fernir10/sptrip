import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './img.less';

export class Img extends Component {
  static propTypes = {
    src: PropTypes.string
  };

  componentDidMount() {
    if (this.img && this.props.lazy) {
      if (!('IntersectionObserver' in window)) {
        const img = document.createElement('img');
        img.src = this.props.src;

        img.onload = () => {
          if (this.img) {
            this.img.style.objectFit = '';
            this.img.src = img.src;
          }
        };
      } else {
        const observer = new IntersectionObserver((entries) => {
          entries.forEach((entry) => {
            observer.unobserve(entry.target);
            const img = new Image();
            img.src = this.props.src;

            img.onload = () => {
              if (this.img) {
                this.img.style.objectFit = '';
                this.img.src = img.src;
              }
            };
          });
        }, {
          threshold: 0.5
        });

        observer.observe(this.img);
      }
    }
  }

  render = () => (
    <img
      src={this.props.lazy ? '/img/preloader.gif' : this.props.src}
      onClick={this.props.onClick}
      ref={(node) => { this.img = node; }}
      className={cn('image', this.props.className)}
      alt={this.props.alt || ''}
      style={{...(this.props.lazy && {objectFit: 'scale-down'}), ...this.props.style}}
    />
  );
}

