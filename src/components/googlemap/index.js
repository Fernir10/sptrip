import React from 'react';
import {GoogleMap, Marker, withGoogleMap, withScriptjs} from 'react-google-maps';

const MapWithAMarker = withScriptjs(withGoogleMap((props) =>
  (<GoogleMap
    defaultZoom={17}
    defaultCenter={{
      lat: parseFloat(props.coord1),
      lng: parseFloat(props.coord2)
    }}
  >
    <Marker
      position={{
        lat: parseFloat(props.coord1),
        lng: parseFloat(props.coord2)
      }}
    />
  </GoogleMap>)
));

export const GMap = (props) => (
  <MapWithAMarker
    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyABL23TE0nj1_FacJI3VwBeImGbO97Igqo&v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div style={{height: '100%'}}/>}
    containerElement={<div style={props.style} className={props.className}/>}
    mapElement={<div style={{height: '100%'}}/>}
    {...props}
  />
);
