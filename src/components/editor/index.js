import React, {Component} from 'react';
import Editor from 'react-pell';
import {exec} from 'pell';
import sanitizeHtml from 'sanitize-html';

import './editor.less';

export class RTF extends Component {
  onChange = (html) => {
    if (this.props.onChange) {
      this.props.onChange(sanitizeHtml(html, {
        allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img']),
        allowedAttributes: {
          a: ['href', 'name', 'target'],
          img: ['src']
        }
      }));
    }
  };

  render() {
    return (
      <Editor
        contentClass="custom-pell"
        actionBarClass={this.props.actionBarClass}
        containerClass={this.props.containerClass}
        buttonClass={this.props.buttonClass}
        actions={['bold', 'italic', 'underline', 'strikethrough', 'heading1', 'heading2',
          {
            name: 'heading3',
            icon: '<b>H<sub>3</sub></b>',
            title: 'Heading 3',
            result: () => exec('formatBlock', '<h3>')
          },
          {
            name: 'heading4',
            icon: '<b>H<sub>4</sub></b>',
            title: 'Heading 4',
            result: () => exec('formatBlock', '<h4>')
          },
          'paragraph', 'quote', 'olist', 'ulist', 'code', 'line', 'link', 'image']}
        defaultContent={sanitizeHtml(this.props.value, {
          allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img']),
          allowedAttributes: {
            a: ['href', 'name', 'target'],
            img: ['src']
          }
        })}
        onChange={this.onChange}
      />
    );
  }
}
