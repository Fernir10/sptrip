import React, {PureComponent} from 'react';
import cn from 'classnames';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';
import {formItem} from './../../decorators/formitem';
import {paranja} from './../../decorators/paranja';

import {Arrow, Dropdown} from './..';

import './select.less';

@formItem
@paranja
class SelectUnstyled extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isOpened: false,
      focus: false,
      selectedItem: null,
      searchWord: '',
      size: null
    };

    this.componentWillReceiveProps(props);
  }

  componentWillReceiveProps(props) {
    this.setValue(props.value, props.options);
  }

  onSelectClick = (e) => {
    if (this.state.isOpened || this.props.options.length < 2) {
      this.close();
    } else {
      this.onSearchClick(e);
      this.setState({
        isOpened: true
      });
    }
  };

  onSearch(e) {
    this.setState({searchWord: e.target.value});
  }

  onOptionClick(opt) {
    this.close();
    if (this.state.selectedItem !== opt.value) {
      this.props.onChange(opt.value);
    }
  }

  setValue(value, options) {
    let initOption;

    if (options && options.length) {
      if (value != null) {
        initOption = options[
          options.findIndex((opt) => opt.value == value)
        ];
      } else {
        initOption = options[0];
      }
    } else {
      initOption = null;
    }

    this.state.searchWord = initOption ? initOption.title : '';
    this.state.selectedItem = initOption;
  }

  close() {
    this.setState({
      isOpened: false
    });
  }

  onSearchClick = (e) => {
    e.stopPropagation();
    if (this.input) {
      this.input.focus();
    }
  };

  renderOptions(opts, disabled = false) {
    if (opts && opts.length) {
      if (this.props.withSearch && this.state.searchWord) {
        const re = new RegExp(this.state.searchWord, 'gi');

        opts = opts.filter((opt) => opt.title.match(re));
      }

      return opts.map((opt, i) => this.renderOption(opt, i, disabled));
    }
    return null;
  }

  renderOption(opt, i) {
    const selected = this.state.selectedItem != null && opt.value === this.state.selectedItem.value;
    const optionClassNames = cn('select-row__option', {
      'select-row__option--selected': selected,
      'select-row__option--disabled': opt.disabled,
      'select-row__option--group-title': opt.static,
      [`select-row__option--${this.props.size}`]: this.props.size
    });

    return (
      <div
        className={optionClassNames}
        key={`option${i}-${opt.title}-${opt.value}`}
        onClick={!opt.disabled ? () => this.onOptionClick(opt) : (e) => {
          e.stopPropagation();
        }}
      >
        <span>{opt.title}</span>
        {opt.label ?
          <label className="select-row__option__label">
            {opt.label}
          </label>
          : null}
      </div>
    );
  }

  renderSearchOption() {
    return (
      <div className="select-row__option--search" onClick={this.onSearchClick}>
        <input
          type="text"
          ref={(node) => { this.input = node; }}
          className="select-row__option__input-search"
          placeholder="Введите любые буквы поиска"
          value={this.state.searchWord}
          onFocus={() => this.setState({isOpened: true})}
          onChange={(e) => this.onSearch(e)}
        />
      </div>
    );
  }

  render() {
    const {theme, className, nobg, withSearch, bordered, placeholder, options = [], size} = this.props;

    return (
      <div
        className={cn(
          'select-row',
          className,
          {
            'select-row--nobg': nobg,
            'select-row--bordered': bordered,
            'select-row--relative': this.state.isOpened
          })}
        onClick={this.onSelectClick}
      >
        <label className="formitem__label">{placeholder}</label>
        <div className={cn('select-text--container', {'select-text--container--single': !placeholder})}>
          {
            this.state.selectedItem && (
              withSearch ? this.renderSearchOption() : (
                <div className={cn('select-row__selected', {'select-row__selected--single': !placeholder})}>
                  <div className="select-row--text">
                    {this.state.selectedItem.title}
                  </div>
                </div>
              ))
          }
          {
            options && options.length > 1 ? <Arrow color={theme ? theme.color || '#000000' : '#000000'} isOpened={this.state.isOpened} size={size || 12} style={{marginLeft: '5px'}}/> : null
          }
        </div>
        <Dropdown theme={this.props.theme} isOpened={this.state.isOpened}>
          { this.renderOptions(options) }
        </Dropdown>
      </div>
    );
  }
}

export const Select = styled(SelectUnstyled)`${(props) => toCSS(props.style)}`;
