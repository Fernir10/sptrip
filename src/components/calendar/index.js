import React, {PureComponent} from 'react';
import cn from 'classnames';
import styled from 'styled-components';

import {formItem} from './../../decorators/formitem';
import {paranja} from './../../decorators/paranja';

import {CloseIcon} from '../../icons';
import {Dropdown} from './..';

import {ModifyColor} from './../../utils/color';
import {fullMonths, fullMonthsOne} from './../../utils/format';

import './calendar.less';

const daynames = ['вc', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];

const StyledWeek = styled.td`
  text-align: center;
  height: 30px;
  line-height: 30px;
  border: 2px solid transparent;
  margin: auto;
  padding: 0 .5em;
  color: ${(props) => props.theme.orangeLightColor} !important;
`;

const StyledMonth = styled.td`
  color: ${(props) => props.theme.orangeLightColor} !important;
`;

const StyledMonths = styled.div`
  background: ${(props) => ModifyColor(props.theme.backgroundColor, -10)};
  color: ${(props) => props.theme.color};
`;

const StyledMonthsActive = styled.div`
  background: ${(props) => props.theme.whiteBackgroundColor};
  color: ${(props) => props.theme.color};
`;

const DumbText = ({date}) => {
  let dateRow;
  let rangeMonthText = 'Выберите дату';
  let dayName;
  if (date) {
    dateRow = <div className="cld__range-date">{date.getDate()}</div>;
    rangeMonthText = `${fullMonths[date.getMonth()]}`;
    dayName = daynames[date.getDay()];
  }

  return (
    <div className="cld__from-date">
      {dateRow}
      <div>
        <div className="cld__range-month">{rangeMonthText}</div>
        <div className="cld__range-day">{dayName}</div>
      </div>
    </div>
  );
};

const Range = ({dateFrom, dateTo, onClick, focused}) => (
  <div
    className={cn('cld__range', {'cld__range--focused': focused})}
    onClick={onClick}
  >
    <DumbText date={dateFrom ? new Date(dateFrom) : dateFrom}/>
    <div>&mdash;</div>
    <DumbText date={dateTo ? new Date(dateTo) : dateTo}/>
  </div>
);

class CalendarTable extends PureComponent {
  constructor(props) {
    super(props);
    this.currentDate = new Date();
  }

  componentDidMount() {
    this.clickMonth(new Date(this.props.indexStart).getMonth() || this.currentDate.getMonth());
  }

  mouseOver = (index) => {
    if (this.selectionEnabled) {
      this.props.hoverRange(this.props.indexStart, index);
    }
  };

  handleClick = (index) => {
    const {setRange, indexStart, indexEnd} = this.props;

    if (this.selectionEnabled) {
      if ((index === indexStart)) {
        this.selectionEnabled = false;
        setRange(indexStart, indexEnd);
      } else {
        this.selectionEnabled = false;
        setRange(indexStart, index);
      }
    } else if (index === indexEnd) {
      this.selectionEnabled = true;
    } else {
      this.selectionEnabled = true;
      setRange(index);
    }
  };

  onWheel = (e) => {
    e.stopPropagation();
    const top = this.calendar2.parentNode.scrollTop;
    const height = this.calendar2.offsetHeight;
    const percent = Math.min(83.3, Math.max(0, top) / (height / 100));

    if (this.calendar2.style.transition === '') {
      this.calendar2.style.transition = 'top .2s ease';
    }

    this.smallcalHover.style.top = `${percent}%`;
  };

  clickMonth = (month) => {
    const monthRef = this.monthRefs[month] ? this.monthRefs[month].ref : false;

    if (monthRef) {
      const height = this.calendar2.offsetHeight;
      const tbodyheight = monthRef.offsetHeight;
      const top = -(monthRef).offsetTop;
      const percent = Math.max(-83.3, Math.min(10, top) / (height / 100));

      if (this.calendar2.style.transition !== '') {
        this.calendar2.style.transition = '';
      }

      this.smallcalHover.style.top = `${-percent}%`;
      this.calendar2.parentNode.scrollTop = -(Math.max(-(height - (tbodyheight * 1.8)), Math.min(10, top)));
    }
  };

  getDay(date, key, month) {
    const time = date.getTime();
    const {indexEnd, indexStart} = this.props;
    const dayClass = cn('cld__day', {
      selected: (time >= indexStart && time <= indexEnd) || (time >= indexEnd && time <= indexStart),
      dis: (date.getMonth() !== month),
      'out--range': (date <= this.currentDate.setHours(0)),
      'sel--start': (time === indexStart && indexEnd >= indexStart),
      'sel--end': (time === indexEnd && time >= indexStart)
    });

    return ((date.getMonth() !== month) ? <td key={key}>&nbsp;</td> : <td
      className={dayClass}
      {...((date <= this.currentDate.setHours(0)) ? {} : {
        onClick: () => this.handleClick(time),
        onMouseOver: () => this.mouseOver(time)
      })}
      key={key}
    >
      <div className="cld__in-day">
        {date.getDate()}
      </div>
    </td>);
  }

  render() {
    const {theme, date} = this.props;
    const curdate = new Date();
    let curYear = curdate.getFullYear();
    const curMonth = curdate.getMonth();
    let cdate;

    const monthArray = [];
    let monthStep = curMonth;
    for (let i = 0; i < 12; i++) {
      monthArray.push(monthStep);
      monthStep = monthStep + 1 > 11 ? 0 : monthStep + 1;
    }

    return (
      <div
        className="cld--calendar"
        onWheel={this.onWheel}
      >
        <StyledMonths theme={theme} className="cld--calendar--months">
          <StyledMonthsActive theme={theme} innerRef={(node) => { this.smallcalHover = node; }} className="cld--calendar--months--hover"/>
          {
            monthArray.map((m) => (
              <div
                key={`months-${m}`}
                className="cld--calendar--month"
                onClick={() => this.clickMonth(m)}
              >
                {fullMonthsOne[m]}
              </div>
            ))
          }
        </StyledMonths>
        <div className="cld__wrap">
          <table className="cld__table" ref={(node) => { this.calendar2 = node; }} key={date} cellSpacing="0" cellPadding="0">
            {
              monthArray.map((m) => {
                if (m === 0) {
                  curYear++;
                }

                cdate = new Date(curYear, m, 1);

                const firstDay = cdate.getDay();

                if (firstDay !== 1) {
                  if (firstDay === 0) {
                    cdate.setDate(cdate.getDate() - 6);
                  } else {
                    cdate.setDate(cdate.getDate() - (firstDay - 1));
                  }
                }

                cdate.setDate(cdate.getDate() - 1);

                return (
                  <tbody
                    key={m}
                    ref={(node) => {
                      this.monthRefs = this.monthRefs || [];
                      this.monthRefs[m] = this.monthRefs[m] || {};
                      this.monthRefs[m].ref = node;
                    }}
                  >
                    <tr>
                      <StyledMonth theme={theme} colSpan={7} className="cld__table__month">{fullMonthsOne[m]} {curYear}</StyledMonth>
                    </tr>
                    <tr>
                      {'пн вт ср чт пт сб вс'.split(' ').map((dayname) => <StyledWeek key={`${dayname}-${m}`} theme={theme}>{dayname}</StyledWeek>)}
                    </tr>
                    {
                      Array(6).fill(0).map((d, w) => (
                        <tr key={w}>
                          {
                            Array(7).fill(0).map((ds, dd) => {
                              cdate.setDate(cdate.getDate() + 1);
                              return this.getDay(cdate, dd, m);
                            })
                          }
                        </tr>
                      ))
                    }
                  </tbody>
                );
              })
            }
          </table>
        </div>
      </div>
    );
  }
}

@formItem
@paranja
export class Calendar extends PureComponent {
  constructor(props) {
    super(props);

    const {dateFrom = 0, dateTo = 0} = props;

    this.state = {
      isOpened: false,
      date: Date.now(),
      selectionStart: dateFrom,
      selectionStartHover: dateFrom,
      selectionEnd: dateTo,
      selectionEndHover: dateTo
    };
  }

  hoverRange = (selectionStartHover = 0, selectionEndHover = 0) => {
    this.setState({selectionStartHover, selectionEndHover});
  };

  setRange = (selectionStart = 0, selectionEnd = 0) => {
    const {onChange} = this.props;

    this.setState({selectionStart, selectionEnd});

    if (selectionEnd) {
      if (selectionStart > selectionEnd) {
        this.setState({selectionStart: selectionEnd, selectionEnd: selectionStart});
        onChange((new Date(selectionEnd)).getTime(), (new Date(selectionStart)).getTime());
      } else {
        onChange((new Date(selectionStart)).getTime(), (new Date(selectionEnd)).getTime());
      }
      this.setState({isOpened: false});

      if (window.outerWidth <= 760) {
        this.htmlElement.style.overflow = '';
      }
    }
  };

  htmlElement = null;

  componentDidMount() {
    this.htmlElement = document.querySelector('body');
  }

  toggle = () => {
    if (window.outerWidth <= 760) {
      this.htmlElement.style.overflow = this.state.isOpened ? '' : 'hidden';
    }

    this.setState({isOpened: !this.state.isOpened});
  };

  render() {
    const {theme, style, className} = this.props;
    const {date, selectionStart, selectionEndHover, isOpened, selectionEnd} = this.state;

    return (
      <div className={cn('cld', className)} style={style}>
        <Range onClick={this.toggle} dateFrom={selectionStart} dateTo={selectionEnd} focused={isOpened}/>
        <Dropdown theme={theme} isOpened={isOpened} className="cld-container">
          <div className="cld--close" onClick={this.toggle}>
            <CloseIcon color={theme.color} size={12}/>
          </div>
          <CalendarTable
            theme={theme}
            date={date}
            indexStart={selectionStart}
            indexEnd={selectionEndHover}
            setRange={this.setRange}
            hoverRange={this.hoverRange}
          />
        </Dropdown>
      </div>
    );
  }
}
