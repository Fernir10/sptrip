import React from 'react';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';

const StyledPolyline = styled.polyline`
  stroke: ${(props) => props.color};
  fill: none;
`;


export const Arrow = styled(({right, horizontal, className, style, size = 12, color = '#000', isOpened, onClick = () => {}}) => (horizontal ?
  (
    <svg className={className} viewBox="0 0 4.038 6.661" width={size} height={size} onClick={onClick}>
      <StyledPolyline color={color} points="3.684,6.308 0.707,3.331 3.684,0.354"/>
    </svg>
  ) :
  (
    <svg className={className} viewBox="0 0 6.661 4.038" width={size} height={size} onClick={onClick}>
      <StyledPolyline color={color} points="6.308,0.354 3.331,3.331 0.354,0.354"/>
    </svg>
  )))`
    transform-origin: 50% 50%;
    transition: transform .2s ease;
    ${(props) => (props.isOpened ? 'transform: rotate(180deg);' : '')}
    ${(props) => (props.right ? 'transform: rotateZ(180deg);' : '')}
    width: ${(props) => props.size}px;
    height: ${(props) => props.size}px;
    min-width: ${(props) => props.size}px;
    min-height: ${(props) => props.size}px;
    ${(props) => toCSS(props.style)}
  `;
