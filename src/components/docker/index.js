import React, {Fragment} from 'react';
import cn from 'classnames';
import styled from 'styled-components';

import './docker.less';

const DockerItem = styled(({itm, padding = 5, index, arr, className}) => (
  <div
    className={cn('docker--item', {
      mobile: (itm.props && itm.props.className && itm.props.className.indexOf('mobile') !== -1),
      'docker--first': index === 0,
      'docker--last': index === arr.length - 1
    }, className)}
  >
    <div className="docker--item--child">
      {itm}
    </div>
  </div>
))`
    margin-bottom: ${(props) => props.nobottom ? 0 : props.padding}px;
    margin-right: ${(props) => props.padding}px;
`;

export const Docker = ({nobottom, padding = 5, inline, children = [], columns, style, top, className}) => (
  <Fragment>
    {children.length ? (
      <div
        className={cn('docker', {
          'docker--columns': columns,
          'docker--top': top,
          'docker--inline': inline
        }, className)} style={style}
      >
        {children.map((itm, index) => (
          itm ? (
            <DockerItem key={`docker-${index}`} {...{itm, index, arr: children, padding, nobottom}}/>
          ) : null))}
      </div>
    ) : null}
  </Fragment>
);
