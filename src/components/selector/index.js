import React, {PureComponent} from 'react';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';

import {formItem} from './../../decorators/formitem';


const Option = styled.option`
    background: ${(props) => props.theme.whiteBackgroundColor || '#ffffff'};
    color: ${(props) => props.theme.color || '#000000'};
    -webkit-appearance: none;
    outline: none;
    font-size: 15px;

    &:disabled {
      display: none;
    }
`;

const Select = styled.select`
    width: 100%;
    height: 30px;
    border: none;
    box-shadow: inset 0 0 0 1px rgba(0, 0, 0, .5);
    display: flex;
    padding: 0 10px;
    outline: none;
    position: relative;

    &:disabled {
      background: #9c9c9c;
    }

    &:before {
      content: 'добавить ребенка';
      display: flex;
      position: absolute;
    }
    ${(props) => toCSS(props.style)}
`;

@formItem
export class Selector extends PureComponent {
  static defaultProps = {
    value: '',
    options: []
  };

  onChange = (e) => {
    const {onChange, options} = this.props;

    if (onChange) {
      const {value} = options[e.target.selectedIndex - 1] || {};

      if (value) {
        onChange(value);
      }
    }
  };

  render() {
    const {theme, disabled, style, value, className} = this.props;

    return (
      <Select
        theme={theme}
        disabled={disabled}
        className={className}
        style={style}
        onChange={this.onChange}
        value={value}
      >
        <Option
          theme={theme}
          disabled
          key="first-option"
        >
          {value}
        </Option>
        {this.props.options.map((itm) => (
          <Option
            theme={theme}
            key={itm.title}
            value={itm.value}
          >
            {itm.title}
          </Option>
        ))}
      </Select>
    );
  }
}
