import React, {Fragment, PureComponent} from 'react';
import ReactTooltip from 'react-tooltip';
import cn from 'classnames';
import styled from 'styled-components';

import superAgent from './../../utils/superAgent';

import {formItem} from './../../decorators/formitem';
import {paranja} from './../../decorators/paranja';

import {LensIcon, VisaIcon} from './../../icons';
import {CloseIcon, Dropdown, Flag} from './..';

import './autocomplete.less';

const StyledVisa = styled(VisaIcon)`
  margin-left: 10px;
  height: 8px;
`;

const StyledInput = styled.input`
  background: ${(props) => props.theme.whiteBackgroundColor} !important;
  color: ${(props) => props.theme.color} !important;
`;

const styleIcon = `
  position: absolute;
  right: 15px;
  height: 100%;
  width: 20px;
  top: 0px;  
  font-size: 30px;
  vertical-align: baseline;
  color: #555;
`;

const StyledCloseIcon = styled(CloseIcon)`
  ${styleIcon}
  cursor: pointer;
  opacity: .6;

  &:hover {
    opacity: 1;
  }
`;

const StyledLensIcon = styled(LensIcon)`${styleIcon}`;

const StyledOption = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  padding: 2px 10px;
  border-bottom: none;
  min-height: 40px;
  font-size: 14px;

  &:hover {
    background: ${(props) => props.theme.darkColor};
    color: ${(props) => props.theme.whiteTextColor};
  }
  
  ${(props) => (props.selected ? `
    background: ${(props) => props.theme.darkColor};
    color: ${(props) => props.theme.whiteTextColor};
  ` : '')}
`;

@formItem
@paranja
class UnstyledAutoComplete extends PureComponent {
  results = [];
  options = [];

  static defaultProps = {
    value: '',
    size: 0,
    placeholder: 'Введите любые буквы поиска',
    default: []
  };

  constructor(props) {
    super(props);
    this.results = props.default || [];
  }

  state = {
    isOpened: false,
    query: this.props.value
  };

  componentWillReceiveProps(newProps) {
    if (JSON.stringify(this.props) !== JSON.stringify(newProps)) {
      this.setState({
        query: newProps.value
      });
    }

    if (this.state.isOpened) {
      this.input.focus();
    }
  }

  onClick = () => {
    this.onFocus();
  };

  onFocus = () => {
    const {default: defaultOptions = []} = this.props;

    this.results = defaultOptions;

    this.setState({
      isOpened: this.results.length > 0
    }, () => {
      ReactTooltip.rebuild();

      if (this.index && this.options[this.index] && this.container) {
        this.container.scrollTop = this.options[this.index].offsetTop;
      }
    });
  };

  onQuery = (e) => {
    const {size = 0, res, departure, default: defaultOptions = []} = this.props;

    if (e.target.value.length > size) {
      if (res) {
        superAgent.autocomplete({
          query: e.target.value,
          ...(departure ? {departure} : {})
        }).then((data) => {
          this.results = data || [];
        });
      } else {
        const re = new RegExp(e.target.value, 'gi');
        this.results = defaultOptions.filter((a) => a.title.match(re)) || [];
      }
    } else {
      this.results = defaultOptions;
    }

    this.setState({
      query: e.target.value
    });

    ReactTooltip.rebuild();
  };

  onChange = (val, index) => {
    this.results = [];
    this.index = index;
    this.setState({
      isOpened: false,
      query: val.name || val.title
    });

    if (this.props.onChange) {
      this.props.onChange(val);
    }
  };

  drawTitle = (res) => {
    if (res.name) {
      if (res.hasOwnProperty('rating')) {
        return `отель ${res.name.toLowerCase()}, ${res.cn}`;
      } else if (res.country && res.id !== res.country) {
        return `курорт ${res.name.toLowerCase()}, ${res.cn}`;
      }

      return res.name;
    }

    return res.title;
  };

  render() {
    const {theme, className, title, placeholder, onRemove, flags} = this.props;
    const {query, isOpened} = this.state;

    const rexq = query ? new RegExp(query, 'gi') : '';
    return (
      <div className={cn('aut', className, {'aut--focused': isOpened})}>
        <div
          className={cn('aut--pl', {'aut--pl--focused': isOpened})}
          onClick={this.onClick}
        >
          {title && (
            <label className="formitem__label">{title}</label>
          )}
          <label htmlFor="aut-input" className="aut--pl--in">{query || placeholder}</label>
        </div>
        <StyledInput
          theme={theme}
          id="aut-input"
          innerRef={(node) => { this.input = node; }}
          className={cn('aut--input', {'aut--input--focused': isOpened})}
          type="text"
          onChange={this.onQuery}
          value={query}
          autoComplete="off"
          maxLength={100}
        />
        {query ? (
          <StyledCloseIcon
            color={theme.color}
            size={12}
            onClick={() => {
              onRemove();
              this.results = this.props.default || [];
              this.setState({query: ''});
            }}
          />
        ) : (
          <StyledLensIcon
            onClick={this.onClick}
          />
        )}
        {this.results && this.results.map ?
          <Dropdown
            theme={this.props.theme}
            isOpened={isOpened}
            ref={(n) => { this.container = n; }}
          >
            { this.results.map((res, index) => (
              <Fragment key={res.id || res.value}>
                {flags ? (
                  <StyledOption
                    theme={theme}
                    innerRef={(n) => this.options[index] = n}
                    selected={this.index === index}
                    onClick={(e) => {
                      e.stopPropagation();
                      this.onChange(res.value ? {id: res.value, name: res.title, country: res.value} : res, index);
                      this.setState({isOpened: false});
                    }}
                  >
                    {(res.value || (res.id && !res.country)) && <Flag country={res.value} size="small" className="aut--flag"/>}
                    <span dangerouslySetInnerHTML={{__html: `${this.drawTitle(res)}`.replace(rexq, `<b>${query || ''}</b>`)}}/>
                    {((res.value || (res.id && !res.country)) && !!res.visa) && (
                      <span data-tip="Требуется виза">
                        <StyledVisa/>
                      </span>
                    )}
                  </StyledOption>
                ) : (
                  <StyledOption
                    theme={theme}
                    innerRef={(n) => this.options[index] = n}
                    selected={this.index === index}
                    onClick={(e) => {
                      e.stopPropagation();
                      this.onChange(res.value ? {id: res.value, name: res.title, country: res.value} : res, index);
                      this.setState({isOpened: false});
                    }}
                    dangerouslySetInnerHTML={{__html: `${res.name || res.title}`.replace(rexq, this.state.query ? `<b>${this.state.query}</b>` : '')}}
                  />
                )}
              </Fragment>))
            }
            <ReactTooltip type="info"/>
          </Dropdown> : null}
      </div>
    );
  }
}

export const AutoComplete = styled(UnstyledAutoComplete)`${(props) => props.style}`;
