import React, {Component} from 'react';
import PropTypes from 'prop-types';

export class IFrame extends Component {
    static propTypes = {
      src: PropTypes.string,
      className: PropTypes.oneOfType([PropTypes.object, PropTypes.string])
    };

    componentDidMount() {
      const iframe = document.createElement('script');
      iframe.type = 'text/javascript';
      iframe.defer = true;
      iframe.async = true;
      iframe.charset = 'utf-8';
      iframe.src = this.props.src;
      this.iframe.appendChild(iframe);
    }

    render() {
      return (
        <div className={this.props.className} style={this.props.style}>
          <div ref={(node) => { this.iframe = node; }}/>
        </div>
      );
    }
}

