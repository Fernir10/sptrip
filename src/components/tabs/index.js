import React, {PureComponent} from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {ModifyColor} from './../../utils/color';

import './tabs.less';

const StyledTab = styled.div`
  ${(props) => !props.nobg && `
    background: ${props.theme.whiteBackgroundColor};
    color: ${props.theme.color};
  `}
`;

const StyledTabButton = styled.div`
  ${(props) => !props.nobg && `
    background: ${(props.selected ? props.theme.whiteBackgroundColor : ModifyColor(props.theme.whiteBackgroundColor, -20))};
    color: ${props.theme.color};
  
    &:hover {
      background-color: ${props.theme.whiteBackgroundColor};
    }
  `}
`;

export class Tabs extends PureComponent {
  static propTypes = {
    tabs: PropTypes.array
  };

  state = {
    selected: 0
  };

  render() {
    const {theme, tabs = [], className, parentClassName, style, auto, nobg, onClick, selected} = this.props;

    return (
      <StyledTab nobg={nobg} theme={theme} className={cn('tab', {'tab--nobg': nobg}, parentClassName)}>
        <div className="tabs">
          {
            tabs.map((tab, index) => (
              <StyledTabButton
                nobg={nobg}
                theme={theme}
                key={tab.key || `title-${index}`}
                selected={(selected || this.state.selected) === index}
                onClick={() => {
                  if (tab.onClick) {
                    if (tab.onClick(index) === false) {
                      return;
                    }
                  }
                  this.setState({selected: index});
                }}
                className={cn('tabs-tab', {'tabs-tab-active': (selected || this.state.selected) === index, 'tabs-tab--auto': auto})}
              >
                {tab.title}
              </StyledTabButton>
            ))
          }
        </div>
        <div className={cn('tabs-container', className)} style={style}>
          {!!tabs[selected || this.state.selected] && tabs[selected || this.state.selected].content}
        </div>
      </StyledTab>
    );
  }
}

