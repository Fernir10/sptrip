import React, {PureComponent} from 'react';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';

const TIMER = 150; // Milliseconds between moving the next block
const DEF_SIZE = 60; // Pixels height/width
const GUTTER = 5; // Spacing in percentage between tiles

const Holder = styled.div`
  width: 70%;
  height: 70%;
  margin-left: 15%;
  margin-top: 15%;
  position: relative;
  transform: rotate(-45deg);
  
  @media only screen and (max-device-width: 600px) {
    width: 30%;
    height: 30%;
    margin-left: 30%;
    margin-top: 30%;
  }
`;

const StyledLoader = styled.div`
  ${(props) => toCSS(props.styles)}
  pointer-events: none;
  position:absolute;
  left: 50%;
  top:50%;
  transform: translate(-50%, -50%);

  @keyframes moveGradient {
    to {
      background-position: 100% 50%;
    }
  }
`;

const Tile = styled.div`
        ${(props) => toCSS(props.styles)}
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0; top: 0;
        background: linear-gradient(
                to right,
                #141562,
                #486FBC,
                orange,
                darkorange,
                orangered,
                #F4915E) 0 50%;
        background-size: 1000% 1000%;
        overflow: hidden;
        animation: moveGradient 15s infinite;
      `;


export class Spinner extends PureComponent {
  state = {
    positions: {
      1: 'alpha',
      2: 'bravo',
      3: 'charlie',
      4: null,
      5: 'delta',
      6: 'echo',
      7: 'foxtrot'
    },
    stateNumber: 0
  };

  componentDidMount() {
    this.setTimer(TIMER);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  setTimer = (time) => {
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.timer = setInterval(this.setNextState, time);
  };

  setNextState = () => {
    const currentPositions = this.state.positions;
    const emptyIndex = this.positionForTile(null);
    const indexToMove = this.tileIndexToMove();
    const newPositions = Object.assign({}, currentPositions, {
      [indexToMove]: null,
      [emptyIndex]: currentPositions[indexToMove]
    });

    const currentState = this.state.stateNumber;
    const nextState = (currentState === 7) ? 0 : currentState + 1;


    this.setState({stateNumber: nextState, positions: newPositions});
  };

  tileIndexToMove = () => {
    switch (this.state.stateNumber) {
      case 0: return 7;
      case 1: return 6;
      case 2: return 5;
      case 3: return 4;
      case 4: return 3;
      case 5: return 2;
      case 6: return 1;
      case 7: return 4;
      default: return 1;
    }
  };

  positionForTile = (radioCommand) => {
    for (const position in this.state.positions) {
      const tile = this.state.positions[position];

      if (tile === radioCommand) {
        return position;
      }
    }

    return null;
  };

  clipPathForPosition = (position) => {
    position = parseInt(position, 10);
    const SIZE = (100 - (2 * GUTTER)) / 3;
    const VAR0 = '0% ';
    const VAR1 = `${SIZE + GUTTER}% `;
    const VAR2 = `${(2 * SIZE) + (2 * GUTTER)}% `;
    switch (position) {
      case 1: return `inset(${VAR1}${VAR2}${VAR1}${VAR0} round 5%)`;
      case 2: return `inset(${VAR0}${VAR2}${VAR2}${VAR0} round 5%)`;
      case 3: return `inset(${VAR0}${VAR1}${VAR2}${VAR1} round 5%)`;
      case 4: return `inset(${VAR1}${VAR1}${VAR1}${VAR1} round 5%)`;
      case 5: return `inset(${VAR2}${VAR1}${VAR0}${VAR1} round 5%)`;
      case 6: return `inset(${VAR2}${VAR0}${VAR0}${VAR2} round 5%)`;
      case 7: return `inset(${VAR1}${VAR0}${VAR1}${VAR2} round 5%)`;
      default: return `inset(${VAR1}${VAR0}${VAR1}${VAR2} round 5%)`;
    }
  };

  renderTiles() {
    return ['alpha', 'bravo', 'charlie', 'delta', 'echo', 'foxtrot'].map((radioCommand) => {
      const pos = this.positionForTile(radioCommand);
      const styles = {
        transition: `.1s cubic-bezier(0.86, 0, 0.07, 1)`,
        WebkitClipPath: this.clipPathForPosition(pos),
        clipPath: this.clipPathForPosition(pos)
      };

      return <Tile key={`rect-${radioCommand}`} styles={styles}/>;
    });
  }

  render() {
    const {size = 200, style} = this.props;
    const styles = Object.assign({
      width: `${DEF_SIZE}px`,
      height: `${DEF_SIZE}px`
    }, style);

    if (size) {
      styles.width = `${size}px`;
      styles.height = `${size}px`;
    }

    return (
      <StyledLoader styles={styles}>
        <Holder>
          {this.renderTiles()}
        </Holder>
      </StyledLoader>
    );
  }
}
