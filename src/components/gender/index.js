import React from 'react';
import cn from 'classnames';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';

import {formItem} from './../../decorators/formitem';
import {GirlIcon, ManIcon} from './../../icons';

export const Gender = styled(formItem(({value, className, style, onChange}) => (
  <div
    className={cn('gender', className)}
    onClick={() => onChange(value ? 0 : 1)}
    style={style}
  >
    {value ? <GirlIcon color="#aaa"/> : <ManIcon color="#aaa"/>}
  </div>
)))`
  user-select: none;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  height: 40px;
  min-height: 40px;
  cursor: pointer;

  &:before {
    content: 'пол';
    color: #666
  }
  
  ${(props) => toCSS(props.style)}
`;
