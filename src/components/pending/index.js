import React from 'react';
import {Spinner} from './..';

import './pending.less';

export const Pending = () => (
  <div className="pending">
    <Spinner className="pending--spinner"/>
  </div>
);
