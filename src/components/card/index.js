import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {ModifyColor} from './../../utils/color';
import {toCSS} from './../../utils/format';
import {Img} from './..';

const ImageHolder = styled.div`
  overflow: hidden;
  max-height: 150px;
  width: 100%;
  min-width: 200px;
`;

const StyledImg = styled(Img)`
  transition: transform .4s ease;
  height: auto;
  min-height: 150px;
  width: 100%;
  min-width: 200px;
`;

const StyledArticle = styled.article`
  display: flex;
  flex-direction: column;
  box-shadow: 0 0 20px -10px;
  flex: 1 1 23%;
  position: relative;
  ${(props) => toCSS(props.style)}
  margin: ${(props) => props.margin}px;
  transition: box-shadow .2s ease;
  
  @media only screen and (max-device-width: 991px) {
    flex: 1 1 30%;
    margin-right: 0;
    width: 100%;
  }

  @media only screen and (max-device-width: 440px) {
    flex: none;
    margin-right: 0;
    margin-left: 0;
    width: 100%;
  }
  
  &:hover {
    box-shadow: 0 1px 3px rgba(51,51,51,.08), 0 1px 10px rgba(51,51,51,.27);
  }
  
  &:hover ${StyledImg} {
    transform: scale3d(1.05, 1.05, 1);
  }
`;

const Percent = styled.div`
    display: inline-block;
    position: absolute;
    top: 20px;
    left: -10px;
    background: ${(props) => props.theme.greenColor};
    color: white;
    padding: 8px;
    font-weight: bold;
    height: 35px;
    box-shadow: 0 0 1px rgba(0,0,0,.5);

    &:before {
      content: '';
      position: absolute;
      top: 100%;
      left: 0;
      width: 0;
      height: 0;
      border-style: solid;
      border-width: 0 10px 5px 0;
      border-color: transparent ${(props) => ModifyColor(props.theme.greenColor, -30)} transparent transparent;
    }

    &:after {
      content: '';
      position: absolute;
      top: 0;
      left: 100%;
      width: 0;
      height: 0;
      border-style: solid;
      border-width: 35px 6px 0 0;
      border-color:  ${(props) => props.theme.greenColor} transparent transparent transparent;
    }
  }
`;

const ContentLink = styled(Link)`
    background: ${(props) => props.theme.whiteBackgroundColor};
    align-items: flex-start;
    text-decoration: none;
    color: ${(props) => props.theme.color};
    padding: 15px 20px;
    height: 100%;
    width: 100%;

    &:hover {
      text-decoration: none;
    }
`;

const ContentDiv = styled.div`
    background: ${(props) => props.theme.whiteBackgroundColor};
    align-items: flex-start;
    text-decoration: none;
    color: ${(props) => props.theme.color};
    padding: 15px 20px;
    height: 100%;
    width: 100%;

    &:hover {
      text-decoration: none;
    }
`;

export const Card = ({theme, margin, lazy, children, location, picture, alt = '', percent, childClassName, className, style}) => (
  <StyledArticle className={className} margin={margin} style={style}>
    {picture && (
      <Fragment>
        {location ? (
          <Link to={location}>
            <ImageHolder>
              <StyledImg
                lazy={!!lazy}
                src={picture}
                alt={alt}
              />
            </ImageHolder>
            {percent && (
              <Percent theme={theme}>{percent}</Percent>
            )}
          </Link>
        ) : (
          <div>
            <ImageHolder>
              <StyledImg
                lazy
                src={picture}
                alt={alt}
              />
            </ImageHolder>
            {percent && (
              <Percent>{percent}</Percent>
            )}
          </div>
        )}
      </Fragment>
    )}
    {children && (
      <Fragment>
        {location ? (
          <ContentLink theme={theme} className={childClassName} to={location}>
            {children}
          </ContentLink>
        ) : (
          <ContentDiv theme={theme} className={childClassName}>
            {children}
          </ContentDiv>
        )}
      </Fragment>
    )}
  </StyledArticle>
);
