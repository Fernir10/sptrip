import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './stars.less';

import {formItem} from './../../decorators/formitem';

export class Stars extends PureComponent {
  static defaultProps = {
    max: 5,
    size: 10,
    selected: 'red',
    defaultColor: '#ddd'
  };

  static propTypes = {
    max: PropTypes.number,
    size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    onChange: PropTypes.func,
    fav: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    selected: PropTypes.string,
    defaultColor: PropTypes.string
  };

  state = {
    hovered: 0
  };

  render() {
    const {max, size, color, fav, selected, defaultColor, onChange, style} = this.props;
    return (
      <div className="stars">
        {
          Array(max).fill(1).map((d, i) => (
            <div
              className="stars--star"
              key={`star-${i}`}
              style={{
                fontSize: size,
                lineHeight: `${size - (size / 8)}px`,
                color: color ? color : (fav >= i + 1 || this.state.hovered >= i + 1) ? selected : defaultColor,
                ...style
              }}
              onClick={onChange ? () => onChange(i) : () => {}}
              onMouseMove={() => (onChange ? this.setState({hovered: i + 1}) : {})}
              onMouseOut={() => (onChange ? this.setState({hovered: this.props.fav}) : {})}
            />
          ))
        }
      </div>
    );
  }
}


@formItem
export class StarsSelector extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    selected: PropTypes.string,
    defaultColor: PropTypes.string,
    className: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  };

  state = {
    value: this.props.value
  };

  onSelectStars = (val) => {
    this.setState({value: val + 1});
    this.props.onChange(val + 1);
  };

  render() {
    const {bordered, shadowed, className, style, placeholder, size, selected, defaultColor} = this.props;
    return (
      <div className={cn('stars--select', {'stars--select--bordered': bordered, 'stars--select--shadowed': shadowed}, className)} style={style}>
        <div className="stars--select--label">{placeholder}</div>
        <Stars
          onChange={this.onSelectStars}
          fav={this.state.value}
          size={size}
          selected={selected}
          defaultColor={defaultColor}
        />
      </div>
    );
  }
}
