import React from 'react';
import styled from 'styled-components';

import {ModifyColor} from './../../utils/color';

const Holder = styled.div`
  background: ${(props) => ModifyColor(props.theme.blueColor, 30) || '#def'};
  box-shadow: inset 0 0 0 1px ${(props) => ModifyColor(props.theme.blueColor, -30) || '#477ab9'};
  width: 100%;
  border-radius: 2px;
  overflow: hidden;
  height: 5px;
`;

const Thumb = styled.div`
    will-change: width;
    transition: width .3s ease;
    background: ${(props) => props.theme.blueColor || '#477ab9'};
    height: 7px;
    width: ${(props) => props.val}%;
`;

export const Progress = ({theme, value = 80, className}) => (
  <Holder theme={theme} className={className}>
    <Thumb theme={theme} val={value}/>
  </Holder>
);
