import React, {PureComponent} from 'react';
import styled from 'styled-components';

import {ModifyColor} from './../../utils/color';
import {formItem} from './../../decorators/formitem';
import {Spinner} from '../spinner';

const StyledButton = styled.button`
  user-select: none;
  text-align: center;
  cursor: pointer;
  border: none;
  color: ${(props) => ((props.color === 'white' || props.color === '#fff') ? props.theme.color : props.theme.whiteTextColor || '#fff')} !important;
  background: ${(props) => ((props.color === 'white' || props.color === '#fff') ? ModifyColor(props.theme.whiteBackgroundColor, 30) : props.theme.orangeColor) || props.color || '#d95107'} !important;
  box-shadow: 0 0 0 1px ${(props) => ((props.color === 'white' || props.color === '#fff') ? ModifyColor(props.theme.borderColor || props.color, 10) : (props.theme.orangeColor || props.color || '#d95107'))} !important;
  white-space: nowrap;
  -webkit-appearance: none;
  justify-content: center;
  transition: opacity .2s ease;
  text-decoration: none;

  &:hover {
    opacity: .8;
  }

  &:active {
    opacity: .6;
  }

  @media only screen and (max-device-width: 600px) {
    width: 100%;
    margin-left: 0 !important;
    margin-right: 0 !important;
  }
`;

@formItem
export class Button extends PureComponent {
  onClick = (e) => {
    const {onClick, isUpdating, isIdle = true} = this.props;

    e.preventDefault();

    if (isIdle && !isUpdating) {
      onClick();
    }
  };

  render() {
    const {theme, color, style, className, children, isUpdating, size} = this.props;

    return (
      <StyledButton
        theme={theme}
        color={color}
        style={style}
        className={className}
        onClick={this.onClick}
      >
        {isUpdating ? <Spinner size={size}/> : children}
      </StyledButton>
    );
  }
}
