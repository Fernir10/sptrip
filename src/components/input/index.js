import React, {Fragment, PureComponent} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import MaskedInput from './masked-input';
import {formItem} from './../../decorators/formitem';
import {tooltip} from './../../decorators/tooltip';
import {paranja} from './../../decorators/paranja';

import './input.less';

@formItem
@tooltip
@paranja
export class Input extends PureComponent {
  static propTypes = {
    disabled: PropTypes.string,
    placeholder: PropTypes.string,
    onFocusIn: PropTypes.func,
    onFocusOut: PropTypes.func,
    onChange: PropTypes.func,
    onError: PropTypes.func,
    startValue: PropTypes.string,
    value: PropTypes.string,
    setFocus: PropTypes.func,
    coordsRecieved: PropTypes.bool,
    type: PropTypes.string,
    name: PropTypes.string,
    id: PropTypes.string,
    pattern: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    bold: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    mask: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    style: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    autoComplete: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
  };

    static defaultProps = {
      placeholder: '',
      name: '',
      type: 'text',
      id: '',
      errorMessage: '',
      hintMessage: '',
      startValue: '',
      autoComplete: 'off',
      value: ''
    };

    constructor(props) {
      super(props);
      this.setPattern(props.pattern);
    }

  state = {
    focus: false
  };

  componentWillReceiveProps(nextProps) {
    this.setPattern(nextProps.pattern);
  }

  onFocusIn = () => {
    let val = this.props.value;
    if (this.props.startValue) {
      if (val.length <= this.props.startValue.length && val !== this.props.startValue) {
        val = this.props.startValue;
        this.props.onChange(val);
      }
    }

    if (this.props.onFocusIn) {
      this.props.onFocusIn();
    }

    this.setState({focus: true});
  };


  onFocusOut = () => {
    const val = this.props.value;

    if (this.props.startValue) {
      if (val === this.props.startValue) {
        this.props.onChange('');
      }
    }

    if (this.props.onFocusOut) {
      this.props.onFocusOut();
    }

    this.setState({focus: false});
  };

  onError(type) {
    if (this.props.onError) {
      this.props.onError(type);
    }
  }

  onChange = (e) => {
    const pos = e.target.selectionStart;
    const prevValue = e.target.value;
    const length = e.target.value.length;

    if (this.pattern && !this.props.mask) {
      e.target.value = e.target.value.replace(this.pattern, '');

      const applied = length !== e.target.value.length;
      if (applied) {
        this.onError('pattern');
      } else {
        this.onError('');
      }
    }

    if (!this.props.mask) {
      this.setCaretToPos(e.target, pos - (prevValue.length - e.target.value.length));
    }

    if (e.target.value !== this.props.value) {
      this.props.onChange(e.target.value);
    }
  };

  setCaretToPos(input, pos) {
    this.setSelectionRange(input, pos, pos);
  }

  setSelectionRange(input, selectionStart, selectionEnd) {
    let range;

    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
      range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', selectionEnd);
      range.moveStart('character', selectionStart);
      range.select();
    }
  }

  setPattern(pattern) {
    this.pattern = pattern ? new RegExp(`[^${pattern}]`, 'g') : false;
  }

  setFocusOnFirstError(wr) {
    const coordsRecieved = this.props.coordsRecieved;
    const setFocus = this.props.setFocus;
    if (wr) {
      if (setFocus) {
        wr.focus();
      }

      if (coordsRecieved) {
        coordsRecieved(() => wr.getBoundingClientRect());
      }
    }
  }

  render() {
    const {
      mask,
      type,
      id,
      name,
      value,
      autoComplete,
      placeholder,
      disabled,
      style,
      bold,
      capitalize,
      className,
      children
    } = this.props;

    return (
      <Fragment>
        {children}
        {mask ?
          <MaskedInput
            ref={(wr) => this.setFocusOnFirstError(wr)}
            {...((this.state.focus && type === 'date') && {'data-date-inline-picker': true})}
            mask={mask ? mask.replace(/9/gi, '1') : '*'}
            className={cn('input', {'input--bold': bold, 'input--capitalize': capitalize}, className)}
            style={style}
            type={type}
            placeholder={placeholder}
            name={name}
            value={value}
            autoComplete={autoComplete}
            onFocus={this.onFocusIn}
            onBlur={this.onFocusOut}
            disabled={disabled}
            onChange={this.onChange}
          /> :
          <input
            ref={(wr) => this.setFocusOnFirstError(wr)}
            {...((this.state.focus && type === 'date') && {'data-date-inline-picker': true})}
            className={cn('input', {'input--bold': bold, 'input--capitalize': capitalize}, className)}
            style={style}
            type={type}
            placeholder={placeholder}
            name={name}
            value={value}
            autoComplete={autoComplete}
            onFocus={this.onFocusIn}
            onBlur={this.onFocusOut}
            onChange={this.onChange}
            disabled={disabled}
          />
        }
      </Fragment>
    );
  }
}
