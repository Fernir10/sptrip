import React, {Fragment, PureComponent} from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './paginator.less';

export class Paginator extends PureComponent {
  static defaultProps = {
    max: 0
  };

  static propTypes = {
    max: PropTypes.number,
    page: PropTypes.number,
    setPage: PropTypes.func
  };

  changePage = (index) => {
    if (this.props.page !== index) {
      this.props.onChange(index);
    }
  };

  render() {
    return (
      <Fragment>
        {this.props.max > 1 ? <div className={cn('flex-row', this.props.className)} style={this.props.style}>
          {
            Array(this.props.max).fill(1).map((d, i) =>
              (<div
                key={i}
                className={cn('pgn--i', {'pgn--i--s': Number(this.props.page) === i})}
                onClick={() => this.changePage(i)}
              >
                {i + 1}
              </div>))
          }
        </div> : null}
      </Fragment>
    );
  }
}
