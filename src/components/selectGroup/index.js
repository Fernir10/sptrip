import React, {Fragment, PureComponent} from 'react';
import PropType from 'prop-types';
import cn from 'classnames';
import styled from 'styled-components';

import {CheckIcon, SearchIcon} from './../../icons';

import './selectGroup.less';

const statuses = {
  searching: <SearchIcon animated size={20}/>,
  finished: <CheckIcon color="green"/>
};

const StyledSelectedItem = styled.span`
  max-width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  padding: 5px;
  line-height: 1;
  border-radius: 2px;
  flex: 1;
  background: ${(props) => (props.selected ? props.theme.darkColor : 'inherit')};
  color: ${(props) => (props.selected ? props.theme.whiteTextColor : 'inherit')};
`;

const StyledInput = styled.input`
    padding: 5px 10px !important;
    font-size: 12px !important;
    box-shadow: 0 0 0 1px ${(props) => props.theme.borderColor};
    background: ${(props) => props.theme.darkColor};
    color: ${(props) => props.theme.whiteTextColor};
    border: none;
`;

const StyledContainer = styled.div`
    box-shadow: 0 0 0 1px ${(props) => props.theme.borderColor};
    background: ${(props) => props.theme.whiteBackgroundColor};
    color: ${(props) => props.theme.color};
`;

class SelectGroupList extends PureComponent {
  static propTypes = {
    value: PropType.oneOfType([PropType.string, PropType.number]),
    options: PropType.oneOfType([PropType.array, PropType.object]),
    onChange: PropType.func,
    max: PropType.number,
    searchWord: PropType.string,
    showSelected: PropType.bool
  };

  setValue = (itm) => {
    const tmpArr = this.props.value && this.props.value.split ? this.props.value.split(',') : [];
    if (tmpArr.indexOf(`${itm.id || itm.code}`) !== -1) {
      tmpArr.splice(tmpArr.indexOf(`${itm.id || itm.code}`), 1);
    } else {
      tmpArr.push(itm.id || itm.code);
    }

    const retval = tmpArr.join(',') === '' ? null : tmpArr.join(',');
    this.props.onChange(retval);
  };

  render() {
    const tmpArr = this.props.value && this.props.value.split ? this.props.value.split(',') : [];
    const re = new RegExp(this.props.searchWord, 'gi');
    const outputArr = this.props.options && this.props.searchWord.length ? this.props.options.filter((itm) => itm.name.match(re)) : this.props.options || [];
    const output = this.props.showSelected ? outputArr.filter((itm) => (tmpArr.indexOf(`${itm.id || itm.code}`) !== -1)) : outputArr.slice(0, this.props.max);

    return output.map((itm) => (
      <label key={`${itm.id || itm.code}-${itm.name}`} className="selectgroup--item" onClick={() => this.setValue(itm)}>
        <StyledSelectedItem
          theme={this.props.theme}
          selected={tmpArr.indexOf(`${itm.id || itm.code}`) !== -1}
        >
          {itm.name}
        </StyledSelectedItem>
        {itm.rating && (
          <span className="selectgroup--item--text-small">{itm.rating}</span>
        )}
        {itm.status && (
          <span className="selectgroup--item--text-small">{statuses[itm.status]}</span>
        )}
      </label>
    ));
  }
}

export class SelectGroup extends PureComponent {
  static defaultProps = {
    value: '',
    options: []
  };

  static propTypes = {
    value: PropType.oneOfType([PropType.number, PropType.string]),
    className: PropType.oneOfType([PropType.object, PropType.string]),
    label: PropType.string,
    label2: PropType.string,
    options: PropType.oneOfType([PropType.array, PropType.object]),
    search: PropType.bool
  };

  state = {
    itemsMaxCount: 20,
    showSelected: false,
    searchWord: ''
  };

  onSearch = (e) => {
    this.setState({searchWord: e.target.value});
  };

  setMaxItems = () => {
    if (this.list) {
      if (this.list.scrollTop + this.list.offsetHeight >= this.list.scrollHeight - 30) {
        this.setState({itemsMaxCount: this.state.itemsMaxCount + 20});
      }
    }
  };

  toggleSelected = () => {
    this.setState({
      showSelected: !this.state.showSelected
    });
  };

  render() {
    const tmpArr = this.props.value && this.props.value.split ? this.props.value.split(',') : [];
    const count = this.props.options.filter((itm) => (tmpArr.indexOf(`${itm.id || itm.code}`) !== -1)).length || 0;

    return (
      <StyledContainer theme={this.props.theme} className={cn('selectgroup__container', this.props.className, {'selectgroup__container--search': this.props.search})}>
        <span className="selectgroup__label">{this.props.label}</span>
        {this.props.search && (
          <Fragment>
            <div className="selectgroup--input">
              <span className="selectgroup--input--icon">⌕</span>
              <StyledInput theme={this.props.theme} type="text" onChange={this.onSearch} placeholder="Введите любые буквы поиска" value={this.state.searchWord}/>
            </div>
          </Fragment>
        )}
        {this.props.selected && (
          <label className="selectgroup--item" onClick={this.toggleSelected}>
            <StyledSelectedItem
              theme={this.props.theme}
              selected={this.state.showSelected}
            >
              Показать отмеченные{count ? ` (${count})` : ''}
            </StyledSelectedItem>
          </label>
        )}
        {this.props.label2 && (
          <div className="selectgroup__label selectgroup__label--small flex-end">
            {this.props.label2 && (
              <span>{this.props.label2}</span>
            )}
          </div>
        )}
        <div ref={(node) => { this.list = node; }} className="selectgroup" onScroll={this.setMaxItems}>
          {this.props.options ? (
            <SelectGroupList showSelected={this.state.showSelected} searchWord={this.state.searchWord} max={this.state.itemsMaxCount} {...this.props}/>
          ) : (
            <div className="selectgroup--item"><span className="selectgroup--item--text">загрузка&hellip;</span></div>
          )}
        </div>
      </StyledContainer>
    );
  }
}
