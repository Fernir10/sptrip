import React, {Fragment, PureComponent} from 'react';
import styled from 'styled-components';
import {formItem} from './../../decorators/formitem';

const Input = styled.div`
  position: relative;
`;

const InputFile = styled.input`
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    width: 100%;
    z-index: 1;
`;

const InputText = styled.input`
    position: relative;
`;

@formItem
export class FileInput extends PureComponent {
  state = {
    value: ''
  };

  handleChange = (e) => {
    const {onChange} = this.props;
    this.setState({value: e.target.value.split(/(\\|\/)/g).pop()});
    if (onChange) {
      onChange(e);
    }
  };

  render() {
    const {className, placeholder = 'Выберите файл'} = this.props;

    return (
      <Fragment>
        <Input/>
        <InputFile
          type="file"
          className={className}
          onChange={this.handleChange}
        />
        <InputText
          type="text"
          tabIndex={-1}
          value={this.state.value}
          className={className}
          placeholder={placeholder}
        />
      </Fragment>
    );
  }
}
