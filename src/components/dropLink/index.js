import React from 'react';
import cn from 'classnames';

import {Arrow} from './..';

import './dropLink.less';

export const DropLink = ({theme, onClick = () => {}, className, style, children, open}) => (
  <div className={cn('droplink', {'droplink--opened': open}, className)} style={style} onClick={onClick}>
    <span>{children}</span>
    <Arrow isOpened={open} color={theme.color} size={8} className="droplink--arrow"/>
  </div>
);
