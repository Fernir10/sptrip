import React from 'react';
import styled from 'styled-components';

import {toCSS} from './../../utils/format';
import {formItem} from './../../decorators/formitem';

import {CloseIcon} from './..';

const StyledClose = styled(CloseIcon)`
  cursor: pointer;
  margin-left: 10px;
  opacity: .5;
  transition: opacity .2s ease;
  
  &:hover {
    opacity: 1;
  }
`;

export const Removable = styled(formItem(({theme, className, onClose, children}) => (
  <div className={className}>
    {children}
    <StyledClose onClick={onClose} color={theme.color} size={8}/>
  </div>
)))`
  display: flex;
  align-items: center;
  flex: 1 1 auto;
  justify-content: space-between;
  white-space: nowrap;
  ${(props) => toCSS(props.style)}
`;
