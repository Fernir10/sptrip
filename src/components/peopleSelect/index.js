import React, {PureComponent} from 'react';
import cn from 'classnames';

import {formItem} from './../../decorators/formitem';
import {paranja} from './../../decorators/paranja';
import {Arrow, Button, Dropdown, Removable, Selector} from './..';

import './peopleSelect.less';

@formItem
@paranja
export class PeopleSelect extends PureComponent {
  state = {
    isOpened: false,
    childrens: []
  };

  childOption = [];
  adultsOption = [];
  adults = null;
  child = null;
  childage1 = null;
  childage2 = null;
  childage3 = null;

  constructor(props) {
    super(props);
    const {adults = 2, child, childage1, childage2, childage3} = props;
    this.adults = adults;
    this.child = child;
    this.childage1 = childage1;
    this.childage2 = childage2;
    this.childage3 = childage3;

    if (child) {
      if (childage1) this.state.childrens.push(childage1);
      if (childage2) this.state.childrens.push(childage2);
      if (childage3) this.state.childrens.push(childage3);
    }

    for (let i = 1; i < 15; i++) {
      this.childOption.push({title: `До ${i} ${i > 1 ? 'лет' : 'года'}`, value: i});
    }

    for (let i = 1; i < 5; i++) {
      this.adultsOption.push({title: `${i} чел.`, value: i});
    }
  }

  onOpen = () => {
    this.setState({isOpened: !this.state.isOpened});
  };

  removeChildren = (val) => {
    this.state.childrens.splice(this.state.childrens.indexOf(val), 1);
    this.child = this.state.childrens.length;
    this.childage1 = this.state.childrens[0];
    this.childage2 = this.state.childrens[1];
    this.childage3 = this.state.childrens[2];
    this.onChange();
  };

  selectChildren = (val) => {
    if (val > 0) {
      this.state.childrens.push(val);
    }
    this.child = this.state.childrens.length;
    this.childage1 = this.state.childrens[0];
    this.childage2 = this.state.childrens[1];
    this.childage3 = this.state.childrens[2];
    this.onChange();
  };

  onChange = () => {
    this.props.onSelectPeoples({
      adults: this.adults,
      child: this.child,
      childage1: this.childage1,
      childage2: this.childage2,
      childage3: this.childage3
    });
  };

  render() {
    const {theme, placeholder, className} = this.props;

    return (
      <div className={cn('ppls', {'ppls--focused': this.state.isOpened}, className)} style={this.props.style}>
        <label className="formitem__label">{placeholder}</label>
        <div className="ppls--text" onClick={this.onOpen}>
          <div className="ppls--text--text">
            {this.props.child ? `${this.props.adults} взр. ${this.props.child} реб.` : `${this.props.adults} взрослы${this.props.adults === 1 ? 'й' : 'х'}`}
          </div>
          <Arrow color={theme.color} isOpened={this.state.isOpened} size={12}/>
        </div>
        <Dropdown
          theme={this.props.theme}
          isOpened={this.state.isOpened}
          height="fit-content"
          className="ppls--c"
        >
          <p><b>Взрослые</b></p>
          <Selector
            theme={theme}
            className="bottom10"
            onChange={(val) => {
              this.adults = val;
              this.onChange();
            }}
            value={`${this.adults} чел.`}
            options={this.adultsOption}
          />
          <p><b>Дети</b></p>
          {
            this.state.childrens.map((item) =>
              (<Removable
                theme={theme}
                key={item}
                className="bottom10"
                onClose={() => this.removeChildren(item)}
              >
                {`До ${item} ${item > 1 ? 'лет' : 'года'}`}
              </Removable>))
          }
          <Selector
            theme={theme}
            disabled={this.state.childrens.length >= 5}
            className="bottom10"
            onChange={this.selectChildren}
            value={'Добавить'}
            options={this.childOption}
            expand
          />
          <Button theme={theme} className="ppls--ch" onClick={() => this.setState({isOpened: false})}>Выбрать</Button>
        </Dropdown>
      </div>
    );
  }
}
