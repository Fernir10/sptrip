import React, {Component} from 'react';

import {Tabs} from './../../components';
import {Edit} from './editComponent';
import {Menu} from './../../modules';

export class IndexPage extends Component {
  getSelected = () => {
    const url = this.props.match.url;

    if (url.match(/country/)) {
      return 0;
    } else if (url.match(/region/)) {
      return 1;
    } else if (url.match(/hotel/)) {
      return 2;
    }
    return 0;
  };

  render() {
    const country = this.props.match.params.country || 4;
    const region = this.props.match.params.region || 21;
    const hotel = this.props.match.params.hotel || 52873;
    const {history} = this.props;

    return (
      <div style={{marginBottom: '-100px'}}>
        <Menu
          menu={[
            {
              title: 'Документы',
              href: '/admin/'
            },
            {
              title: 'Пользователи',
              href: '/users'
            },
            {
              title: 'Редактировать',
              href: '/edit'
            },
            {
              title: 'Выход',
              aref: '/logout'
            }
          ]}
          color="#444"
        />
        <Tabs
          theme={this.props.theme}
          style={{minHeight: '100vh'}}
          selected={this.getSelected()}
          tabs={
            [
              {
                title: 'Страны',
                onClick: () => { history.push(`/admin/country/${country}/${region}/${hotel}`); },
                content: <Edit type="country" datatype="countries" {...this.props}/>
              },
              {
                title: 'Курорты',
                onClick: () => { history.push(`/admin/region/${country}/${region}/${hotel}`); },
                content: <Edit type="region" datatype="regions" {...this.props}/>
              },
              {
                title: 'Отели',
                onClick: () => { history.push(`/admin/hotel/${country}/${region}/${hotel}`); },
                content: <Edit type="hotel" datatype="hotels" {...this.props}/>
              }
            ]
          }
        />
      </div>
    );
  }
}
