import React, {Component} from 'react';

import {Button, Docker, Input} from './../../components';
import superAgent from './../../utils/superAgent';
import NotificationSystem from 'react-notification-system';
import {Menu} from './../../modules';

export class LoginPage extends Component {
  state = {
    login: '',
    password: '',
    error: ''
  };

  update = () => superAgent.login({
    email: this.state.login,
    password: this.state.password
  }).then((data) => {
    if (data.error) {
      this.setState({error: data.error});
    } else {
      this.setState({error: ''});
      location.assign('/admin/');
    }
  });

  render() {
    return (
      <div className="container-xs">
        <Menu color="#444"/>
        <Docker columns>
          <h2 className="text-center">Авторизация</h2>
          <label>Логин</label>
          <Input
            type="text"
            errorMessage={this.state.error === 'username' ? 'Неверный логин' : ''}
            isShowTooltip={this.state.error === 'username'}
            placeholder="username"
            onChange={(val) => this.setState({login: val})}
            value={this.state.login}
          />
          <label>Пароль</label>
          <Input
            type="password"
            errorMessage={this.state.error === 'password' ? 'Неверный пароль' : ''}
            isShowTooltip={this.state.error === 'password'}
            placeholder="password"
            className="bottom20"
            onChange={(val) => this.setState({password: val})}
            value={this.state.password}
          />
          <Button type="submit" onClick={this.update}>Авторизоваться</Button>
          <p className="text-center">
            <a href="/register">Зарегистрироваться</a>
            <br/>
            <a href="/restore">Забыли пароль?</a>
          </p>
        </Docker>
        <NotificationSystem ref={(node) => { this.notify = node; }}/>
      </div>
    );
  }
}

