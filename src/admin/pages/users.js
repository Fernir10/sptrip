import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import NotificationSystem from 'react-notification-system';

import {CloseIcon, Docker} from './../../components';
import superAgent from './../../utils/superAgent';
import {Menu} from './../../modules';

@observer
export class UsersPage extends Component {
  @observable.shallow users = this.props.store.users || [];

  render() {
    return (
      <div className="container-md">
        <Menu
          menu={[
            {
              title: 'Документы',
              href: '/admin/'
            },
            {
              title: 'Пользователи',
              href: '/users'
            },
            {
              title: 'Редактировать',
              href: '/edit'
            },
            {
              title: 'Выход',
              aref: '/logout'
            }
          ]}
          color="#444"
        />
        <h3>Управление пользователями</h3>
        <Docker columns>
          {this.users && (
            <table className="userstable bottom20">
              <tbody>
                {
                  this.users.sort((a, b) => a.access < b.access).map((user) => (
                    <tr key={user.username}>
                      <td>{user.username}</td>
                      <td><a href={`mailto:${user.email}`}>{user.email}</a></td>
                      <td>
                        <select
                          defaultValue={user.access} onChange={(e) => {
                            superAgent.access({
                              email: user.username,
                              access: ['full', ''][e.target.selectedIndex]
                            }).then((data) => {
                              if (data.error) {
                                this.notify.addNotification({
                                  message: `Не получилось изменить права пользователю ${user.username}`,
                                  level: 'error',
                                  dismissible: 'click',
                                  autoDismiss: 1,
                                  position: 'bc'
                                });
                              } else {
                                this.notify.addNotification({
                                  message: `Права для ${user.username} успешно изменены`,
                                  level: 'success',
                                  dismissible: 'click',
                                  autoDismiss: 1,
                                  position: 'bc',
                                  onRemove: () => location.reload()
                                });
                              }
                            });
                          }}
                        >
                          <option value="full">полный доступ</option>
                          <option value="">без доступа</option>
                        </select>
                      </td>
                      <td><CloseIcon
                        className="pointer" onClick={() => {
                          if (confirm(`Вы действительно хотите удалить пользователя ${user.username}?`)) {
                            superAgent.deleteUser({
                              email: user.username
                            }).then((data) => {
                              if (data.error) {
                                this.notify.addNotification({
                                  message: 'Не получилось удалить пользователя',
                                  level: 'error',
                                  dismissible: 'click',
                                  position: 'bc'
                                });
                              } else {
                                this.users = this.users.filter((u) => u.username !== user.username);

                                this.notify.addNotification({
                                  message: `Пользователь ${user.username} успешно удален`,
                                  level: 'success',
                                  dismissible: 'click',
                                  position: 'bc'
                                });
                              }
                            });
                          }
                        }}
                      /></td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          )}
          <NotificationSystem ref={(node) => { this.notify = node; }}/>
        </Docker>
      </div>
    );
  }
}

