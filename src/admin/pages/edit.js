import React, {Component} from 'react';

import {Button, Docker, Input} from './../../components';
import superAgent from './../../utils/superAgent';
import NotificationSystem from 'react-notification-system';
import {Menu} from './../../modules';

export class Edit extends Component {
  state = {
    password: '',
    email: this.props.store.email || ''
  };

  update = () => superAgent.changePassword({
    email: this.props.store.user,
    emailaddr: this.state.email,
    password: this.state.password
  }).then((data) => {
    if (data.error) {
      this.notify.addNotification({
        message: 'Не получилось сохранить',
        level: 'error',
        dismissible: 'click',
        position: 'bc'
      });
    } else {
      this.notify.addNotification({
        message: 'Успешно сохранено',
        level: 'success',
        dismissible: 'click',
        position: 'bc',
        onRemove: () => location.reload()
      });
    }
  });

  render() {
    return (
      <div className="container-md">
        <Menu
          menu={[
            {
              title: 'Документы',
              href: '/admin/'
            },
            {
              title: 'Пользователи',
              href: '/users'
            },
            {
              title: 'Редактировать',
              href: '/edit'
            },
            {
              title: 'Выход',
              aref: '/logout'
            }
          ]}
          color="#444"
        />
        <div className="container-xs">
          <h3>Настройки</h3>
          <Docker columns>
            <label>Почта</label>
            <Input type="text" name="email" placeholder="Email" onChange={(val) => this.setState({email: val})} value={this.state.email}/>
            <label>Пароль</label>
            <Input className="bottom20" type="password" placeholder="password" onChange={(val) => this.setState({password: val})} value={this.state.password}/>
            <Button onClick={this.update}>Сохранить</Button>
          </Docker>
        </div>
        <NotificationSystem ref={(node) => { this.notify = node; }}/>
      </div>
    );
  }
}

