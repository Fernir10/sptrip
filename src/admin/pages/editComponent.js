import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import NotificationSystem from 'react-notification-system';
import styled from 'styled-components';

import {Button, Docker, FileInput, Img, Input, RTF, Select} from '../../components';

import superAgent from './../../utils/superAgent';

const AdminImg = styled(Img)`
  object-fit: cover;
  max-height: 200px;
`;

@observer
export class Edit extends Component {
  text = '';
  @observable regionList = null;
  @observable countryList = {};
  @observable options = [];
  @observable title = '';
  @observable keywords = '';
  @observable country = 4;
  @observable region = 19;
  @observable hotel = 52873;
  @observable file = '';
  @observable image = null;

  componentWillMount() {
    this.componentWillReceiveProps(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.country = newProps.match.params.country || 4;
    this.region = newProps.match.params.region || 21;
    this.hotel = newProps.match.params.hotel || 52873;

    if (newProps.store.country) {
      newProps.store.country.forEach((c) => {
        this.countryList[c.id] = c.name;
      });
    }

    if (newProps.store.region) {
      this.regionList = newProps.store.region;
    }

    this.update(newProps).then(() => {
      this.load(newProps.match.params[newProps.type] || 4, newProps);
    });
  }

  update = (props) => superAgent.form({
    type: 'country,region,hotel',
    departure: 1,
    cndep: 1,
    country: this.country,
    hotcountry: this.country
  }).then((data) => {
    if (data.countries) {
      data.countries.forEach((c) => {
        this.countryList[c.id] = c.name;
      });
    }

    if (data.regions) {
      this.regionList = data.regions;
    }

    if (data[props.datatype]) {
      this.options = data[props.datatype].map((itm) => ({title: itm.name, value: itm.id}));
    }
  });

  save = () => {
    const id = this.props.match.params[this.props.type] || 4;
    const formData = new FormData();

    if (this.file) {
      formData.append('img', this.file);
    }

    formData.append('type', this.props.type);
    formData.append('id', id);
    formData.append('title', this.title);
    formData.append('keywords', this.keywords);
    formData.append('text', this.text);
    formData.append('country', this.country);
    formData.append('countryName', this.countryList[this.country]);

    if (id) {
      superAgent.record(formData).then((data) => {
        if (data.status && data.status === 'success') {
          this.notify.addNotification({
            message: 'Успешно сохранено',
            level: 'success',
            autoDismiss: 1,
            dismissible: 'click',
            position: 'tc'
          });
        } else {
          this.notify.addNotification({
            message: 'Не получилось сохранить',
            level: 'error',
            autoDismiss: 1,
            dismissible: 'click',
            position: 'tc'
          });
        }
      });
    }
  };

  load = (val, props) => {
    this.text = '';

    if (val || props ? props.match.params[props.type] || 4 : this.props.match.params[this.props.type] || 4) {
      superAgent.getrecord({
        type: props ? props.type : this.props.type,
        id: val || props ? props.match.params[props.type] || 4 : this.props.match.params[this.props.type] || 4
      }).then((data) => {
        this.text = data.text || '';
        this.title = data.title || '';
        this.keywords = data.keywords || '';
        this.image = data.img || null;
      });
    }
  };

  onChange = (value) => {
    this.text = value;
  };

  render() {
    return (
      <Docker columns>
        <Docker className="bottom20">
          <Select
            options={this.options}
            onChange={(val) => {
              this.props.history.push(`/admin/${this.props.type}/${this.props.type === 'country' ? val : this.country}/${this.props.type === 'region' ? val : this.region}/${this.props.type === 'hotel' ? val : this.hotel}/`);
              this.load(val, this.props);
            }}
            value={this.props.type === 'country' ? this.props.match.params[this.props.type] || 4 : this.props.match.params[this.props.type]}
            bordered
          />
          <Input bold placeholder="заголовок" value={this.title} onChange={(val) => { this.title = val; }}/>
          <Input bold placeholder="keywords" value={this.keywords} onChange={(val) => { this.keywords = val; }}/>
          <FileInput
            value={this.file}
            onChange={(e) => {
            if (e.target.files[0]) {
              this.file = e.target.files[0];
            }
          }}/>
          <Button onClick={this.save}>Сохранить</Button>
        </Docker>
        {this.image && (
          <AdminImg src={this.image}/>
        )}
        <RTF
          style={{
            maxHeight: '70vh'
          }}
          value={this.text}
          onChange={this.onChange}
        />
        <NotificationSystem ref={(node) => { this.notify = node; }}/>
      </Docker>
    );
  }
}

