export {IndexPage} from './indexPage';
export {LoginPage} from './login';
export {LogoutPage} from './logout';
export {RegisterPage} from './register';
export {Edit} from './edit';
export {RestorePage} from './restore';
export {UsersPage} from './users';
