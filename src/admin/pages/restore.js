import React, {Component} from 'react';

import {Button, Docker, Input} from './../../components';
import superAgent from './../../utils/superAgent';
import {Menu} from './../../modules';


export class RestorePage extends Component {
  state = {
    username: '',
    error: null
  };

  update = () => superAgent.restore({
    email: this.state.username
  }).then((data) => {
    this.setState({error: null});
    if (data.error) {
      this.setState({error: data.error});
    } else {
      location.assign('/');
    }
  });

  render() {
    return (
      <div className="container-xs">
        <Menu color="#444"/>
        <h4>Пароль будет выслан на почту, указанную при регистрации</h4>
        {this.state.error && (
          <h5 style={{color: 'red'}}>{this.state.error}</h5>
        )}
        <Docker columns>
          <label>Логин</label>
          <Input className="bottom20" type="text" placeholder="username" onChange={(val) => this.setState({username: val})} value={this.state.username}/>
          <Docker>
            <Button onClick={this.update}>Выслать</Button>
            <Button color="#fff" onClick={() => this.props.history.goBack()}>Отмена</Button>
          </Docker>
        </Docker>
      </div>
    );
  }
}

