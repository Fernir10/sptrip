import React, {Component} from 'react';

import superAgent from './../../utils/superAgent';
import {Button, Docker, Input} from './../../components';
import NotificationSystem from 'react-notification-system';
import {Menu} from './../../modules';

export class RegisterPage extends Component {
  state = {
    login: '',
    password: '',
    emailaddr: '',
    error: null
  };

  update = () => superAgent.register({
    email: this.state.login,
    password: this.state.password,
    emailaddr: this.state.emailaddr
  }).then((data) => {
    this.setState({ error: null });
    if (data.error){
      this.notify.addNotification({
        message: data.error,
        level: 'error',
        dismissible: 'click',
        position: 'tc',
        action: {
          label: 'Авторизоваться',
          callback: () => location.assign('/login')
        }
      });
    } else {
      this.props.history.push('/');
    }
  });

  render() {
    return (
      <div className="container container-xs">
        <Menu color="#444"/>
        <Docker columns>
          <h3 className="text-center">Регистрация</h3>
          <label>Почта</label>
          <Input type="text" name="emailaddr" placeholder="Email" onChange={(val) => this.setState({emailaddr: val})} value={this.state.emailaddr} autoComplete={'off'}/>
          <label>Логин</label>
          <Input type="text" name="email" placeholder="username" onChange={(val) => this.setState({login: val})} value={this.state.login} autoComplete={'off'}/>
          <label>Пароль</label>
          <Input className="bottom20" type="password" name="password" placeholder="password" onChange={(val) => this.setState({password: val})} value={this.state.password} autoComplete={'off'}/>
          <Docker>
            <Button type="submit" onClick={this.update}>Зарегистрироваться</Button>
            <Button color="#fff" onClick={() => this.props.history.goBack()}>Отмена</Button>
          </Docker>
        </Docker>
        <NotificationSystem ref={(node) => { this.notify = node; }}/>
      </div>
    );
  }
}

