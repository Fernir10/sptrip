import React, {Fragment, PureComponent} from 'react';
import {Helmet} from 'react-helmet';
import {extras} from 'mobx';
import {Route, Switch} from 'react-router';
import {withRouter} from 'react-router-dom';

import {Edit, IndexPage, LoginPage, LogoutPage, RegisterPage, RestorePage, UsersPage} from './../admin/pages/index';

import {Img} from './../components';

import {ErrorIcon} from './../icons';

import './admin.less';

// extras.isolateGlobalState();

const Notfound = (props) => (
  <div className="container error--message">
    <Helmet>
      <title>Страница не найдена!</title>
      <meta name="description" content={`Страница ${props.location.pathname} не найдена!`}/>
    </Helmet>
    <h2>Страница {props.location.pathname} не найдена!</h2>
    <div className="flex">
      <ErrorIcon style={{marginRight: 10}}/>
      <span>Возможно она была удалена или перенесена</span>
      <Img src="/img/404-girl.png" style={{position: 'absolute', bottom: 0}}/>
    </div>
  </div>
);


@withRouter
export default class Admin extends PureComponent {
  render() {
    const {pathname} = this.props.location;

    return (
      <Fragment>
        <Helmet
          titleTemplate="Панель управления sptrip.ru | %s"
          defaultTitle="Панель управления"
        >
          <title>Панель управления</title>
          <meta charSet="utf-8"/>
          <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=0"/>
          <link rel="canonical" href={`https://sptrip.ru:3000${pathname}`}/>
          <meta name="robots" content="noindex,nofollow"/>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
          <link rel="icon" href="/img/cropped-sptrip_f-80x80.png" sizes="80x80"/>
          <link rel="apple-touch-icon-precomposed" href="/img/cropped-sptrip_f-180x180.png"/>
          <meta name="msapplication-TileImage" content="/img/cropped-sptrip_f-270x270.png"/>
          <link rel="manifest" href="/manifest.json"/>
          <meta name="theme-color" content="#477ab9"/>
          <meta name="application-name" content="Панель управления sptrip.ru"/>
          <meta property="og:type" content="website"/>
          <meta property="og:url" content={`https://sptrip.ru:3000${pathname}`}/>
          <meta property="og:image" content="https://sptrip.ru/img/cropped-sptrip_f-192x192.png"/>
          <meta property="og:site_name" content="sptrip.ru"/>
        </Helmet>
        <Switch>
          <Route exact path="/admin/" render={(props) => (<IndexPage {...props} store={this.props.store}/>)}/>
          <Route exact path="/admin/country/:country?/:region?/:hotel?/" render={(props) => (<IndexPage {...props} store={this.props.store}/>)}/>
          <Route exact path="/admin/region/:country?/:region?/:hotel?/" render={(props) => (<IndexPage {...props} store={this.props.store}/>)}/>
          <Route exact path="/admin/hotel/:country?/:region?/:hotel?/" render={(props) => (<IndexPage {...props} store={this.props.store}/>)}/>
          <Route exact path="/edit/" render={(props) => (<Edit {...props} store={this.props.store}/>)}/>
          <Route exact path="/login/" component={LoginPage}/>
          <Route exact path="/logout/" component={LogoutPage}/>
          <Route exact path="/register/" component={RegisterPage}/>
          <Route exact path="/restore/" component={RestorePage}/>
          <Route exact path="/users/" render={(props) => (<UsersPage {...props} store={this.props.store}/>)}/>
          <Route exact path="/" render={() => {
            location.assign('/');
            return null;
          }}/>
          <Route component={Notfound}/>
        </Switch>
      </Fragment>
    );
  }
}
