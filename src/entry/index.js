import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';

import Site from './../site';

const store = window.__store;

ReactDOM.hydrate(
  <BrowserRouter>
    <Site
      store={store}
      search={store ? store.search : {}}
      tours={store ? store.tours : {}}
      hotel={store ? store.hotel : {}}
      hot={store ? store.hot : {}}
    />
  </BrowserRouter>
  , document.querySelector('.site-wrapper'));
