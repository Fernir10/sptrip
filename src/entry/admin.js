import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import Admin from './../admin';


const store = window.__store;
ReactDOM.hydrate(
  <BrowserRouter>
    <Admin store={store}/>
  </BrowserRouter>
  , document.querySelector('.site-wrapper'));
