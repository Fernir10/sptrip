import React from 'react';
import {Helmet} from 'react-helmet';
import {IFrame} from './../components';

export const Insurance = () => (
  <div className="container">
    <Helmet>
      <title>Страхование путешественников</title>
      <meta name="description" content="Страхование путешественников"/>
    </Helmet>

    <br/><br/>
    <IFrame src="//c53.travelpayouts.com/content?promo_id=1753&shmarker=91717&width=auto&hide_logo=true&backgroundColor=faf9f0&borderColor=faf9f0&iconsColor=000000&titleColor=000000&labelsColor=000000&btnSearchColor=ff0000&btnResultColor=ff0000"/>
  </div>
);
