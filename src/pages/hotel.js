import React from 'react';

import {HotelFull} from './../modules/search/hotel/hotel';

export const HotelPage = (props) => (
  <div className="container bottom20">
    <HotelFull {...props}/>
  </div>
);
