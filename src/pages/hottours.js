import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';

import {ModifyColor} from './../utils/color';
import {Breadcrumb} from './../components';

import {HotTours} from './../modules/hot';

export const HotToursPage = (props) => (
  <Fragment>
    <div className="container top20">
      <Breadcrumb bg={props.theme.blueColor} hover={ModifyColor(props.theme.blueColor, 10)} text={props.theme.whiteTextColor}>
        <Link to="/">На главную</Link>
        <span>Горящие туры</span>
      </Breadcrumb>
    </div>
    <HotTours {...props}/>
  </Fragment>
);
