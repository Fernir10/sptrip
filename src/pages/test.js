import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import {Docker} from './../components';

import './indexPage.less';

export class NewSearchPage extends Component {
  render() {
    return (
      <div className="container">
        <Docker>
          <Link className="index-service index-service--avia" to="/avia">Авиаперелеты</Link>
          <Link className="index-service index-service--hotels" to="/hotels">Отели</Link>
          <Link className="index-service index-service--insurance" to="/insurance">Страхование</Link>
        </Docker>
      </div>
    );
  }
}
