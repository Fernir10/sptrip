import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';

import {Search} from './../modules/search';
import {ModifyColor} from './../utils/color';
import {Breadcrumb} from './../components';

export const SearchPage = (props) => (
  <Fragment>
    <div className="container top20">
      <Breadcrumb bg={props.theme.blueColor} hover={ModifyColor(props.theme.blueColor, 10)} text={props.theme.whiteTextColor}>
        <Link to="/">На главную</Link>
        <span>Поиск</span>
      </Breadcrumb>
    </div>
    <Search {...props} title="Выберите себе путешествие!"/>
  </Fragment>
);
