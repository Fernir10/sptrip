import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {IFrame} from './../components';

export const Hotels = () => (
  <Fragment>
    <Helmet>
      <title>Отели</title>
      <meta name="description" content="SPTRIP.RU | Всевозможные варианты отелей, представленные модулем"/>
    </Helmet>
    <IFrame src="//hotels.sptrip.ru/iframe.js"/>
  </Fragment>
);