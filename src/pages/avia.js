import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';

import {IFrame} from './../components';

export const Avia = () => (
  <Fragment>
    <Helmet>
      <title>Популярные направления перелётов</title>
      <meta name="description" content="SPTRIP.RU | Всевозможные варианты перелетов, представленные модулем"/>
    </Helmet>
    <IFrame src="//avia.sptrip.ru/iframe.js"/>
  </Fragment>
);