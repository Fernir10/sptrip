import React, {Fragment, PureComponent} from 'react';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import styled from 'styled-components';

import superAgent from './../utils/superAgent';
import {ModifyColor} from './../utils/color';

import {Search} from './../modules/search';

import {Breadcrumb, Flag} from './../components';

const StyledFull = styled.div`
  background: url(${(props) => props.img}) no-repeat top / cover;
  padding-bottom: 40px;
  margin-bottom: 20px;
  position: relative;
  
  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: rgba(255, 255, 255, .7);
  }
`;

const StyledHeader = styled.h1`
  color: ${(props) => props.theme.blackColor};
`;

const StyledFlag = styled(Flag)`
  margin-right: 10px;
`;

@observer
export class RegionPage extends PureComponent {
  @observable store = {
    id: '',
    text: '',
    title: '',
    keywords: '',
    country: '',
    countryName: '',
    img: ''
  };

  componentWillMount() {
    const {match: {params: {region = 21} = {}} = {}, store: {id, text, title, keywords, country, countryName, img} = {}} = this.props;

    if (id !== region) {
      superAgent.getrecord({
        type: 'region',
        id: region
      }).then((data) => {
        this.store = {
          id: region,
          text: data.text,
          title: data.title,
          keywords: data.keywords,
          country: data.country,
          countryName: data.countryName,
          img: data.img || ''
        };
      });
    } else {
      this.store = {id, text, title, keywords, country, countryName, img};
    }
  }

  render() {
    const {id, country, countryName, title, text, keywords, img} = this.store;

    return (
      <Fragment>
        <Helmet>
          <title>{title}</title>
          <meta name="title" content={`Интернет-агентство SPTRIP.RU - ${title}`}/>
          <meta name="description" content={`Интернет-агентство SPTRIP.RU - ${title} ${keywords}`}/>
          <meta name="keywords" content={`${title} ${keywords}`}/>
        </Helmet>
        <StyledFull img={img}>
          <div className="container">
            <StyledHeader theme={this.props.theme} className="inline-flex"><StyledFlag country={country} size="big"/>Курорт {title}</StyledHeader>
            <Breadcrumb bg={this.props.theme.blueColor} hover={ModifyColor(this.props.theme.blueColor, 10)} text={this.props.theme.whiteTextColor}>
              <Link to="/">На главную</Link>
              <Link to={`/countries/${country}`}>{countryName}</Link>
              <span>{title}</span>
            </Breadcrumb>
            <Search
              className="bottom20"
              country={country}
              region={id}
              hideDeparture
              hideCountry
              hideRegions
              {...this.props}
              autosearch
            />
          </div>
        </StyledFull>
        <div className="container">
          {text !== '' ? (
            <section className="multi2 bottom20 top20" dangerouslySetInnerHTML={{__html: text}}/>
          ) : (
            <noindex>Извините, в данный момент информация добавляется...</noindex>
          )}
        </div>
      </Fragment>
    );
  }
}
