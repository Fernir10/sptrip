import React from 'react';

import {TourInfo} from '../modules/search/tour/tourInfo';

export const TourPage = (props) => <TourInfo {...props}/>;
