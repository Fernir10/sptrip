import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import styled from 'styled-components';

import superAgent from './../utils/superAgent';
import {Search} from './../modules/search';

import {countryNameFrom} from './../utils/countryNames';
import {ModifyColor} from './../utils/color';

import {HotTours} from './../modules/hot';

import {Breadcrumb, Flag} from './../components';

const StyledFull = styled.div`
  background: url(${(props) => props.img}) no-repeat top / cover;
  padding-bottom: 40px;
  margin-bottom: 20px;
  position: relative;
  
  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: rgba(255, 255, 255, .7);
  }
`;

const StyledHeader = styled.h1`
  color: ${(props) => props.theme.blackColor};
`;

const StyledHeader2 = styled.h2`
  color: ${(props) => props.theme.blackColor};
`;

const StyledFlag = styled(Flag)`
  margin-right: 15px;
`;

@observer
export class CountryPage extends Component {
  @observable store = {
    id: '',
    text: '',
    title: '',
    keywords: '',
    regions: [],
    img: '',
    country: ''
  };

  componentWillMount() {
    this.componentWillReceiveProps(this.props);
  }

  componentWillReceiveProps(props) {
    const {match: {params: {country = 21} = {}} = {}, store: {id: storeId} = {}} = props;

    if (storeId !== country) {
      superAgent.getrecord({
        type: 'country',
        id: country
      }).then(({id, text = '', title = '', keywords = '', regions = [], img = ''}) => {
        this.store = {id, text, title, keywords, regions, img, country};
      });
    } else {
      const {store: {id, text, title, keywords, regions = [], img} = {}} = props;
      this.store = {id, text, title, keywords, regions, img, country};
    }
  }

  render() {
    const {title, text, keywords, regions = [], img, country} = this.store;
    const titleText = `Поиск лучших туров в ${countryNameFrom(country)} онлайн`;

    return (
      <Fragment>
        <Helmet>
          <title>{titleText}</title>
          <meta property="og:title" content={`SPTRIP.RU | ${titleText}`}/>
          <meta name="description" content={titleText}/>
          <meta name="keywords" content={`${title} ${keywords}`}/>
          <meta property="og:image" content={`https://sptrip.ru/${img}`}/>
        </Helmet>
        <StyledFull img={img}>
          <div className="container">
            <StyledHeader theme={this.props.theme} className="inline-flex"><StyledFlag country={country} size="big"/>{title}</StyledHeader>
            <Breadcrumb bg={this.props.theme.orangeColor} hover={ModifyColor(this.props.theme.orangeColor, 10)} text={this.props.theme.whiteTextColor}>
              <Link to="/">На главную</Link>
              <span>{title}</span>
            </Breadcrumb>
            <Breadcrumb bg={this.props.theme.blueColor} hover={ModifyColor(this.props.theme.blueColor, 10)} text={this.props.theme.whiteTextColor}>
              {(regions && regions.map) && regions.map((reg) => (
                <Link
                  key={reg.id}
                  to={`/regions/${reg.id}`}
                >
                  {reg.title}
                </Link>
              ))}
            </Breadcrumb>
            <StyledHeader2 theme={this.props.theme} className="nomargin--bottom">Поиск лучших туров в {countryNameFrom(country)} онлайн</StyledHeader2>
            <Search
              className="bottom20"
              country={country}
              hideDeparture
              hideCountry
              {...this.props}
              autosearch
            />
          </div>
        </StyledFull>
        <div className="container">
          {text !== '' ? (
            <section className="multi2 bottom20 top20" dangerouslySetInnerHTML={{__html: text}}/>
          ) : (
            <noindex>Извините, в данный момент информация добавляется...</noindex>
          )}
          <h2>Горящие туры в {countryNameFrom(country) || title} из Москвы {(new Date()).getFullYear()}</h2>
          <HotTours
            nomenu
            count={4}
            departure={1}
            country={country}
            {...this.props}
          />
        </div>
      </Fragment>
    );
  }
}
