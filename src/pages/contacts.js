import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';

import {ModifyColor} from './../utils/color';
import {ContactsIcon} from './../icons';
import {Breadcrumb, Docker, IFrame} from './../components';

export const Contacts = ({theme}) => (
  <div className="container">
    <Helmet>
      <title>Свяжитесь с нами</title>
      <meta property="og:title" content="SPTRIP.RU | Свяжитесь с нами"/>
    </Helmet>
    <div className="container top20">
      <Breadcrumb bg={theme.blueColor} hover={ModifyColor(theme.blueColor, 10)} text={theme.whiteTextColor}>
        <Link to="/">На главную</Link>
        <span>Контакты</span>
      </Breadcrumb>
    </div>
    <h1 className="inline-flex"><ContactsIcon color={theme.color} style={{marginRight: 15}}/>О нас</h1>
    <Docker className="bottom20" top>
      <Docker columns top className="bottom20 noflex">
        <Fragment>
          <h3>Реквизиты</h3>
          <p>ИП Новиков Андрей Геннадьевич</p>
          <p>ИНН 710400364720</p>
          <p>ОГРНИП 316715400119637</p>
          <p>р/сч: 40802810600000062577</p>
          <p>АО «Тинькофф Банк» в г. Москва</p>
          <p>к/сч: 30101810145250000974</p>
          <p>БИК 044525974</p>
        </Fragment>
        <Fragment>
          <p><b>Адрес:</b> г. Москва, ул. Большая Якиманка, 38А</p>
          <p><b>Телефон:</b> <a href="tel:+74957443317">+7 (495) 744-33-17</a></p>
          <p><b>Время работы:</b> пн-сб, 09:00 — 19:00</p>
          <p><b>E-mail:</b> <a href="mailto:info@sptrip.ru">info@sptrip.ru</a></p>
        </Fragment>
      </Docker>
      <IFrame style={{marginBottom: 20}} src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A01dc6ef325559bbcbc8ae1bb549be5b90e9ea50b3d14b8f8c1f3f6d38d9d30eb&amp;width=100%&amp;height=450&amp;lang=ru_RU&amp;scroll=false"/>
    </Docker>
  </div>
);
