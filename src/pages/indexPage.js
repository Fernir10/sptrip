import React from 'react';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {Search} from './../modules/search';
import {HotToursIndexPage} from './../modules/hot/hotIndexPage';
import {SocialBlock} from './../modules/social';
import {ReviewsBlock} from './../modules/reviews';
import {MinPricesBlock} from '../modules/calendar/minPricesBlock';
import {Docker, Img} from './../components';

import './indexPage.less';

const StyledLink = styled(Link)`
  &:before {
    background-color: ${(props) => props.theme.orangeColor};
  }
  
  &:after {
    color: ${(props) => props.theme.color};
  }
`;

export const IndexPage = (props) => (
  <Search full {...props} title="Выберите себе путешествие!">
    <Helmet>
      <meta name="description" content="Интернет-агентство SPTRIP.RU - главная страница"/>
    </Helmet>
    <h2 className="text-center">Страны без визы</h2>
    <MinPricesBlock {...props}/>
    <HotToursIndexPage {...props}>
      <h2 className="text-center">Лучшие предложения недели</h2>
    </HotToursIndexPage>
    <Link to="/callus" className="text-center">
      <Img src="/img/select_variants_call.jpg" alt="Раннее бронирование - покупай раньше, плати меньше!" style={{maxWidth: '100%'}}/>
    </Link>
    <h2 className="text-center">Наши услуги</h2>
    <div className="flex flex-center">
      <Docker padding={0} inline>
        <StyledLink theme={props.theme} className="index-service index-service--avia" to="/avia">Авиаперелеты</StyledLink>
        <StyledLink theme={props.theme} className="index-service index-service--hotels" to="/hotels">Отели</StyledLink>
        <StyledLink theme={props.theme} className="index-service index-service--insurance" to="/insurance">Страхование</StyledLink>
      </Docker>
    </div>
    <ReviewsBlock theme={props.theme}/>
    <SocialBlock/>
  </Search>
);
