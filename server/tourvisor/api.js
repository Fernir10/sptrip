import superAgent from './../../src/utils/superAgent';
import {Countries, Hotels, Regions} from './../loadLists';
import {HotelsDetail} from './../hotelsDatabase';
import {Calendar} from './../casper';

class Tourvisor {
  credentials = require('./../../settings.json');

  log = (...args) => {
    if (!process.env.production) {
      console.log(...args);
    }
  };

  prepareParams = (params) => {
    Object.assign(params, {
      authlogin: this.credentials.login,
      authpass: this.credentials.password,
      format: 'json',
      removetags: 1,
      reviews: 1,
      request: 0
    });
    return params;
  };

  getCalendar = async (params) => Calendar.find({
    month: params.month || (new Date()).getMonth(),
    ...(params.country && {id: params.country})
  });

  async getHotelImages(params) {
    this.prepareParams(params);

    return new Promise((resolve, reject) => {
      superAgent.server.API_HOTEL_IMAGES_URL(params).then((response) => {
        const {data: {hotel: {images} = {}} = {}} = response;

        if (images) {
          resolve(images);
        } else {
          reject([]);
        }
      }).catch(reject);
    });
  }

  async getHotToursData(params) {
    this.prepareParams(params);

    return new Promise((resolve, reject) => {
      superAgent.server.API_HOTTOURS_URL(params).then((response) => {
        const {hottours: {tour} = {}} = response;

        if (tour) {
          const outputTour = tour.map((t) => {
            const {fulldesclink, fuelcharge, departurecode, hotelregioncode, countrycode, meal, operatorname, ...others} = t;

            return others;
          });

          Promise.all(outputTour.map((t) => new Promise((res) => HotelsDetail.findOne((err, data) => res(data || {})).where({id: t.hotelcode})))).then((hotelsData) => {
            resolve(outputTour.map((tourData) => {
              const found = hotelsData.find((h) => typeof h.id !== 'undefined' && h.id.toString() === tourData.hotelcode);
              if (typeof found !== 'undefined') {
                const {images: [firstImage] = []} = found;
                return firstImage ? Object.assign(tourData, {hotelpicture: firstImage.original}) : tourData;
              }

              return tourData;
            }));
          });
        } else {
          reject([]);
        }
      }).catch((d) => {
        this.log('getHotToursData alarm!');
        reject(d);
      });
    });
  }

  async getFormData(params) {
    this.prepareParams(params);

    return new Promise((resolve, reject) => {
      Calendar.find().then((dbCountries) => {
        if (params.country) {
          params.regcountry = params.country;
        }

        superAgent.server.API_LIST_URL(params).then((response) => {
          const {lists: {
            departures: {departure} = {},
            countries: {country} = {},
            hotels: {hotel} = {},
            regions: {region} = {},
            subregions: {subregion} = {},
            flydates: {flydate} = {},
            meals: {meal} = {},
            operators: {operator} = {}
          } = {}} = response;

          const parsed = {};

          Object.assign(parsed, {country: params.country, departure: params.departure});

          if (departure) {
            const msk = departure.find((itm) => itm.name.match(/москва/gi));
            const spb = departure.find((itm) => itm.name.match(/петербург/gi));
            departure.splice(departure.indexOf(msk), 1);
            departure.splice(departure.indexOf(spb), 1);
            Object.assign(parsed, {departures: [msk, spb, ...departure].map((dep) => ({id: dep.id, name: dep.name}))});
          }

          if (country) {
            const priorCountry = [];
            [
              /таиланд/gi,
              /турция/gi,
              /россия/gi,
              /тунис/gi,
              /греция/gi,
              /Кипр/gi,
              /словакия/gi,
              /италия/gi,
              /китай/gi,
              /куба/gi
            ].forEach((rex) => {
              const found = country.find((itm) => itm.name.match(rex));
              if (found) {
                priorCountry.push(found);
              }
              country.splice(country.indexOf(found), 1);
            });

            const joined = [...priorCountry, ...country];
            Object.assign(parsed, {countries: []});

            joined.forEach((c) => {
              const foundCountry = dbCountries.find((cn) => cn.id === Number(c.id));

              if (foundCountry) {
                parsed.countries.push({...c, visa: foundCountry.visa, img: foundCountry.img});
              } else {
                parsed.countries.push({...c});
              }
            });
          }

          if (hotel) {
            Object.assign(parsed, {hotels: hotel.map((itm) => {
              if (params.hotcountry || params.country) {
                Object.assign(itm, {country: params.country});
              }

              const {subregion, relax, health, beach, family, city, stars, ...other} = itm;
              return other;
            }).filter((itm) => itm.country === params.country || itm.country === params.hotcountry)});
          }

          if (region) {
            Object.assign(parsed, {regions: region.filter((reg) => reg.country === params.country).map((itm) => ({id: itm.id, name: itm.name}))});
          }

          if (subregion) {
            Object.assign(parsed, {subregions: subregion.filter((reg) => reg.country === params.country).map((itm) => ({id: itm.id, name: itm.name}))});
          }

          if (flydate) {
            Object.assign(parsed, {flydates: flydate});
          }

          if (meal) {
            Object.assign(parsed, {meals: meal});
          }

          if (operator) {
            Object.assign(parsed, {operators: operator.map((itm) => ({id: itm.id, name: itm.russian, online: itm.onlinebooking}))});
          }

          if (parsed.error) {
            reject(parsed.error);
            return;
          }

          if (parsed) {
            resolve(parsed);
          }
        });
      }).catch((d) => {
        this.log('getFormData alarm!');
        reject(d);
      });
    });
  }

  getHotelDetailsDB(params) {
    this.prepareParams(params);
    return HotelsDetail.findOne({id: params.hotelcode});
  }

  async getHotelDetails(params) {
    this.prepareParams(params);

    return new Promise((resolve, reject) => {
      HotelsDetail.findOne({
        id: params.hotelcode
      })
        .then((data) => {
          if (!data.images) {
            this.getHotelImages({hotelcode: params.hotelcode}).then((res) => {
              Object.assign(data, {
                images: res.map((img) => ({
                  original: img.big,
                  thumbnail: img.preview
                }))
              });
            });
          }

          this.log('update hotel!');
          data.id = params.hotelcode;
          data.timestamp = (new Date()).getTime() + (60 * 60 * 60 * 1000);

          data.update({...data}, {upsert: true});

          resolve(data);
        })
        .catch(() => {
          superAgent.server.API_HOTEL_URL(params).then((response) => {
            if (response) {
              const parsed = response;

              if (parsed.error) {
                reject(parsed.error);
                return;
              }

              if (parsed) {
                this.getHotelImages({hotelcode: params.hotelcode}).then((res) => {
                  Object.assign(parsed.data.hotel, {
                    images: res.map((img) => ({
                      original: img.big,
                      thumbnail: img.preview
                    }))
                  });

                  this.log('update hotel!');
                  parsed.data.hotel.id = params.hotelcode;
                  parsed.data.hotel.timestamp = (new Date()).getTime() + (60 * 60 * 60 * 1000);

                  const record = new HotelsDetail({...parsed.data.hotel});

                  record.save();

                  resolve(parsed.data.hotel);
                }).catch(() => {
                  resolve(parsed);
                });
              }
            }
          }).catch((d) => {
            this.log('getHotelDetails alarm!');
            reject(d);
          });
        });
    });
  }

  async getTour(params) {
    this.prepareParams(params);

    return new Promise((resolve, reject) => {
      superAgent.server.API_TOUR_URL(params).then((response) => {
        if (response) {
          const parsed = response;

          if (parsed.error) {
            reject(parsed.error);
            return;
          }

          if (parsed && parsed.data) {
            this.getHotelImages({hotelcode: parsed.data.tour.hotelcode}).then((res) => {
              Object.assign(parsed.data.tour, {
                images: res.map((img) => ({
                  original: img.big,
                  thumbnail: img.preview
                }))
              });

              resolve({...parsed.data});
            }).catch(reject);
          }
        }
      }).catch(reject);
    });
  }

  async getTourDetails(params) {
    this.prepareParams(params);

    return new Promise((resolve, reject) => {
      superAgent.server.API_ACTUALIZE_TOUR_URL(params)
        .then((response2) => {
          const parsed2 = response2;

          if (parsed2.error) {
            reject([]);
            return;
          }

          const {iserror, errormessage, ...otherData} = parsed2;

          resolve({...otherData});
        })
        .catch(reject);
    });
  }

  async getDictionary(params) {
    this.prepareParams(params);

    return new Promise((resolve, reject) => {
      superAgent.server.API_LIST_URL(params).then((response) => {
        if (response) {
          const parsed = response.lists;
          if (parsed.error) {
            reject(parsed.error);
            return;
          }
          if (parsed) {
            resolve(parsed);
          }
        }
      }).catch(reject);
    });
  }

  async getRequestId(params) {
    return new Promise((resolve, reject) => {
      this.prepareParams(params);

      superAgent.server.API_SEARCH_URL(params).then((response) => {
        if (response && response.error) {
          reject(response);
          return;
        }

        if (response.result) {
          const parsed = response;
          if (parsed && parsed.error) {
            reject(parsed);
            return;
          }
          if (parsed && parsed.result) {
            const requestId = parsed.result.requestid;
            if (requestId) {
              resolve(requestId);
            }
          }
        }
      }).catch(reject);
    });
  }

  async checkStatus(requestId, type = 'result') {
    const params = this.prepareParams({requestid: requestId, type, operatorstatus: 1});
    return new Promise((resolve, reject) => {
      superAgent.server.API_RESULT_URL(params).then((response) => {
        if (response) {
          const {error, data} = response;

          if (error) {
            reject(response);
            return;
          }

          if (data) {
            const {status: {progress} = {}} = {} = data;
            if (progress > 0) {
              resolve(data);
            }
          }
        }
      }).catch(reject);
    });
  }

  async checkDone(requestId) {
    return new Promise((resolve, reject) => {
      this.checkStatus(requestId).then((data) => {
        if (data && data.status.state === 'finished') {
          resolve(data);
        } else {
          reject(data);
        }
      }).catch(reject);
    });
  }

  async grabPages(requestId, page, resolve, reject, preToursData) {
    let pageToSend = page || 1;
    const preToursDataToSend = preToursData || {};

    const {results: {result: {hotel: preToursDataToSendHotel} = {}} = {}} = preToursDataToSend;

    const params = this.prepareParams({requestid: requestId, type: 'result', page: pageToSend, operatorstatus: 1});

    superAgent.server.API_RESULT_URL(params).then((response) => {
      if (response) {
        if (response.error) {
          reject(response);
          return;
        }

        const {data: {status: {state, hotelsfound}} = {}} = response;

        if (state === 'finished' && hotelsfound === 0) {
          reject(response.data);
          return;
        }

        const {data: {result: {hotel} = {}} = {}} = response;

        if (hotel) {
          const preData = {requestId, results: response.data};

          if (!preToursDataToSend.results) {
            preData.results.result.hotel.forEach((hot) => {
              HotelsDetail.findOne((err, data) => {
                if (!err && data) {
                  const {_doc: {images, beach, inroom, coord1, coord2} = {}} = data;

                  Object.assign(hot, {images, beach, inroom, coord1, coord2});
                  Object.assign(preToursDataToSend, preData);
                  return;
                }

                if (!err && !data) {
                  superAgent.hotel({
                    hotelcode: hot.hotelcode,
                    reviews: 1
                  }).then(({data: {hotel: foundHotel} = {}}) => {
                    if (foundHotel) {
                      this.log(`look new ${hot.hotelcode}`);
                      foundHotel.id = hot.hotelcode;
                      foundHotel.timestamp = (new Date()).getTime() + (3600 * 124);
                      const record = new HotelsDetail({...foundHotel});

                      record.save();
                      Object.assign(preToursDataToSend, preData);
                    }
                  }).catch(() => {
                    this.log(`fail to resolve ${hot.hotelcode}`);
                  });
                }
              }).where({id: hot.hotelcode});
            });
          }

          if (preToursDataToSend.results) {
            response.data.result.hotel.forEach((hot) => {
              if (!preToursDataToSend.results.result.hotel.some((d) => d.hotelcode === hot.hotelcode)) {
                HotelsDetail.findOne((err, data) => {
                  if (!err && data) {
                    const {_doc: {images, beach, inroom, territory, placement} = {}} = data;

                    Object.assign(hot, {images, beach, inroom, territory, placement});
                    preToursDataToSend.results.result.hotel.push(hot);
                    return;
                  }

                  if (!err && !data) {
                    superAgent.hotel({
                      hotelcode: hot.hotelcode,
                      reviews: 1
                    }).then(({data: {hotel: foundHotel} = {}}) => {
                      if (hotel) {
                        this.log(`look new ${hot.hotelcode}`);
                        foundHotel.id = hot.hotelcode;
                        foundHotel.timestamp = (new Date()).getTime() + (3600 * 124);
                        const record = new HotelsDetail({...foundHotel});

                        record.save();

                        preToursDataToSend.results.result.hotel.push(hot);
                      }
                    }).catch(() => {
                      this.log(`fail to resolve ${hot.hotelcode}`);
                    });
                  }
                }).where({id: hot.hotelcode});
              }
            });
          }
          pageToSend++;

          this.grabPages(requestId, pageToSend, resolve, reject, preToursDataToSend);
        } else if (preToursDataToSendHotel) {
          resolve(preToursDataToSend);
        } else if (state === 'searching') {
          this.grabPages(requestId, pageToSend, resolve, reject, preToursDataToSend);
        } else {
          reject({status: 'notfound'});
        }
      } else {
        reject();
      }
    }).catch(() => {
      reject();
    });
  }

  find = (collection, query, limit = 10) => new Promise((resolve) => {
    collection.find(query, (err, data) => {
      if (!err && data) {
        resolve(data);
      }
    }).limit(limit);
  });

  async getLists(query) {
    return new Promise((resolve) => {
      const countryNames = {};
      let dataOut = [];

      this.find(Countries, {}, 1000).then((data) => {
        data.forEach((i) => {
          countryNames[`${i.id}`] = i.name;
        });
      }).then(() => {
        this.find(Countries, {name: {$regex: query, $options: 'i'}}).then((countries) => {
          dataOut = [...dataOut, ...countries];
        });
      }).then(() => {
        this.find(Regions, {name: {$regex: query, $options: 'i'}}).then((data) => {
          dataOut = [...dataOut, ...data.map((item) => ({...item._doc, cn: countryNames[item.country]}))];
        });
      })
        .then(() => {
          this.find(Hotels, {name: {$regex: query, $options: 'i'}}).then((data) => {
            dataOut = [...dataOut, ...data.map((item) => ({...item._doc, cn: countryNames[item.country]}))];

            resolve(dataOut);
          });
        });
    });
  }

  async getResults(requestId, preToursData = {}) {
    return new Promise((resolve, reject) => {
      this.grabPages(requestId, null, resolve, reject, preToursData);
    });
  }

  async createRequest(params) {
    return new Promise((resolve, reject) => {
      this.getRequestId(params).then((requestId) => {
        this.checkDone(requestId).then(() => {
          this.getResults(requestId).then(resolve).catch(reject);
        }).catch(reject);
      }).catch(reject);
    });
  }

  moreSearch = (socket, dataStatus, requestId) => {
    const {status: {state} = {}} = dataStatus;
    if (state === 'finished') {
      socket.stop = true;
    }

    return this.createSearchStream(requestId, socket);
  };

  async createSearchStream(requestId, socket) {
    if (socket) {
      if (!socket.stop) {
        this.checkStatus(requestId).then((dataStatus) => {
          socket.emit('search', dataStatus);
          return this.moreSearch(socket, dataStatus, requestId);
        }).catch(() => {});
      }
    } else {
      this.log('no socket');
    }
  }

  async getSearchStreamResults(requestId, socket) {
    if (socket) {
      return this.getResults(requestId).then((response) => {
        delete response.results.result.status;
        socket.emit('search', response);
      }).catch(() => {});
    }

    return null;
  }
}


const api = new Tourvisor();

export {
  api
};
