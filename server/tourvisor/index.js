import {Router} from 'express';
import compression from 'compression';
import apicache from 'apicache';

import {api} from './api';

const tourvisor = Router();

const cache = apicache.middleware;

tourvisor.use(compression());


tourvisor.get('/api/tv/all/', (req, res) => api.getLists(`${req.query.query ? req.query.query : ''}`).then((response) => res.send(response)));

tourvisor.get('/api/tv/gettours/', cache(), (req, res) => api.getResults(req.query.requestId || 0).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/hot/', cache(), (req, res) => api.getHotToursData(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/form/', cache(), (req, res) => api.getFormData(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/info/', cache(), (req, res) => api.getDictionary(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/hotel/', cache(), (req, res) => api.getHotelDetails(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/hoteldb/', cache(), (req, res) => api.getHotelDetailsDB(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/search/', (req, res) => api.createRequest(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/tour/', (req, res) => api.getTour(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/tourdetails/', (req, res) => api.getTourDetails(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

tourvisor.get('/api/tv/calendar/', (req, res) => api.getCalendar(req.query).then((response) => res.send(response)).catch((response) => res.send(response)));

export {
  tourvisor
};

