import mongoose from 'mongoose';
import superAgent from '../src/utils/superAgent';

mongoose.Promise = Promise;

mongoose.connect('mongodb://localhost/sptrip', { useNewUrlParser: true });

mongoose.set('useCreateIndex', true);

const Departures = mongoose.model('departures', new mongoose.Schema({
  id: {type: Number, unique: true, required: true},
  name: {type: String, required: true},
  namefrom: {type: String}
}));

const Countries = mongoose.model('countries', new mongoose.Schema({
  id: {type: Number, unique: true, required: true},
  name: {type: String, required: true},
  country: {type: Number}
}));

const Regions = mongoose.model('regions', new mongoose.Schema({
  id: {type: Number, unique: true, required: true},
  name: {type: String, required: true},
  country: {type: Number}
}));

const Hotels = mongoose.model('hotels', new mongoose.Schema({
  id: {type: Number, unique: true, required: true},
  name: {type: String, required: true},
  stars: {type: Number},
  region: {type: Number},
  subregion: {type: Number},
  rating: {type: Number},
  country: {type: Number}
}));

const saveRecord = (collection, item) => {
  switch (collection) {
    case 'departures': {
      Departures.update({...item}, {upsert: true});
      break;
    }
    case 'countries': {
      Countries.update({...item}, {upsert: true});
      break;
    }
    case 'regions': {
      Regions.update({...item}, {upsert: true});
      break;
    }
    case 'hotels': {
      Hotels.update({...item}, {upsert: true});
      break;
    }
    default:
      break;
  }
};

const parseList = (countrylist, index) => {
  console.clear();
  console.log(`\x1b[32mprocessing: \x1b[31m${Math.ceil(index / (countrylist.length / 100))}\x1b[32m%`);
  const country = countrylist[index];
  if (country) {
    superAgent.form({
      type: 'region,hotel',
      country,
      regcountry: country,
      hotcountry: country
    }).then((list) => {
      if (list.regions) saveRecord('regions', list.regions.map((itm) => { itm.country = country; return itm; }));
      if (list.hotels) saveRecord('hotels', list.hotels.map((itm) => { itm.country = country; return itm; }));
      index++;
      parseList(countrylist, index);
    });
  } else {
    console.log('\x1b[37mcompleted!');
  }
};

const dropData = () => {
  console.log('clear collections...');
  Departures.remove();
  Countries.remove();
  Regions.remove();
  Hotels.remove();
};

const updateStoredData = () => {
  dropData();
  console.log('start updating!');
  superAgent.form({type: 'departure,country'}).then((data) => {
    if (data.countries) {
      saveRecord('countries', data.countries);
      if (data.departures) {
        saveRecord('departures', data.departures);
      }

      const countrylist = data.countries.map((c) => Number(c.id)).sort();

      parseList(countrylist, 0);
    }
  });
};

const updateList = (drop) => {
  if (drop) {
    updateStoredData();
  }
  setInterval(() => updateStoredData(), 24 * 60 * 60 * 1000);
};

export {
  Countries,
  Departures,
  Regions,
  Hotels,
  updateList
};
