import mongoose from 'mongoose';
import fs from 'fs';

import {countryFromName} from './../src/utils/countryNames';
import {formatDate} from './../src/utils/format';
import superAgent from './../src/utils/superAgent';

mongoose.Promise = Promise;

mongoose.connect('mongodb://localhost/sptrip', { useNewUrlParser: true });

const db = mongoose.connection;

const exec = require('child_process').exec;
const cheerio = require('cheerio');

const fullMonthsOne = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

mongoose.set('useCreateIndex', true);

const Calendar = mongoose.model('calendar', new mongoose.Schema({
  id: Number,
  month: Number,
  monthName: String,
  visa: Number,
  updated: String,
  year: String,
  temp: String,
  countryName: String,
  tempwater: String,
  img: String,
  columns: Array
}));

const updateCal = async () => {
  console.log('start calendar updating...');

  if (fs.existsSync('./public/uploads/fetched.data')) {
    const stdout = fs.readFileSync('./public/uploads/fetched.data', 'utf8');

    const $ = cheerio.load(stdout);
    const countries = [];

    superAgent.form({type: 'country'}).then(({countries: countryList = []}) => {
      $('.js-win-cal').each(function () {
        const countryNameTo = $(this).find('.TVCalCountryName').text().replace('в ', '')
          .replace('на ', '');

        const id = countryFromName(countryNameTo);

        if (id) {
          const {name: countryName = ''} = countryList.find((c) => c.id === id) || {};

          const [month, year] = $(this).find('.TVCalendarMonthValue').text().split(' ');
          const monthid = fullMonthsOne.indexOf(month);

          const visa = $(this).find('.TVCalendarCountryVisa').attr('title') ? 1 : 0;

          const obj = {
            id,
            month: monthid,
            monthName: month,
            countryName,
            visa,
            year,
            img: $(this).find('.TVCalendarWindowHeader').css('background-image'),
            updated: formatDate((new Date()).getTime()),
            temp: $(this).find('.TVCalTempSun').text(),
            tempwater: $(this).find('.TVCalTempWater').text(),
            columns: []
          };

          $(this).find('.TVCalDiagramItem').each(function () {
            const nightsResult = ($(this).find('.TVCalDiagramTitle').find('div:nth-child(2)').text() || '').match(/\d+/);
            const nights = nightsResult ? Number(nightsResult[0]) : 0;
            const price = Number($(this).find('.TVCalendarPriceValue').text().replace(/ /, ''));

            obj.columns.push({
              ...(nights && {nights}),
              price: price || 0
            });
          });

          countries.push(obj);
        }
      });

      console.log('done!');

      console.log(countries.length);

      if (countries.length) {
        console.log('recording...');
        db.collection('calendars').drop();

        Calendar.find().count().then((size) => console.log('removing all entries:', size));

        const tmpArr = [];

        countries.forEach((obj) => {
          if (!tmpArr.find((u) => u.id === obj.id && u.month === obj.month)) {
            (new Calendar(obj)).save();
            tmpArr.push(obj);
          }
        });

        Calendar.find().count().then((size) => console.log('now entries:', size));
        console.log('record done!');
      }
    });
  }
};

const updateCalendar = async (drop) => {
  if (drop) {
    console.log('start downloaded.....');
    exec('> public/uploads/fetched.data && casperjs server/loadCalendar.js | tee -a public/uploads/fetched.data', {maxBuffer: 1024 * 100000}, (error, stdout) => {
      if (error) {
        console.log(error);
        return;
      }

      console.log('file size:', stdout.length);

      console.log('end downloaded!');

      if (fs.existsSync('./public/uploads/fetched.data')) {
        updateCal();
      }
    });
  }

  // if (fs.existsSync('./public/uploads/fetched.data')) {
  //   updateCal();
  // }

  setInterval(() => updateCalendar(1), 24 * 60 * 60 * 1000);
};

export {
  updateCalendar,
  Calendar
};
