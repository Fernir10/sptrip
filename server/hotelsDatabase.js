import mongoose from 'mongoose';
import superAgent from '../src/utils/superAgent';

import {Hotels} from './loadLists';

mongoose.Promise = Promise;

mongoose.connect('mongodb://localhost/sptrip', { useNewUrlParser: true });

mongoose.set('useCreateIndex', true);

const HotelsDetail = mongoose.model('hotelsdetail', new mongoose.Schema({
  id: {type: Number, unique: true, required: true},
  name: {type: String, required: true},
  stars: Number,
  region: String,
  regioncode: Number,
  country: String,
  countrycode: Number,
  rating: Number,
  placement: String,
  phone: String,
  site: String,
  build: String,
  repair: String,
  square: String,
  description: String,
  territory: String,
  inroom: String,
  roomtypes: String,
  services: String,
  servicefree: String,
  servicepay: String,
  animation: String,
  child: String,
  beach: String,
  meallist: [String],
  mealtypes: String,
  images: mongoose.Schema.Types.Mixed,
  imagescount: Number,
  coord1: String,
  coord2: String,
  timestamp: Number,
  reviews: mongoose.Schema.Types.Mixed
}));

const parseHotel = (hotellist, hotelindex) => {
  console.clear();
  console.log(`\x1b[32mprocessing update hotels data: \x1b[31m${Math.ceil(hotelindex / (hotellist.length / 100))}\x1b[32m%`);
  if (hotellist[hotelindex]) {
    HotelsDetail.findOne({id: hotellist[hotelindex], timestamp: {$lt: (new Date()).getTime()}}, (err, rec) => {
      if (!err) {
        if (rec && rec.id) {
          superAgent.hotel({
            hotelcode: rec.id,
            reviews: 1
          })
            .then((hoteldata) => {
              if (hoteldata) {
                console.log(`update ${rec.id}`);
                hoteldata.id = rec.id;
                hoteldata.timestamp = (new Date()).getTime() + (3600 * 124);
                rec.update({...hoteldata});
              }
              hotelindex++;
              parseHotel(hotellist, hotelindex);
            }).catch(() => {
              hotelindex++;
              parseHotel(hotellist, hotelindex);
            });
        } else {
          superAgent.hotel({
            hotelcode: hotellist[hotelindex],
            reviews: 1
          })
            .then((hoteldata) => {
              if (hoteldata) {
                console.log(`look new ${hotellist[hotelindex]}`);
                hoteldata.id = hotellist[hotelindex];
                hoteldata.timestamp = (new Date()).getTime() + (3600 * 124);
                const record = new HotelsDetail({...hoteldata});
                record.save();
              }
              hotelindex++;
              parseHotel(hotellist, hotelindex);
            }).catch(() => {
              hotelindex++;
              parseHotel(hotellist, hotelindex);
            });
        }
      } else {
        console.log(err.message);
        hotelindex++;
        parseHotel(hotellist, hotelindex);
      }
    });
  } else {
    console.log('\x1b[37mcompleted!');
  }
};

const updateStoredData = () => {
  console.log('start updating!');
  Hotels.find({}, (herr, hrec) => {
    if (!herr) {
      const ids = hrec.map((r) => r.id);
      if (ids.length) {
        parseHotel(ids, 0);
      }
    } else {
      throw herr;
    }
  });
};

const updateHotelsDetailDatabase = (drop) => {
  if (drop) {
    // HotelsDetail.remove();
    updateStoredData();
  }
  setInterval(() => updateStoredData(), 3600 * 600 * 24 * 10);
};

export {
  HotelsDetail,
  updateHotelsDetailDatabase
};
