import express from 'express';
import nodemailer from 'nodemailer';
import React from 'react';
import v1 from 'uuid/v1';
import {renderToString} from 'react-dom/server';
import {StaticRouter} from 'react-router';
import {ServerStyleSheet, StyleSheetManager} from 'styled-components';
import path from 'path';
import compression from 'compression';
import mcache from 'memory-cache';
import bodyParser from 'body-parser';
import {Helmet} from 'react-helmet';
import netjet from 'netjet';

import superAgent from '../src/utils/superAgent';
import Site from '../src/site/index';
import {tourvisor} from './tourvisor/index';
import {admin} from './admin';
import {settings} from './usersettings';

import {updateList} from './loadLists';
import {updateHotelsDetailDatabase} from './hotelsDatabase';
import {updateCalendar} from './casper';

import {Country, Region} from './admin/record';

if (process.env.NODE_ENV !== 'production') {
  console.log('process env:', process.env.NODE_ENV);
  mcache.clear();
}

const curHash = v1();
const HASH_TIME = 360;
const app = express();

const cache = (duration) => (req, res, next) => {
  const key = `__express__${req.originalUrl}` || req.url;
  const cachedBody = mcache.get(key);
  if (cachedBody) {
    res.send(cachedBody);
    return;
  }
  res.sendResponse = res.send;
  res.send = (body) => {
    mcache.put(key, body, duration * 1000);
    res.sendResponse(body);
  };
  next();
};

app
  .use(bodyParser.json())
  .use(compression())
  .use((req, res, next) => ((req.secure || req.headers.host === '127.0.0.1') ? next() : res.status(403).redirect(`https://${req.headers.host}${req.url}`)))
  .use(admin)
  .use(tourvisor)
  .use(settings)
  .use(netjet())
  .use(express.static(path.join(__dirname, '../public'), {maxAge: '120d'}))
  .use((req, res, next) => {
    if (req.url.match(/favicon/gi)) {
      res.writeHead(200, {'Content-Type': 'image/x-icon'});
      res.end();
    }
    next();
  });


updateList();
updateCalendar();
updateHotelsDetailDatabase();

const credentials = require('../settings.json');

const transporter = nodemailer.createTransport({
  service: 'Yandex',
  auth: {
    user: credentials.MailLogin,
    pass: credentials.MailPassword
  }
}, {
  from: credentials.MailFrom
});

const adminMails = [
  'info@sptrip.ru',
  'ann@nic.ru',
  'cutterman12345@mail.ru'
];

app.post('/ajax/email/', (req, res) => {
  const mails = adminMails.slice(0);

  if (req.body.to) {
    mails.push(req.body.to);
  }

  mails.forEach((mail) => {
    transporter.sendMail({
      to: mail,
      subject: req.body.subject,
      html: req.body.body
    }, (err) => {
      if (err) {
        res.send(err);
        return;
      }
      res.send({status: 'ok'});
    });
  });
});

const startRouting = (defaultform, countriesLinks = []) => {
  setInterval(() => () => {
    Country.find({title: /^(.*)$/}).then((cdata = []) => { countriesLinks = cdata.map((r) => ({id: r.id, name: r.title})); });
  }, 60 * 60 * 1000);

  const renderPage = (req, res, component, data = {}, errcode) => {
    superAgent.getsettings({ip: req.connection.remoteAddress}).then((dataSettings) => {
      Object.assign(data, {settings: dataSettings});

      const sheet = new ServerStyleSheet();
      renderToString(sheet.collectStyles(component));
      const styleTags = sheet.getStyleElement();

      const helmet = Helmet.rewind();

      const body = renderToString(
        <StyleSheetManager sheet={sheet.instance}>
          <html lang="ru">
            <head>
              {helmet.base.toComponent()}
              {helmet.title.toComponent()}
              {helmet.meta.toComponent()}
              {helmet.link.toComponent()}
              {helmet.script.toComponent()}
              <link rel="preconnect" href="https://static.tourvisor.ru"/>
              <link rel="stylesheet" href={`/js/site.css?${curHash}`}/>
              {styleTags}
              <script src={`/js/babel-polyfill.js?${curHash}`} defer async/>
              <script src={`/js/site.js?${curHash}`} async/>
            </head>
            <body>
              <div className="site-wrapper">
                {component}
              </div>
              {data && (
                <script defer async dangerouslySetInnerHTML={{__html: `window.__store=${JSON.stringify(data)}`}}/>
              )}
            </body>
          </html>
        </StyleSheetManager>
      );

      if (errcode) {
        res.status(errcode);
      } else {
        res.status(200);
      }

      return res.send(`<!doctype html>${body}`);
    });
  };

  const renderApp = (res, req, allData) => {
    const appHtml = (
      <StaticRouter location={req.url} context={{}}>
        <Site store={allData} search={allData.search} tours={allData.tours} hotel={allData.hotel} hot={allData.hot} countriesLinks={countriesLinks}/>
      </StaticRouter>
    );

    return renderPage(req, res, appHtml, allData);
  };

  const searchMv = (req, res) => {
    res.header('Cache-Control', 'public, no-cache');

    const departure = req.params.departure || 1;
    const queryhotel = req.params.queryhotel && req.params.queryhotel.toString() !== '0' ? req.params.queryhotel : null;
    const country = req.params.country && req.params.country.toString() !== '0' ? req.params.country : null;

    superAgent.calendar()
      .then((calendar) => {
        superAgent.hot({
          items: 8,
          picturetype: 1,
          visa: 1
        })
          .then((hotdata) => {
            if (hotdata) {
              if (country) {
                superAgent.form({
                  type: 'departure,country,region,subregion,hotel',
                  cndep: departure,
                  ...(country && {hotcountry: country}),
                  ...(country && {country}),
                  ...(country && {regcountry: country})
                }).then((formdata) => {
                  formdata.meals = defaultform.meals;

                  const allData = {search: formdata, hot: hotdata, countriesLinks, calendar};

                  if (queryhotel) {
                    superAgent.hotel({
                      hotelcode: queryhotel,
                      reviews: 1,
                      removetags: 1
                    }).then((hoteldata) => {
                      Object.assign(allData, {hotel: hoteldata});
                      Object.assign(allData.hotel, {operators: defaultform.operators});
                      renderApp(res, req, allData);
                    });
                  } else {
                    renderApp(res, req, allData);
                  }
                });
              } else {
                renderApp(res, req, {
                  countriesLinks,
                  calendar,
                  search: {
                    departures: defaultform.departures,
                    countries: defaultform.countries,
                    regions: defaultform.regions,
                    subregions: defaultform.subregions
                  },
                  hot: hotdata
                });
              }
            }
          });
      });
  };

  app.get('/preselect/', cache(HASH_TIME), (req, res) => {
    superAgent.calendar()
      .then((calendar) => {
        const store = {
          search: {
            departures: defaultform.departures,
            countries: defaultform.countries,
            regions: defaultform.regions
          },
          countriesLinks,
          calendar
        };
        const appHtml = (
          <StaticRouter location={req.url} context={{}}>
            <Site store={store}/>
          </StaticRouter>
        );

        res.header('Cache-Control', 'public, no-cache');
        return renderPage(req, res, appHtml, store);
      });
  });
  app.get('/hottours/', cache(HASH_TIME), (req, res) => {
    res.header('Cache-Control', 'public, no-cache');

    superAgent.calendar()
      .then((calendar) => superAgent.hot({
        items: 12,
        picturetype: 1,
        city: 1,
        countries: 4,
        stars: 3,
        visa: 0
      })
        .then((data) => {
          const retData = {
            tourList: data,
            countries: defaultform.countries,
            departures: defaultform.departures,
            operators: defaultform.operators,
            countriesLinks,
            calendar
          };

          const appHtml = (
            <StaticRouter location={req.url} context={{}}>
              <Site store={retData}/>
            </StaticRouter>
          );

          return renderPage(req, res, appHtml, retData);
        }));
  });

  app.get('/hotel/:hotelcode?/:search?/', cache(HASH_TIME), (req, res) => {
    res.header('Cache-Control', 'public, no-cache');
    const allData = {};

    superAgent.hotel({hotelcode: req.params.hotelcode, reviews: 1}).then((hoteldata) => {
      if (hoteldata) {
        Object.assign(allData, {hotel: hoteldata, countriesLinks});
        if (defaultform && defaultform.operators) {
          Object.assign(allData.hotel, {operators: defaultform.operators});

          const appHtml = (
            <StaticRouter location={req.url} context={{}}>
              <Site store={allData}/>
            </StaticRouter>
          );

          renderPage(req, res, appHtml, allData);
        }
      }
    });
  });

  app.get('/tour/:tourid?/', cache(HASH_TIME), (req, res) => {
    res.header('Cache-Control', 'public, no-cache');

    superAgent.tour({tourid: req.params.tourid}).then((tourdata) => {
      const retData = {meals: defaultform.meals, countries: defaultform.countries, countriesLinks};

      Object.assign(tourdata, retData);

      if (tourdata.errormessage || !tourdata.tour) {
        const appHtml = (
          <StaticRouter location={req.url} context={{}}>
            <Site store={tourdata}/>
          </StaticRouter>
        );

        renderPage(req, res, appHtml, tourdata);
        return;
      }

      superAgent.tourdetails({tourid: req.params.tourid}).then((tourDetails) => {
        Object.assign(tourdata, {...tourDetails});

        const appHtml = (
          <StaticRouter location={req.url} context={{}}>
            <Site store={tourdata}/>
          </StaticRouter>
        );

        renderPage(req, res, appHtml, tourdata);
      });
    });
  });

  app.get('/', cache(HASH_TIME), searchMv);

  app.get('/test/', cache(HASH_TIME), searchMv);

  app.get('/search/:departure?/:country?/:region?/:hotel?/:queryhotel?/:datefrom?/:dateto?/:nightsfrom?/:nightsto?/:adults?/:child?/:childage1?/:childage2?/:childage3?/:beach?/:auto?/', cache(HASH_TIME), searchMv);

  const defaultMV = (req, res, next, errorcode) => {
    superAgent.calendar()
      .then((calendar) => {
        const store = {countriesLinks, calendar};

        const appHtml = (
          <StaticRouter location={req.url} context={{}}>
            <Site store={store}/>
          </StaticRouter>
        );

        res.header('Cache-Control', 'public, no-cache');

        return renderPage(req, res, appHtml, store, errorcode);
      });
  };

  app.get('/personal/', cache(HASH_TIME), defaultMV);
  app.get('/callus/', cache(HASH_TIME), defaultMV);
  app.get('/avia/', cache(HASH_TIME), defaultMV);
  app.get('/hotels/', cache(HASH_TIME), defaultMV);
  app.get('/insurance/', cache(HASH_TIME), defaultMV);
  app.get('/contacts/', cache(HASH_TIME), defaultMV);
  app.get('/reviews/', cache(HASH_TIME), defaultMV);


  app.get('/countries/:country?/', cache(HASH_TIME), (req, res) => {
    const country = req.params.country || 4;

    Country.findOne({id: country})
      .then(({id, text = '', title = '', keywords = '', img = ''} = {}) => {
        Region.find({country, text: /(.*)/})
          .then((regions) => {
            superAgent.calendar({country})
              .then((calendarData) => {
                superAgent.hot({
                  items: 4,
                  picturetype: 1,
                  city: 1,
                  countries: country,
                  stars: 3,
                  visa: 0
                })
                  .then((data) => {
                    const store = {
                      calendarData,
                      tourList: data,
                      countries: defaultform.countries,
                      departures: defaultform.departures,
                      operators: defaultform.operators,
                      id,
                      text,
                      title,
                      keywords,
                      img,
                      regions,
                      countriesLinks
                    };

                    const appHtml = (
                      <StaticRouter location={req.url} context={{}}>
                        <Site store={store}/>
                      </StaticRouter>
                    );

                    res.header('Cache-Control', 'public, no-cache');
                    renderPage(req, res, appHtml, store);
                  });
              });
          });
      });
  });

  app.get('/regions/:region?/', cache(HASH_TIME), (req, res) => {
    const region = req.params.region || 21;

    Country.find().then((countriesData = {}) => {
      const countryNames = {};

      countriesData.forEach((country) => { countryNames[country.id] = country.title || ''; });

      Region.findOne({id: region}).then((regdata = {}) => {
        const store = {
          id: regdata.id,
          text: (regdata.text || '').replace(/(?:\r\n|\r|\n)/gi, ' ').replace(/ {2}/gi, ' '),
          title: regdata.title || '',
          keywords: regdata.keywords || '',
          country: regdata.country || 0,
          countryName: countryNames[regdata.country] || '',
          img: regdata.img || '',
          countriesLinks
        };

        const appHtml = (
          <StaticRouter location={req.url} context={{}}>
            <Site store={store}/>
          </StaticRouter>
        );

        res.header('Cache-Control', 'public, no-cache');
        renderPage(req, res, appHtml, store);
      });
    });
  });

  app.get('*', (req, res, next) => defaultMV(req, res, next, 404));

  return app;
};

superAgent.form({type: 'departure,country,operator,meal,region,subregion', departure: 1, cndep: 1, country: 4, regcountry: 4}).then((infodata) => {
  let defaultform = {};
  let countriesLinks = [];

  defaultform = infodata;

  Country.find({title: /^(.*)$/}).then((cdata = []) => {
    countriesLinks = cdata.map((r) => ({id: r.id, name: r.title}));

    startRouting(defaultform, countriesLinks);
  });
});

module.exports = app;
