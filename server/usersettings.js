import {Router} from 'express';
import mcache from 'memory-cache';
import compression from 'compression';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

mongoose.Promise = Promise;

mongoose.connect('mongodb://localhost/sptrip', { useNewUrlParser: true });

const settings = Router();

mongoose.set('useCreateIndex', true);

const UserSettings = mongoose.model('user_settings', new mongoose.Schema({
  id: {type: String, unique: true, required: true},
  settings: Object
}));

settings
  .use(compression())
  .use(bodyParser.json());

settings.post('/settings_save', (req, res) => {
  UserSettings.findOneAndUpdate({
    id: req.connection.remoteAddress
  }, {settings: req.body.settings}, {upsert: true})
    .then(() => {
      mcache.clear();
      res.send({status: 'success'});
    })
    .catch((err) => {
      res.send({error: err.message});
    });
});

settings.get('/settings', (req, res) => {
  UserSettings.findOne({id: req.query.ip || req.connection.remoteAddress})
    .then((data) => {
      res.send(data.settings || {});
    })
    .catch((err) => {
      res.send({error: err.message});
    });
});

export {
  settings
};
