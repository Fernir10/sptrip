import React, {Fragment} from 'react';
import v1 from 'uuid/v1';
import {renderToString} from 'react-dom/server';
import {Router} from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
import mongoose from 'mongoose';

import superAgent from '../../src/utils/superAgent';

mongoose.Promise = Promise;

const MongoStore = require('connect-mongo')(session);

mongoose.connect('mongodb://localhost/sptrip', { useNewUrlParser: true });

const db = mongoose.connection;
const autorize = Router();

autorize.use(compression());

// Middlewares, которые должны быть определены до passport:
autorize.use(cookieParser());
autorize.use(bodyParser.json());

autorize.use(session({
  secret: v1(),
  store: new MongoStore({mongooseConnection: mongoose.connection}),
  name: 'sptrip_user',
  proxy: true,
  resave: true,
  saveUninitialized: true
}));

// Passport:
autorize.use(passport.initialize());
autorize.use(passport.session());

mongoose.set('useCreateIndex', true);

const User = mongoose.model('user', new mongoose.Schema({
  username: {type: String, unique: true, required: true},
  password: {type: String, required: true},
  email: {type: String, required: true},
  access: String
}));

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, (username, password, done) => {
  User.findOne({username}, (err, user) => (err
    ? done(err)
    : user
      ? password === user.password
        ? done(null, user)
        : done(null, false, {message: 'password'})
      : done(null, false, {message: 'username'})));
}));

passport.serializeUser((user, done) => done(null, user._id));
passport.deserializeUser((id, done) => User.findById(id, (err, user) => done(err, user)));


autorize.post('/login', (req, res, next) => passport.authenticate('local', (err, user, info) => {
  if (err) {
    return next(err);
  } else if (user) {
    req.logIn(user, (errLogin) => {
      if (errLogin) {
        return next(errLogin);
      }

      if (user && user.access === 'full') {
        return res.redirect('/admin/');
      }

      return res.send({error: 'Страница доступна только пользователям с полным доступом!'});
    });
  }

  if (info && info.message) {
    return res.send({error: info.message});
  }
})(req, res, next));

autorize.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/admin/');
});

// Регистрация пользователя. Создаем его в базе данных, и тут же, после сохранения, вызываем метод `req.logIn`, авторизуя пользователя
autorize.post('/register', (req, res) => {
  const user = new User({
    username: req.body.email,
    password: req.body.password,
    email: req.body.emailaddr,
    access: req.body.email === 'admin' ? 'full' : ''
  });

  user.save().then(() => req.logIn(user, (loginErr) => {
    if (loginErr) {
      return res.send({error: loginErr});
    }
    if (user && user.access === 'full') {
      return res.redirect('/admin/');
    }
    return res.send({error: 'Страница доступна только пользователям с полным доступом!'});
  })).catch((err) => res.send({error: err.message.match(/duplicate key/) ? `Пользователь ${req.body.email} уже существует` : err.message}));
});

autorize.post('/edit', (req, res) => {
  User.findOneAndUpdate({username: req.user.username}, {
    ...(req.body.password !== '' && {password: req.body.password}),
    ...(req.body.emailaddr !== '' && {email: req.body.emailaddr})
  }, {upsert: true})
    .then(() => res.redirect('/admin/'))
    .catch((err) => res.send({error: err.message}));
});

autorize.post('/delete', (req, res) => {
  User.findOneAndRemove({username: req.body.email})
    .then(() => res.send({status: 'success'}))
    .catch((err) => res.send({error: err.message}));
});

autorize.post('/access', (req, res) => {
  User.findOneAndUpdate({username: req.body.email}, {access: req.body.access}, {upsert: true})
    .then(() => res.send({status: 'success'}))
    .catch((err) => res.send({error: err.message}));
});

autorize.post('/restore', (req, res) => {
  const curHash = (v1()).substr(1, 5);

  User.findOneAndUpdate({username: req.body.email}, {password: curHash}, {upsert: true})
    .then((data) => {
      if (data.email) {
        superAgent.email({
          subject: 'Новый пароль',
          body: renderToString(
            <Fragment>
              <p>Новый пароль</p>
              <p>{curHash}</p>
            </Fragment>
          )
        })
          .then(() => {
            res.send({state: 'success'});
          })
          .catch(() => {
            res.send({state: 'fail'});
          });
      } else {
        res.send({error: 'Не указана почта для отправки нового пароля!'});
      }
    }).catch((err) => {
      res.send({error: err.message});
    });
});

const mustAuthenticatedMw = (req, res, next) => {
  User.findOneAndRemove({username: 'null'});
  User.findOneAndRemove({username: ''});
  User.findOneAndRemove({email: ''});
  User.findOneAndRemove({email: 'null'});

  if (req.isAuthenticated()) {
    if (req.user && req.user.access === 'full') {
      next();
    } else {
      res.redirect('/login');
    }
  } else if (req.url === '/restore') {
    next();
  } else {
    res.redirect('/login');
  }
};

export {
  mustAuthenticatedMw,
  User,
  autorize
};
