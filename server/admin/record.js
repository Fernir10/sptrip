import {Router} from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import formidable from 'express-formidable';
import fs from 'fs';


import {autorize, mustAuthenticatedMw} from './autorize';

mongoose.Promise = Promise;

mongoose.connect('mongodb://localhost/sptrip', { useNewUrlParser: true });

const record = Router();

record.use(compression());
record.use(bodyParser.json());
record.use(autorize);

mongoose.set('useCreateIndex', true);

const Country = mongoose.model('country', new mongoose.Schema({
  id: {type: String, unique: true, required: true},
  title: String,
  text: String,
  keywords: String,
  regions: Array,
  img: {data: Buffer, contentType: String}
}));

const Region = mongoose.model('region', new mongoose.Schema({
  id: {type: String, unique: true, required: true},
  country: String,
  title: String,
  text: String,
  keywords: String,
  countryName: String,
  img: {data: Buffer, contentType: String}
}));

const Hotel = mongoose.model('hotel', new mongoose.Schema({
  id: {type: String, unique: true, required: true},
  country: String,
  title: String,
  text: String,
  keywords: String,
  region: String,
  regionName: String,
  countryName: String,
  img: {data: Buffer, contentType: String}
}));

record.post('/record', mustAuthenticatedMw, formidable({
  encoding: 'utf-8',
  uploadDir: './public/uploads'
}), (req, res, next) => {
  switch (req.fields.type) {
    case 'country':
    {
      if (req.files.img) {
        const filePath = `./public/uploads/${req.fields.id}.${req.files.img.name.split('.')[1]}`;

        fs.renameSync(req.files.img.path, filePath);
      }

      Country.findOneAndUpdate({id: req.fields.id}, {
        title: req.fields.title,
        text: req.fields.text,
        keywords: req.fields.keywords,
        regions: req.fields.regions,
        ...(req.files.img && {img: `/uploads/${req.fields.id}.${req.files.img.name.split('.')[1]}`})
      }, {upsert: true})
        .then(() => {
          res.send({status: 'success'});
          next();
        })
        .catch((err) => {
          res.send({error: err.message});
          next();
        });
      break;
    }
    case 'region': {
      if (req.files.img) {
        const filePath = `./public/uploads/region-${req.fields.id}.${req.files.img.name.split('.')[1]}`;

        fs.renameSync(req.files.img.path, filePath);
      }

      Region.findOneAndUpdate({id: req.fields.id}, {
        countryName: req.fields.countryName,
        country: req.fields.country,
        title: req.fields.title,
        keywords: req.fields.keywords,
        text: req.fields.text,
        ...(req.files.img && {img: `/uploads/region-${req.fields.id}.${req.files.img.name.split('.')[1]}`})
      }, {upsert: true})
        .then(() => {
          res.send({status: 'success'});
          next();
        })
        .catch((err) => {
          res.send({error: err.message});
          next();
        });
      break;
    }
    case 'hotel': {
      Hotel.findOneAndUpdate({id: req.fields.id}, {
        region: req.fields.region,
        regionName: req.fields.regionName,
        countryName: req.fields.countryName,
        country: req.fields.country,
        title: req.fields.title,
        keywords: req.fields.keywords,
        text: req.fields.text
      }, {upsert: true})
        .then(() => {
          res.send({status: 'success'});
          next();
        })
        .catch((err) => {
          res.send({error: err.message});
          next();
        });
      break;
    }
    default:
      res.send({status: 'success'});
      next();
  }
});

record.post('/getrecord', (req, res, next) => {
  switch (req.body.type) {
    case 'country':
    {
      Country.findOne({id: req.body.id})
        .then((data) => {
          Region.find({country: req.body.id, text: /(.*)/})
            .then((regions) => {
              res.send({
                text: data.text || '',
                title: data.title || '',
                regions,
                keywords: data.keywords || '',
                img: data.img || ''
              });
              next();
            });
        }).catch((err) => {
          res.send({error: err.message});
          next();
        });
      break;
    }
    case 'region': {
      Region.findOne({id: req.body.id})
        .then((data) => {
          res.send({
            text: data ? data.text : '',
            title: data ? data.title : '',
            keywords: data.keywords || '',
            country: data.country || '',
            countryName: data.countryName || '',
            img: data.img || ''
          });
          next();
        })
        .catch((err) => {
          res.send({error: err.message});
          next();
        });
      break;
    }
    case 'hotel': {
      Hotel.findOne({id: req.body.id})
        .then((data) => {
          res.send({
            text: data.text || '',
            title: data.title || '',
            keywords: data.keywords || '',
            country: data.country || '',
            countryName: data.countryName || '',
            region: data.region || '',
            regionName: data.regionName || ''
          });
          next();
        })
        .catch((err) => {
          res.send({error: err.message});
          next();
        });
      break;
    }
    default:
      next();
  }
});

export {
  record,
  Country,
  Region,
  Hotel
};
