import React from 'react';
import express from 'express';
import v1 from 'uuid/v1';
import {renderToString} from 'react-dom/server';
import {StaticRouter} from 'react-router';
import {ServerStyleSheet, StyleSheetManager} from 'styled-components';
import path from 'path';
import compression from 'compression';
import bodyParser from 'body-parser';
import {Helmet} from 'react-helmet';

import superAgent from './../../src/utils/superAgent';
import {tourvisor} from './../tourvisor';
import {autorize, mustAuthenticatedMw, User} from './autorize';
import {record} from './record';
import Admin from './../../src/admin/';

const curHash = v1();

const app = express();

app.use(tourvisor);
app.use(autorize);
app.use(record);

app.use(bodyParser.json());
app.use(compression());

app.use(express.static(path.join(__dirname, '../../public'), {maxAge: '120d'}));

let defaultform = {};
superAgent.form({type: 'country,hotel', departure: 1, cndep: 1, country: 4, hotcountry: 4}).then((infodata) => { defaultform = infodata; });


const renderPage = (res, component, data, errcode) => {
  const sheet = new ServerStyleSheet();
  renderToString(sheet.collectStyles(component));
  const styleTags = sheet.getStyleElement();

  const helmet = Helmet.rewind();

  const body = renderToString(
    <StyleSheetManager sheet={sheet.instance}>
      <html lang="ru">
        <head>
          {helmet.base.toComponent()}
          {helmet.title.toComponent()}
          {helmet.meta.toComponent()}
          {helmet.link.toComponent()}
          {helmet.script.toComponent()}
          <link rel="stylesheet" type="text/css" href={`/js/site.css?${curHash}`}/>
          <link rel="preconnect" href="https://static.tourvisor.ru"/>
          {styleTags}
          <script src={`/js/babel-polyfill.js?${curHash}`} defer async/>
          <script src={`/js/admin.js?${curHash}`} async/>
        </head>
        <body>
          <div className="site-wrapper">
            {component}
          </div>
          {data && (
            <script defer async dangerouslySetInnerHTML={{__html: `window.__store=${JSON.stringify(data)}`}}/>
          )}
        </body>
      </html>
    </StyleSheetManager>
  );

  if (errcode) {
    res.status(errcode);
  } else {
    res.status(200);
  }

  return res.send(`<!doctype html>${body}`);
};

const renderApp = (res, req, allData) => {
  const appHtml = (
    <StaticRouter location={req.url} context={{}}>
      <Admin store={allData}/>
    </StaticRouter>
  );

  return renderPage(res, appHtml, allData);
};

const pageMw = (req, res) => {
  res.header('Cache-Control', 'public, no-cache');
  renderApp(res, req, {
    user: req.user ? req.user.username : '',
    email: req.user ? req.user.email : '',
    users: res.users || []
  });
};

const usersMv = async (req, res, next) => {
  User.find({username: {$ne: req.user.username}}, (err, data) => {
    if (!err) {
      if (req.isAuthenticated() && req.user && req.user.access === 'full') {
        res.users = data.map((u) => ({username: u.username, email: u.email, access: u.access}));
      }
    }

    next();
  });
};

app.get('/restore', (req, res, next) => {
  if (req.isAuthenticated() && req.user && req.user.access === 'full') {
    res.redirect('/admin/');
    return;
  }
  next();
}, pageMw);

app.get('/login', (req, res, next) => {
  if (req.isAuthenticated() && req.user && req.user.access === 'full') {
    res.redirect('/admin/');
    return;
  }
  next();
}, pageMw);

app.get('/register', (req, res, next) => {
  if (req.isAuthenticated() && req.user && req.user.access === 'full') {
    res.redirect('/admin/');
    return;
  }
  next();
}, pageMw);

app.get('/users', mustAuthenticatedMw, usersMv, pageMw);

app.get('/edit', mustAuthenticatedMw, usersMv, pageMw);

app.get('/logout', mustAuthenticatedMw, pageMw);

app.get('/admin/', mustAuthenticatedMw, usersMv, (req, res) => {
  res.header('Cache-Control', 'public, no-cache');

  renderApp(res, req, {
    user: req.user ? req.user.username : '',
    email: req.user ? req.user.email : '',
    users: res.users || [],
    country: defaultform.countries
  });
});

app.get('/admin/country/:country?/:region?/:hotel?/', mustAuthenticatedMw, usersMv, (req, res) => {
  res.header('Cache-Control', 'public, no-cache');

  superAgent.form({type: 'region', departure: 1, cndep: 1, country: req.params.country || 4}).then((infodata) => {
    renderApp(res, req, {
      user: req.user ? req.user.username : '',
      email: req.user ? req.user.email : '',
      users: res.users || [],
      region: infodata.regions,
      country: defaultform.countries
    });
  });
});

app.get('/admin/region/:country?/:region?/:hotel?/', mustAuthenticatedMw, usersMv, (req, res) => {
  res.header('Cache-Control', 'public, no-cache');

  superAgent.form({type: 'region', departure: 1, cndep: 1, country: req.params.country || 4}).then((infodata) => {
    renderApp(res, req, {
      user: req.user ? req.user.username : '',
      email: req.user ? req.user.email : '',
      users: res.users || [],
      region: infodata.regions,
      country: defaultform.countries
    });
  });
});

app.get('/admin/hotel/:country?/:region?/:hotel?/', mustAuthenticatedMw, usersMv, (req, res) => {
  res.header('Cache-Control', 'public, no-cache');

  superAgent.form({type: 'hotel', departure: 1, cndep: 1, country: req.params.country || 4, hotcountry: req.params.country || 4}).then((infodata) => {
    renderApp(res, req, {
      user: req.user ? req.user.username : '',
      email: req.user ? req.user.email : '',
      users: res.users || [],
      hotel: infodata.hotels,
      country: defaultform.countries
    });
  });
});

app.get('/admin/*', mustAuthenticatedMw, usersMv, pageMw);

module.exports = {
  admin: app
};
