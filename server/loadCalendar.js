const cs = require('casper').create();

cs.options.waitTimeout = 1000;

cs.start();

function calcStep() {
  cs
    .waitForSelector('.TVMFDates')
    .thenClick('.TVMFDates .TVTextBoxSelector')
    .thenClick('.TVListBlock2 .TVListBoxItem:nth-child(1)')
    .waitForSelector('.TVCalendarCountyList')
    .thenClick('.TVCalShowAll')
    .waitForSelector('.TVCalendarRow:nth-child(30)')
    .then(function() {
      this
        .waitForSelector('.TVCalendarCountyList')
        .then(function() {
          var rows = this.evaluate(function() { return Array.prototype.slice.call(document.querySelectorAll('.TVCalendarRow')); });

          rows = rows.slice(0, rows.length - 1);

          var bindex = 1;

          this.eachThen(rows, function() {
            this
              .waitForSelector('.TVCalendarList:nth-child(' + bindex + ')')
              .thenClick('.TVCalendarList:nth-child(' + bindex + ')')
              .waitForSelector('.TVCalDiagram')
              .then(function () {
                this.echo('<div class="js-win-cal"><div class="js-win-cal-row">' + this.getHTML('.TVCalendarList:nth-child(' + bindex + ')') + '</div>' + this.getHTML('.TVCalendarWindow') + '</div>');
              })
              .eachThen([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], function() {
                this
                  .waitForSelector('.TVCalDiagram')
                  .thenClick('.TVCalendarNext')
                  .waitForSelector('.TVCalendarPriceValue')
                  .then(function () {
                    this.echo('<div class="js-win-cal"><div class="js-win-cal-row">' + this.getHTML('.TVCalendarList:nth-child(' + bindex + ')') + '</div>' + this.getHTML('.TVCalendarWindow') + '</div>');
                  });
              })
              .then(function(){
                bindex++;
              });
          });
        });
    });
}

cs
  .thenOpen('http://tourvisor.ru/modules-calendar.php')
  .then(function() {
    calcStep();
  });


cs.run();
