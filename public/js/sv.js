/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/root/site/public";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/modules/sv/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/modules/sv/index.js":
/*!*********************************!*\
  !*** ./src/modules/sv/index.js ***!
  \*********************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar PRECACHE = 'precache-v1';\nvar RUNTIME = 'runtime';\n\nvar PRECACHE_URLS = ['', '/', '/js/styles.css', '/js/entry.js'];\n\nself.addEventListener('install', function (event) {\n  event.waitUntil(caches.open(PRECACHE).then(function (cache) {\n    return cache.addAll(PRECACHE_URLS);\n  }).then(self.skipWaiting()));\n}, { passive: true });\n\nself.addEventListener('activate', function (event) {\n  var currentCaches = [PRECACHE, RUNTIME];\n  event.waitUntil(caches.keys().then(function (cacheNames) {\n    return cacheNames.filter(function (cacheName) {\n      return !currentCaches.includes(cacheName);\n    });\n  }).then(function (cachesToDelete) {\n    return Promise.all(cachesToDelete.map(function (cacheToDelete) {\n      return caches.delete(cacheToDelete);\n    }));\n  }).then(function () {\n    return self.clients.claim();\n  }));\n}, { passive: true });\n\nself.addEventListener('fetch', function (event) {\n  if (event.request.url.startsWith(self.location.origin)) {\n    event.respondWith(caches.match(event.request).then(function (cachedResponse) {\n      if (cachedResponse) {\n        return cachedResponse;\n      }\n\n      return caches.open(RUNTIME).then(function (cache) {\n        return fetch(event.request).then(function (response) {\n          return cache.put(event.request, response.clone()).then(function () {\n            return response;\n          });\n        });\n      });\n    }));\n  }\n}, { passive: true });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvbW9kdWxlcy9zdi9pbmRleC5qcz9lZDQwIl0sIm5hbWVzIjpbIlBSRUNBQ0hFIiwiUlVOVElNRSIsIlBSRUNBQ0hFX1VSTFMiLCJzZWxmIiwiYWRkRXZlbnRMaXN0ZW5lciIsImV2ZW50Iiwid2FpdFVudGlsIiwiY2FjaGVzIiwib3BlbiIsInRoZW4iLCJjYWNoZSIsImFkZEFsbCIsInNraXBXYWl0aW5nIiwicGFzc2l2ZSIsImN1cnJlbnRDYWNoZXMiLCJrZXlzIiwiY2FjaGVOYW1lcyIsImZpbHRlciIsImNhY2hlTmFtZSIsImluY2x1ZGVzIiwiY2FjaGVzVG9EZWxldGUiLCJQcm9taXNlIiwiYWxsIiwibWFwIiwiY2FjaGVUb0RlbGV0ZSIsImRlbGV0ZSIsImNsaWVudHMiLCJjbGFpbSIsInJlcXVlc3QiLCJ1cmwiLCJzdGFydHNXaXRoIiwibG9jYXRpb24iLCJvcmlnaW4iLCJyZXNwb25kV2l0aCIsIm1hdGNoIiwiY2FjaGVkUmVzcG9uc2UiLCJmZXRjaCIsInJlc3BvbnNlIiwicHV0IiwiY2xvbmUiXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBTUEsV0FBVyxhQUFqQjtBQUNBLElBQU1DLFVBQVUsU0FBaEI7O0FBRUEsSUFBTUMsZ0JBQWdCLENBQ3BCLEVBRG9CLEVBRXBCLEdBRm9CLEVBR3BCLGdCQUhvQixFQUlwQixjQUpvQixDQUF0Qjs7QUFPQUMsS0FBS0MsZ0JBQUwsQ0FBc0IsU0FBdEIsRUFBaUMsVUFBQ0MsS0FBRCxFQUFXO0FBQzFDQSxRQUFNQyxTQUFOLENBQ0VDLE9BQU9DLElBQVAsQ0FBWVIsUUFBWixFQUNHUyxJQURILENBQ1EsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLE1BQU1DLE1BQU4sQ0FBYVQsYUFBYixDQUFYO0FBQUEsR0FEUixFQUVHTyxJQUZILENBRVFOLEtBQUtTLFdBQUwsRUFGUixDQURGO0FBS0QsQ0FORCxFQU1HLEVBQUNDLFNBQVMsSUFBVixFQU5IOztBQVFBVixLQUFLQyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxVQUFDQyxLQUFELEVBQVc7QUFDM0MsTUFBTVMsZ0JBQWdCLENBQUNkLFFBQUQsRUFBV0MsT0FBWCxDQUF0QjtBQUNBSSxRQUFNQyxTQUFOLENBQ0VDLE9BQU9RLElBQVAsR0FBY04sSUFBZCxDQUFtQixVQUFDTyxVQUFEO0FBQUEsV0FBZ0JBLFdBQVdDLE1BQVgsQ0FBa0IsVUFBQ0MsU0FBRDtBQUFBLGFBQWUsQ0FBQ0osY0FBY0ssUUFBZCxDQUF1QkQsU0FBdkIsQ0FBaEI7QUFBQSxLQUFsQixDQUFoQjtBQUFBLEdBQW5CLEVBQXlHVCxJQUF6RyxDQUE4RyxVQUFDVyxjQUFEO0FBQUEsV0FBb0JDLFFBQVFDLEdBQVIsQ0FBWUYsZUFBZUcsR0FBZixDQUFtQixVQUFDQyxhQUFEO0FBQUEsYUFBbUJqQixPQUFPa0IsTUFBUCxDQUFjRCxhQUFkLENBQW5CO0FBQUEsS0FBbkIsQ0FBWixDQUFwQjtBQUFBLEdBQTlHLEVBQW9OZixJQUFwTixDQUF5TjtBQUFBLFdBQU1OLEtBQUt1QixPQUFMLENBQWFDLEtBQWIsRUFBTjtBQUFBLEdBQXpOLENBREY7QUFHRCxDQUxELEVBS0csRUFBQ2QsU0FBUyxJQUFWLEVBTEg7O0FBT0FWLEtBQUtDLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLFVBQUNDLEtBQUQsRUFBVztBQUN4QyxNQUFJQSxNQUFNdUIsT0FBTixDQUFjQyxHQUFkLENBQWtCQyxVQUFsQixDQUE2QjNCLEtBQUs0QixRQUFMLENBQWNDLE1BQTNDLENBQUosRUFBd0Q7QUFDdEQzQixVQUFNNEIsV0FBTixDQUNFMUIsT0FBTzJCLEtBQVAsQ0FBYTdCLE1BQU11QixPQUFuQixFQUE0Qm5CLElBQTVCLENBQWlDLFVBQUMwQixjQUFELEVBQW9CO0FBQ25ELFVBQUlBLGNBQUosRUFBb0I7QUFDbEIsZUFBT0EsY0FBUDtBQUNEOztBQUVELGFBQU81QixPQUFPQyxJQUFQLENBQVlQLE9BQVosRUFBcUJRLElBQXJCLENBQTBCLFVBQUNDLEtBQUQ7QUFBQSxlQUFXMEIsTUFBTS9CLE1BQU11QixPQUFaLEVBQXFCbkIsSUFBckIsQ0FBMEIsVUFBQzRCLFFBQUQ7QUFBQSxpQkFBYzNCLE1BQU00QixHQUFOLENBQVVqQyxNQUFNdUIsT0FBaEIsRUFBeUJTLFNBQVNFLEtBQVQsRUFBekIsRUFBMkM5QixJQUEzQyxDQUFnRDtBQUFBLG1CQUFNNEIsUUFBTjtBQUFBLFdBQWhELENBQWQ7QUFBQSxTQUExQixDQUFYO0FBQUEsT0FBMUIsQ0FBUDtBQUNELEtBTkQsQ0FERjtBQVNEO0FBQ0YsQ0FaRCxFQVlHLEVBQUN4QixTQUFTLElBQVYsRUFaSCIsImZpbGUiOiIuL3NyYy9tb2R1bGVzL3N2L2luZGV4LmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgUFJFQ0FDSEUgPSAncHJlY2FjaGUtdjEnO1xuY29uc3QgUlVOVElNRSA9ICdydW50aW1lJztcblxuY29uc3QgUFJFQ0FDSEVfVVJMUyA9IFtcbiAgJycsXG4gICcvJyxcbiAgJy9qcy9zdHlsZXMuY3NzJyxcbiAgJy9qcy9lbnRyeS5qcydcbl07XG5cbnNlbGYuYWRkRXZlbnRMaXN0ZW5lcignaW5zdGFsbCcsIChldmVudCkgPT4ge1xuICBldmVudC53YWl0VW50aWwoXG4gICAgY2FjaGVzLm9wZW4oUFJFQ0FDSEUpXG4gICAgICAudGhlbigoY2FjaGUpID0+IGNhY2hlLmFkZEFsbChQUkVDQUNIRV9VUkxTKSlcbiAgICAgIC50aGVuKHNlbGYuc2tpcFdhaXRpbmcoKSlcbiAgKTtcbn0sIHtwYXNzaXZlOiB0cnVlfSk7XG5cbnNlbGYuYWRkRXZlbnRMaXN0ZW5lcignYWN0aXZhdGUnLCAoZXZlbnQpID0+IHtcbiAgY29uc3QgY3VycmVudENhY2hlcyA9IFtQUkVDQUNIRSwgUlVOVElNRV07XG4gIGV2ZW50LndhaXRVbnRpbChcbiAgICBjYWNoZXMua2V5cygpLnRoZW4oKGNhY2hlTmFtZXMpID0+IGNhY2hlTmFtZXMuZmlsdGVyKChjYWNoZU5hbWUpID0+ICFjdXJyZW50Q2FjaGVzLmluY2x1ZGVzKGNhY2hlTmFtZSkpKS50aGVuKChjYWNoZXNUb0RlbGV0ZSkgPT4gUHJvbWlzZS5hbGwoY2FjaGVzVG9EZWxldGUubWFwKChjYWNoZVRvRGVsZXRlKSA9PiBjYWNoZXMuZGVsZXRlKGNhY2hlVG9EZWxldGUpKSkpLnRoZW4oKCkgPT4gc2VsZi5jbGllbnRzLmNsYWltKCkpXG4gICk7XG59LCB7cGFzc2l2ZTogdHJ1ZX0pO1xuXG5zZWxmLmFkZEV2ZW50TGlzdGVuZXIoJ2ZldGNoJywgKGV2ZW50KSA9PiB7XG4gIGlmIChldmVudC5yZXF1ZXN0LnVybC5zdGFydHNXaXRoKHNlbGYubG9jYXRpb24ub3JpZ2luKSkge1xuICAgIGV2ZW50LnJlc3BvbmRXaXRoKFxuICAgICAgY2FjaGVzLm1hdGNoKGV2ZW50LnJlcXVlc3QpLnRoZW4oKGNhY2hlZFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGlmIChjYWNoZWRSZXNwb25zZSkge1xuICAgICAgICAgIHJldHVybiBjYWNoZWRSZXNwb25zZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBjYWNoZXMub3BlbihSVU5USU1FKS50aGVuKChjYWNoZSkgPT4gZmV0Y2goZXZlbnQucmVxdWVzdCkudGhlbigocmVzcG9uc2UpID0+IGNhY2hlLnB1dChldmVudC5yZXF1ZXN0LCByZXNwb25zZS5jbG9uZSgpKS50aGVuKCgpID0+IHJlc3BvbnNlKSkpO1xuICAgICAgfSlcbiAgICApO1xuICB9XG59LCB7cGFzc2l2ZTogdHJ1ZX0pO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/modules/sv/index.js\n");

/***/ })

/******/ });