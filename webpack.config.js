const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyWebpackPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const glob = require('glob');


const path = require('path');

const production = process.env.NODE_ENV === 'production';

console.warn('Production build:', production);

process.env.production = !production;

let plugins = [
  new webpack.DefinePlugin({
    __SERVER__: !production,
    __DEVELOPMENT__: !production,
    __DEVTOOLS__: !production,
    'process.env': {
      BABEL_ENV: JSON.stringify(process.env.NODE_ENV),
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      DEV_ENV: !production
    }
  }),
  new webpack.LoaderOptionsPlugin({
    options: {
      debug: !production,
      context: __dirname
    }
  }),
  new webpack.optimize.ModuleConcatenationPlugin(),
  new ExtractTextPlugin({filename: '[name].css', allChunks: true})
];

if (production) {
  plugins = plugins.concat([
    // new ImageminPlugin({
    //   externalImages: {
    //     context: 'public',
    //     sources: glob.sync('public/uploads/**/*.*'),
    //     destination: 'public'
    //   }
    // }),
    new CleanWebpackPlugin([path.join(__dirname, 'web/js/build')]),
    new UglifyWebpackPlugin({
      extractComments: true,
      parallel: true,
      uglifyOptions: {
        sourceMap: false,
        mangle: true,
        warnings: false,
        compress: {
          sequences: true, // join consecutive statemets with the “comma operator”
          properties: true, // optimize property access: a["foo"] → a.foo
          dead_code: true, // discard unreachable code
          drop_debugger: true, // discard “debugger” statements
          drop_console: true,
          unsafe: false, // some unsafe optimizations (see below)
          conditionals: true, // optimize if-s and conditional expressions
          comparisons: true, // optimize comparisons
          evaluate: true, // evaluate constant expressions
          booleans: true, // optimize boolean expressions
          loops: true, // optimize loops
          unused: true, // drop unused variables/functions
          hoist_funs: true, // hoist function declarations
          hoist_vars: false, // hoist variable declarations
          if_return: true, // optimize if-s followed by return/continue
          join_vars: true, // join var declarations
          side_effects: true // drop side-effect-free statements
        }
      }
    })
  ]);
}

const entry = {
  admin: path.resolve(__dirname, 'src/entry/admin'),
  site: path.resolve(__dirname, 'src/entry'),
  sv: path.resolve(__dirname, 'src/modules/sv'),
  'babel-polyfill': 'babel-polyfill'
};

module.exports = {
  devtool: production ? false : 'eval-source-map',
  entry,
  output: {
    path: path.join(__dirname, 'public/js'),
    filename: '[name].js',
    chunkFilename: '[name]-[chunkhash].js',
    publicPath: path.join(__dirname, 'public')
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname),
      '~': path.resolve(__dirname),
      components: path.join(__dirname, './src/components'),
      images: path.join(__dirname, './public/img')
    }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'postcss-loader',
            {loader: 'css-loader', options: {minimize: true}}
          ]
        }),
        exclude: /(public)/
      },
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {loader: 'css-loader', options: {minimize: true}},
            'postcss-loader',
            'less-loader'
          ]
        }),
        exclude: /(public)/
      },
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /(node_modules|public|main.js)/
      },
      {
        test: /\.(png|gif|jpg|jpeg|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              optipng: {
                disable: false
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false
              },
              disable: !production
            }
          }
        ],
        include: /(public)/,
        exclude: /(node_modules)/
      },
      {
        test: /\.(woff|eot|ttf)$/i,
        use: ['url-loader']
      },
      {
        test: /\.(svg)$/i,
        use: ['babel-loader', 'react-svg-loader']
      }
    ]
  },
  plugins
};
